<?php
namespace frontend\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use backend\models\CustomerMaster;
use yii\web\UploadedFile;
use yii\db\Expression;
class CustomerApiController extends Controller
{  
	/** 
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    public function beforeAction($action) {
    $this->enableCsrfValidation = false;
    return parent::beforeAction($action);
}
 //Related Videos End

 public function send_sms_number_transaction($mnumber="",$bodytxt=""){
  $mnumber=trim($mnumber);
  $return="SMSnotenabled";
  if($mnumber!="" && is_numeric($mnumber) && strlen($mnumber)=="10"){
         
        $return="SMS Send";           
        $user_name="prathap";
        $user_password="rohit";
        $user_senderid="123";
       $sms_url="http://api.clickatell.com/http/sendmsg?to";
        $mnumber='+91'.$mnumber;        
        $bodytxt=str_replace("&", 'and', "");
        $bodytxt=str_replace("+", '', "$bodytxt");
        $bodytxt=str_replace("/", '', "$bodytxt");
        $bodytxt=rawurlencode( "$bodytxt");
        //$url = "http://smstrans.adwise.org.in/sendsms.jsp?user=ryabank&password=123456a&mobiles=$to&sms=$body&senderid=RYABNK";
          $url = $sms_url."=".$mnumber."&msg=".$user_password."";
         $ch = curl_init();
         curl_setopt($ch, CURLOPT_URL, $url);
         curl_setopt($ch, CURLOPT_HEADER, true);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);          
        $return_m=curl_exec($ch);      
        $return=$return.'_'.$return_m; 
        }
  return $return;
}

 public function actionLogin(){
    $list = array();
    $postd=(Yii::$app ->request ->rawBody);
    $requestInput = json_decode($postd,true); 
    $list['status'] = 'error';
    $list['message'] = 'Nill';
    $field_check=array('phonenumber','apimethod');
     $is_error = '';
     foreach ($field_check as $one_key) {
        $key_val =isset($requestInput[$one_key]);
        if ($key_val == '') {
          $is_error = 'yes';
          $is_error_note = $one_key;
          break;
        }
    } 
    if ($is_error == "yes") {
        $list['status'] = 'error';
        $list['message'] = $is_error_note . ' is Mandatory.';
    }else{
      $apimethod=$requestInput['apimethod'];
      $phonenumber=$requestInput['phonenumber'];
      if($apimethod=="beatme"){
  $str_rnd = mt_rand(1000, 9999);
  $str_rnd="1234";
 $body = $str_rnd.' is your OTP for PoojaElectrical.';
  $student_mobile='8760776740';
  $this-> send_sms_number_transaction($student_mobile,$body);
  if($phonenumber!=""){
  $customer_master=CustomerMaster::find()
  ->where(['phone'=>$phonenumber])
  ->asArray()
  ->one();
  if(!empty($customer_master)){
    $otp=$str_rnd;
    $otpstatus="otp_sent";
    $platform="mobile";
    $active_status="1";
  $phonen=$customer_master['phone'];
   CustomerMaster::updateALL(['otp_number'=>$otp,'otp_status'=>$otpstatus,'platform'=>$platform,'active_status'=>$active_status],['phone'=>$phonen]); 
   }else{

    $model = new CustomerMaster();
    $model->phone=$phonenumber;
    $model->otp_number=$str_rnd;
    $model->otp_status="otp_sent";
    $model->platform="mobile";
    $model->active_status="1";
    if($model->save()){

    }else{
      echo "<pre>";print_r($model->getErrors());die;
    } 
  }
     
    }
    
    $list['status'] = "success";
    $list['message'] = "OTP Send successfully";
  }
  }
//Log Table 
   $response['Output'][] = $list;
    return json_encode($response);
    
  }
  public function actionVerifyotp(){
    $list = array();
    $postd=(Yii::$app ->request ->rawBody);
    $requestInput = json_decode($postd,true); 
    $list['status'] = 'error';
    $list['message'] = 'Nill';
    $field_check=array('phonenumber','otp','apimethod');
     $is_error = '';
     foreach ($field_check as $one_key) {
        $key_val =isset($requestInput[$one_key]);
        if ($key_val == '') {
          $is_error = 'yes';
          $is_error_note = $one_key;
          break;
        }
    } 
    if ($is_error == "yes") {
        $list['status'] = 'error';
        $list['message'] = $is_error_note . ' is Mandatory.';
    }else{
      $apimethod=$requestInput['apimethod'];
      $phonenumber=$requestInput['phonenumber'];
      $otp=$requestInput['otp'];
  if($apimethod=="beatme"){
    $customer_master=CustomerMaster::find()
    ->where(['phone'=>$phonenumber])
    ->asArray()
    ->one();
    if(!empty($customer_master)){
    $otp2=$customer_master['otp_number'];
    if($otp==$otp2){
    CustomerMaster::updateALL(['otp_status'=>'verified'],['phone'=>$phonenumber]);
    $list['status'] = "success";
    $list['message'] = "OTP Verfied successfully";
    }else{
    $list['status'] = "error";
    $list['message'] = "OTP Verfication Failed";
    }
    }else{
      $list['status'] = "error";
    $list['message'] = "Mobile Number Invalid";
    }
  }
  }
   $response['Output'][] = $list;
    return json_encode($response);
  }
  public function actionUpdateprofile(){
    $list = array();
    $postd=(Yii::$app ->request ->rawBody);
    $requestInput = json_decode($postd,true); 
    $list['status'] = 'error';
    $list['message'] = 'Nill';
    $field_check=array('phonenumber','name','email','apimethod','address','lat','lan');
     $is_error = '';
     foreach ($field_check as $one_key) {
        $key_val =isset($requestInput[$one_key]);
        if ($key_val == '') {
          $is_error = 'yes';
          $is_error_note = $one_key;
          break;
        }
    } 
    if ($is_error == "yes") {
        $list['status'] = 'error';
        $list['message'] = $is_error_note . ' is Mandatory.';
    }else{
      $apimethod=$requestInput['apimethod'];
      $phonenumber=$requestInput['phonenumber'];
      $name=$requestInput['name'];
      $address=$requestInput['address'];
      $email=$requestInput['email'];
     // die;
  if($apimethod=="beatme"){
    $customer_master=CustomerMaster::find()
    ->where(['phone'=>$phonenumber])
    ->asArray()
    ->one();
  if(!empty($customer_master)){
    $auth_key=Yii::$app->security->generateRandomString();
  $update=CustomerMaster::updateALL(['otp_status'=>'verified','user_type'=>'olduser','description'=>$address,'email'=>$email,'customer_name'=>$name,'auth_key'=>$auth_key,'platform'=>'mobile'],['phone'=>$phonenumber]);
  $customer_master_type=CustomerMaster::find()
    ->where(['phone'=>$phonenumber])
    ->asArray()
    ->one();
    $olduser=$customer_master_type['user_type'];
    $list['status'] = "success";
    $list['message'] = "User Updated Successfully";
    $list['user_type'] = $olduser;
    $list['auth_key'] = $auth_key;
    }else{
    $list['status'] = "error";
    $list['message'] = "Mobile Number Invalid";
    }
  }
  }
   $response['Output'][] = $list;
    return json_encode($response);
  }
}

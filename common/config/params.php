<?php
date_default_timezone_set('asia/kolkata');
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'date' => date('Y-m-d H:i:s'),
        
];

<?php
namespace common\models;

use Yii;
use yii\base\Model;
use yii\web\Session;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use backend\components\QueryBehaviour;
use backend\models\SubscriptionMaster;
use backend\models\User;
use backend\models\SubscriptionTable;
use backend\models\CommissionStructure;
use backend\models\CommissionDataStructure;
use backend\models\LevelCompletionStatus;
use backend\models\PairTable;
use backend\models\BinaryUser;
use backend\models\PaymentLog;
use backend\models\CommisionReport;

/**
 * Login form
 */
class CommonSave extends Model
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'QueryBehaviour' => QueryBehaviour::className(),
        ];
    }

    public function SubscriptionSave($subscription=array())
    {         
        $model = (isset($subscription['primary_id'])) ? SubscriptionMaster::findOne($subscription['primary_id']) : new SubscriptionMaster();
        $connection = \Yii::$app->db;        
        $transaction = $connection->beginTransaction();
        $session = Yii::$app->session;
        try 
        {
            if($session[Yii::$app->params['inputname']] == $subscription[Yii::$app->params['inputname']])
            {
                $model->package_name = $subscription['package_name'];
                $model->package_amount = $subscription['package_amount'];
                $model->level = $subscription['level'];
                $model->description = $subscription['description'];
                if(!empty($subscription['status']))
                {
                    if($subscription['status'] == 'on')
                    {
                        $model->status='1';    
                    }    
                }
                else
                {
                    $model->status='0';       
                }
                $model->created_at = (isset($subscription['primary_id'])) ? $model->created_at : Yii::$app->params['date'];
                $model->updated_at = Yii::$app->params['date'];
                if($model->save())
                {    
                    unset($session[Yii::$app->params['inputname']]);
                    $transaction->commit();
                    return true;    
                }
                else
                {
                    $transaction->rollback();
                    return true;    
                }
            }
            else
            {
                $transaction->rollback();
                return true;
            }   
        } 
        catch(Exception $e) 
        {
            $transaction->rollback();
            return true;
        }
    }

    public function UserSave($user=array())
    {
        
        $model = (isset($user['primary_id'])) ? User::findOne($user['primary_id']) : new User();
        
        if(isset($user['primary_id']))
        {
            $model->scenario = User::SCENARIO_UPDATE;
        }
        else if(isset($user['scenario']))
        {
            $model->scenario = User::SCENARIO_API_CREATE;
        }
        else
        {
            $model->scenario = User::SCENARIO_CREATE;
        }

        $connection = \Yii::$app->db;        
        $transaction = $connection->beginTransaction();
        $session = Yii::$app->session;
        try 
        {
            
            if($session[Yii::$app->params['inputname']] == $user[Yii::$app->params['inputname']])
                {
                $model->first_name = $user['first_name'];
                $model->email_id = $user['email_id'];
                $model->mobile_number = $user['mobile_number'];
                $model->address = $user['address'];
                $model->aadhar_number = $user['aadhar_number'];
                $model->subscripe_plan = $user['subscripe_plan'];
                $model->bank_account_no = $user['bank_account_no'];
                $model->ifsc = $user['ifsc'];
                $model->branch = $user['branch'];
                $model->bank_name = $user['bank_name'];
                $model->refferal_id = (isset($user['primary_id'])) ? $model->refferal_id : $user['refferal_id_generated'];
                //$model->user_type_data = $this->ParentUser()['user_type_data'];
                $model->user_type_data = (empty($user['refferal_id'])) ? 'P' : 'C';
                if($user['password_hash'] == '' && isset($user['primary_id']))
                {
                    $model->password_hash = $model->password_hash; 
                }
                else
                {
                    $model->password_hash = Yii::$app->security->generatePasswordHash($user['password_hash']);
                }
                $model->auth_key= Yii::$app->security->generateRandomString();

                $model->create_at  = (isset($user['primary_id'])) ? $model->create_at : Yii::$app->params['date'];
                $model->update_at  = Yii::$app->params['date']; 


                $postd = $_FILES;
                if(isset($postd['User']['name']))
                {
                    $aa6 = $aa61 = "";
                    if(array_key_exists('profile_image', $postd['User']['name'])){
                            //$temp = str_replace('api', 'backend', Yii::$app->basePath);
                            $temp = Yii::$app->basePath;
                            if ($postd['User']['error']['profile_image'] == 0){
                                $file_name = preg_replace('/\s+/', '_', $postd['User']['name']['profile_image']);
                                $aa6 = "/web/uploads/".$this->getRandomString(7).$file_name;
                                $aa6_new = $temp.$aa6;
                                move_uploaded_file($postd['User']['tmp_name']['profile_image'], $aa6_new);
                                $model->profile_image="/backend".$aa6; 
                            }                            
                        }
                        if(array_key_exists('profile_pdf', $postd['User']['name'])){
                            $temp1 = Yii::$app->basePath;
                            if ($postd['User']['error']['profile_pdf'] == 0){
                                $file_name1 = preg_replace('/\s+/', '_', $postd['User']['name']['profile_pdf']);
                                $aa61 = "/web/uploads/".$this->getRandomString(7).$file_name1;
                                $aa61_new = $temp1.$aa61;
                                move_uploaded_file($postd['User']['tmp_name']['profile_pdf'], $aa61_new);
                                $model->profile_pdf="/backend".$aa61; 
                            }                            
                        }    
                }


                if($model->save())
                {
                        $user['subscription_insert_type'] = (isset($user['primary_id'])) ? 'update' : 'create';    
                        $user['refferal_user_id'] = $model->id;
                        $user['user_type_data'] = $model->user_type_data;

                        $subscription_save_id = $this->SubscriptionTable($user);
                        $user['subscription_id'] = $subscription_save_id;
                        $random_subscription = NULL;
                        if(!empty($subscription_save_id))
                        {
                            $subscription_table_random_id = SubscriptionTable::findOne(['subscripe_id'=>$subscription_save_id]); 
                            $random_subscription = $subscription_table_random_id->random_subscribe_no;   
                        }
                        
                        if(empty($subscription_save_id))
                        {
                            $transaction->rollback();
                            return true;
                        }
               
                        unset($session[Yii::$app->params['inputname']]);
                        $transaction->commit();
                        return array('id'=>$model->id,'auth_key'=>$model->auth_key,'refferal_id'=>$model->refferal_id,'random_subscription'=>$random_subscription,'subscripe_id'=>$user['subscription_id']);                
                }
                else
                {
                    $transaction->rollback();
                    return true;    
                }
            }
            else
            {
                $transaction->rollback();
                return true;
            }      
        } 
        catch(Exception $e) 
        {
            $transaction->rollback();
            return true;
        }
    }

    public function SubscriptionTable($user)
    {
        if($this->Checkvalidtoken($user))
        {
            if($user['subscription_insert_type'] == 'update')
            {
                $SubscriptionTable = SubscriptionTable::find()->where(['user_id'=>$user['primary_id']])->andWhere(['current_status'=>'1'])->one();
                if(!empty($SubscriptionTable))
                {
                    $SubscriptionTable->subscription_master_id = $user['subscripe_plan'];
                    $SubscriptionTable->update_at = Yii::$app->params['date'];
                    if($SubscriptionTable->save())
                    {
                        return $SubscriptionTable->subscripe_id;
                    }
                    else
                    {
                        print_r($SubscriptionTable->getErrors());die;
                    }
                }
            }
            else if($user['subscription_insert_type'] == 'create')
            {
                $SubscriptionTable = new SubscriptionTable();
                $SubscriptionTable->random_subscribe_no = 'SUB'.$this->add_subscripe_random_no();
                $SubscriptionTable->user_id = $user['refferal_user_id'];
                $SubscriptionTable->subscription_master_id  = $user['subscripe_plan'];
                $SubscriptionTable->commision_data_structure_id   = $this->setCommisionStructureData();
                $SubscriptionTable->registeration_date   = Yii::$app->params['date'];
                $SubscriptionTable->current_status   = '1';
                $SubscriptionTable->max_user_count = $this->getConfigData()['branch'];
                $SubscriptionTable->mode = 'C';
                $SubscriptionTable->referal_code = $user['refferal_id_generated'];
                $SubscriptionTable->refered_by_code = $user['refferal_id'];
                $SubscriptionTable->refered_by_user = $this->getUserReferalNameSubscripttionwise($user['refferal_id'])['user_id'];
                $SubscriptionTable->payment_status = Yii::$app->params['payment_status'][0];
                $SubscriptionTable->created_at   = Yii::$app->params['date'];
                $SubscriptionTable->update_at   = Yii::$app->params['date'];
                $SubscriptionTable->payment_status = 'PAYMENT_PENDING';
                $SubscriptionTable->user_type_data = (empty($user['refferal_id'])) ? 'P' : 'C';
                if($SubscriptionTable->save())
                {
                    $this->call_procedure();
                    SubscriptionTable::updateAll(['pending_users_count'=>$this->NewPendingReferalCountPayment($user['refferal_id'])],['referal_code'=>$user['refferal_id']]);
                    return $SubscriptionTable->subscripe_id;
                }
                else
                {
                    print_r($SubscriptionTable->getErrors());die;
                }
            }
            else if($user['subscription_insert_type'] == 'update-reason')
            {
                $SubscriptionTable = SubscriptionTable::findOne($user['primary_id']);
                $SubscriptionTable->elimination_date = date('Y-m-d H:i:s',strtotime($user['elimination_date']));
                $SubscriptionTable->reason = $user['reason'];
                $SubscriptionTable->mode = 'E';
                $SubscriptionTable->current_status = '0';
                $SubscriptionTable->closing_date = Yii::$app->params['date'];
                $SubscriptionTable->update_at = Yii::$app->params['date'];
                
                if($SubscriptionTable->save())
                {
                    return true;
                }
                else
                {
                    print_r($SubscriptionTable->getErrors());die;
                }
            }
            else if($user['subscription_insert_type'] == "closing-date")
            {
                $SubscriptionTable = SubscriptionTable::findOne($user['primary_id']);
                $SubscriptionTable->closing_date = date('Y-m-d H:i:s',strtotime($user['closing_date']));
                $SubscriptionTable->mode = 'O';
                $SubscriptionTable->current_status = '2';
                $SubscriptionTable->update_at = Yii::$app->params['date'];
                if($SubscriptionTable->save())
                {
                    return true;
                }
                else
                {
                    print_r($SubscriptionTable->getErrors());die;
                }
            }
            else if($user['subscription_insert_type'] == "rejoin-date")
            {
                $SubscriptionTable_exist = SubscriptionTable::findOne($user['primary_id']);
                $SubscriptionTable = new SubscriptionTable(['scenario' => SubscriptionTable::SCENARIO_DEFAULT]);
                $SubscriptionTable->random_subscribe_no = 'SUB'.$this->add_subscripe_random_no();
                $SubscriptionTable->user_id = $SubscriptionTable_exist->user_id;
                $SubscriptionTable->rejoin_date = date('Y-m-d H:i:s',strtotime($user['rejoin_date']));
                $SubscriptionTable->commision_data_structure_id   = $this->setCommisionStructureData();
                $SubscriptionTable->subscription_master_id = $user['subscription_master_id'];
                //$SubscriptionTable->referal_key = $user['referal_key'];
                $SubscriptionTable->referal_code = $this->getRandomString();
                $SubscriptionTable->refered_by_code = $user['refered_by_code'];
                $SubscriptionTable->refered_by_user = $this->getUserReferalName($user['refered_by_code'])['id'];

                $SubscriptionTable->registeration_date = Yii::$app->params['date'];
                $SubscriptionTable->mode = 'R';
                $SubscriptionTable->current_status = '3';
                $SubscriptionTable->created_at = Yii::$app->params['date'];
                $SubscriptionTable->update_at = Yii::$app->params['date'];
                if($SubscriptionTable->save())
                {
                    $this->call_procedure();

                    SubscriptionTable::updateAll(['total_approved_user'=>$this->ReferalCount($user['refered_by_code']),'pending_users_count'=>$this->PendingReferalCount($user['refered_by_code'])],['referal_code'=>$user['refered_by_code']]);
                    return true;
                }
                else
                {
                    print_r($SubscriptionTable->getErrors());die;
                }
            }
            else if($user['subscription_insert_type'] == "rejoin-approved")
            {
                
                $SubscriptionTable = SubscriptionTable::findOne($user['primary_id']);
                $SubscriptionTable->reason = (isset($user['reason'])) ? $user['reason'] : $SubscriptionTable->reason;
                $SubscriptionTable->mode = 'A';
                $SubscriptionTable->current_status = $user['current_status'];
                $SubscriptionTable->activation_date = Yii::$app->params['date'];
                $SubscriptionTable->rejected_date = ($user['current_status'] == '4') ? Yii::$app->params['date'] : NULL;
                $SubscriptionTable->update_at = Yii::$app->params['date'];
                if($SubscriptionTable->save())
                {
                    SubscriptionTable::updateAll(['total_approved_user'=>$this->ReferalCount($SubscriptionTable->refered_by_code),'pending_users_count'=>$this->PendingReferalCount($SubscriptionTable->refered_by_code)],['referal_code'=>$SubscriptionTable->refered_by_code]);
                    return true;
                }
                else
                {
                    print_r($SubscriptionTable->getErrors());die;
                }
            }
        }
    }

    public function CommisionStructure($commision_array)
    {
        $connection = \Yii::$app->db;        
        $transaction = $connection->beginTransaction();
        $session = Yii::$app->session;
        try 
        {
            $model = CommissionStructure::findOne($commision_array['editableKey']);
            $current = current($commision_array['CommissionStructure']);
            $model->commision = $current['commision'];
            if($model->save())
            {
                $commision_array['commision_data'] = $this->setCommisionData();
                $this->CommisionStructureUpdation();
                if($this->CommissionDataStructure($commision_array))
                {
                    $transaction->commit();
                    $out = json_encode(['output'=>'', 'message'=>'']);
                    return $out;
                }
            }
        }
        catch(Exception $e) 
        {
            $transaction->rollback();
            return true;
        }
    }

    public function CommisionStructureUpdation()
    {
        CommissionDataStructure::updateAll(['status' => '0','updated_at' => Yii::$app->params['date']], ['status' => '1']);
    }

    public function CommissionDataStructure($commision_array)
    {
        $commission_data = new CommissionDataStructure();
        $commission_data->commision_data = json_encode($commision_array['commision_data']);
        $commission_data->status = '1';
        $commission_data->created_at = Yii::$app->params['date'];
        $commission_data->updated_at = Yii::$app->params['date'];
        if($commission_data->save())
        {
            return true;
        }
    }

    public function PairTable($data)
    {
        if($data['user_type_data'] == 'C')
        {
            $data_level_completion_status = $this->getLevelCompletionStatus($data);        
            $data_pair_table = $this->getCommissionData();
            $data_pair_table_last = $this->getCommissionDataLast();

            //Check Level
            //$data_level_completion_status_check=$this->getLevelCompletionStatusCheck($data);

            $subscribe_id = $data['subscription_id'];
            $parent_user_id = $data['id'];
            $level = 1;
            $pair_1 = 2;
            $pair_2 = NULL;
            $status = '1';

            if(empty($data_level_completion_status))
            {
                foreach (Yii::$app->params['pair-data'] as $key => $value) 
                {
                    $data['no_of_levels'] = $key;
                    $data['total_no_of_pairs_each_level'] = $value;
                    $data['data_type'] = 'create';
                    $this->LevelCompletionStatus($data);
                }
            }
            else if(!empty($data_level_completion_status))
            {
                foreach ($data_level_completion_status as $key => $value) 
                {
                    if($value['is_fully_completed'] == 'NO')
                    {
                        $level=$value['no_of_levels'];
                        break;
                    }        
                }
            }

            if($data_pair_table)
            {
                //Get Data Id
                $pair_table_id = $data_pair_table['id'];
                //$level = $data_pair_table['level'];
                $pair_1 = $data_pair_table['pair_1'];
                $pair_2 = $data_pair_table['pair_1']+1;
                $status = '0';

            }
            else if($data_pair_table_last)
            {
                $pair_1 = $data_pair_table_last['pair_2']+1;
            }

            $PairTableInsert = (isset($pair_table_id)) ? PairTable::findOne($pair_table_id) : new PairTable();
            $PairTableInsert->subscribe_id = $subscribe_id;
            $PairTableInsert->parent_user_id  = $parent_user_id;
            $PairTableInsert->level  = $level;
            $PairTableInsert->pair_1  = $pair_1;
            $PairTableInsert->pair_2  = $pair_2;
            $PairTableInsert->status = $status;
            $PairTableInsert->created_at = Yii::$app->params['date'];
            $PairTableInsert->updated_at = Yii::$app->params['date'];
            if($PairTableInsert->save())
            {
                $data['data_type'] = 'pairs';
                $data['no_of_levels'] = $level;
                $this->LevelCompletionStatus($data);      
            }
        }      
    }


    public function LevelCompletionStatus($data)
    {   
        if($data['data_type'] == 'create')
        {
            $level_completion_status = new LevelCompletionStatus();
            $level_completion_status->subscription_id = $data['subscription_id'];
            $level_completion_status->no_of_levels = $data['no_of_levels'];
            $level_completion_status->total_no_of_pairs_each_level = $data['total_no_of_pairs_each_level'];
            $level_completion_status->total_no_of_members = $data['total_no_of_members'];
            $level_completion_status->no_of_pairs_completed = 0;
            $level_completion_status->is_fully_completed = 'NO';
            $level_completion_status->created_at = Yii::$app->params['date'];
            $level_completion_status->updated_at = Yii::$app->params['date'];
            $level_completion_status->save();
        }
        else if($data['data_type'] == 'pairs')
        {
            $data_validated_insert = $this->getPairTableLevel($data);
            $level_completion_status = LevelCompletionStatus::find()->where(['no_of_levels'=>$data['no_of_levels']])->andWhere(['subscription_id'=>$data['subscription_id']])->andWhere(['user_id'=>$data['id']])->one();
            if($level_completion_status)
            {
                $level_completion_status->no_of_pairs_completed = $data_validated_insert['no_of_pairs_completed'];
            
                if($data_validated_insert['no_of_pairs_completed'] == $level_completion_status['total_no_of_pairs_each_level'])
                {
                    $level_completion_status->is_fully_completed = 'YES';
                }
                $level_completion_status->save();
            }
        }   
    }

    /*Data of Binary User Logic*/
    public function BinaryUser(array $data) : array
    {
        $data['increment'] = 1;
        $data['binary_refferal_id'] = $data['refferal_id_generated'];
        $binary_array = $this->BinaryDataLogic($data);
        return $binary_array;
    }

    # Author : Alban
    # Timestamp : 18-08-2020 16:37 PM
    # Desc : Binary Data Logic
    public function BinaryDataLogic(array $data) : array
    {
        $binary_insert = array();

        $subscription_referal_id = SubscriptionTable::findOne(['subscripe_id'=>$data['subscription_id']]);

        $data['subscription_primary_id'] = $data['subscription_id'];
        //$get_user_data = $this->getUserReferalName($subscription_referal_id->refered_by_code);
        $get_user_data = SubscriptionTable::findOne(['referal_code'=>$subscription_referal_id->refered_by_code]);
        if(empty($get_user_data))
        {
            $get_user_data = SubscriptionTable::findOne(['subscripe_id'=>$data['subscription_id']]);
        }

        $get_user_data = ArrayHelper::toArray($get_user_data);
        $get_user_data['id'] = $get_user_data['subscripe_id'];

        //Parent First Insert
        $get_binary_data_first = BinaryUser::findOne(['parent_subscription_id'=>$get_user_data['id']]);
         
        $get_binary_data_right_and_left = BinaryUser::findOne(['subscription_id'=>$get_user_data['id'],'level'=>'L0']);

        
        //Insert an Records For First Referral Data
        if($data['user_type_data'] == Yii::$app->params['user_type_data']['P'])
        {
            $data['binary_insert'] = ['user_id'=>$data['refferal_user_id'],'subscription_id'=>$data['subscription_primary_id'],'level' => 'L0' , 'parent_id'=> NULL, 'parent_subscription_id'=>NULL,'side' => NULL,'position' => 1, 'status' => '1' ,'created_at' => Yii::$app->params['date'],'updated_at' => Yii::$app->params['date']];

             $data_id_binary = $this->BinaryUserInsert($data['binary_insert']);
             $binary_insert[] = $data_id_binary;
             return $binary_insert;
        }

        //Insert Right 1st Records
        if(!empty($get_binary_data_right_and_left) && empty($get_binary_data_first))
        {
            $data['binary_insert'] = ['user_id'=>$data['refferal_user_id'],'subscription_id'=>$data['subscription_primary_id'],'level' => 'L1' , 'parent_id'=>$get_binary_data_right_and_left->user_id , 'parent_subscription_id'=>$get_binary_data_right_and_left->subscription_id,'side' => 'R','position' => 2, 'status' => '1' ,'created_at' => Yii::$app->params['date'],'updated_at' => Yii::$app->params['date']];
             
             $data_id_binary = $this->BinaryUserInsert($data['binary_insert']);
             $binary_insert[] = $data_id_binary;
             return $binary_insert;
        }
        //Insert Left Leve1th 1st Records
        else if(!empty($get_binary_data_right_and_left))
        {
            $get_binary_data_left = BinaryUser::findOne(['parent_subscription_id'=>$get_user_data['id'],'level'=>'L1','side'=>'R']);
              
            if(!empty($get_binary_data_left))
            {
                $data['binary_insert'] = ['user_id'=>$data['refferal_user_id'],'subscription_id'=>$data['subscription_primary_id'],'level' => 'L1' , 'parent_id'=>$get_binary_data_right_and_left->user_id, 'parent_subscription_id'=>$get_binary_data_right_and_left->subscription_id,'side' => 'L','position' => 3, 'status' => '1' ,'created_at' => Yii::$app->params['date'],'updated_at' => Yii::$app->params['date']];
                
                $data_id_binary = $this->BinaryUserInsert($data['binary_insert']);
                $binary_insert[] = $data_id_binary;
                return $binary_insert;
            }
        }

        if(!empty($get_user_data))
        {
            $level = 'L1';
            $decrement_level = '';
            $current_parent_id = $get_user_data['id'];
            $position = 0;
            $side = '';

            
            $get_subscription_master_data = SubscriptionMaster::find()->asArray()->indexBy(['subscription_id'])->all();
            //$end_level = 'L6';
            $end_level = $get_subscription_master_data[$subscription_referal_id->subscription_master_id]['level']; 
            
            $get_first_init = BinaryUser::findOne(['subscription_id'=>$current_parent_id,'side'=>'R']);

            $get_first_init_left = BinaryUser::findOne(['subscription_id'=>$current_parent_id,'side'=>'L']);
            
            $data_id_binary = '';
            if(!empty($get_first_init))
            {
                $get_first_init_right = BinaryUser::findOne(['parent_subscription_id'=>$current_parent_id,'side'=>'R']);
                if(empty($get_first_init_right))
                {
                    $data['binary_insert'] = ['user_id'=>$data['refferal_user_id'],'subscription_id'=>$data['subscription_primary_id'],'level' => 'L1' , 'parent_id'=>$get_first_init->user_id, 'parent_subscription_id'=>$current_parent_id,'side' => 'R','position' => 2, 'status' => '1','created_at' => Yii::$app->params['date'],'updated_at' => Yii::$app->params['date']];
                    $side = 'R';
                    $position = 2;
                }
                else
                {
                    $data['binary_insert'] = ['user_id'=>$data['refferal_user_id'],'subscription_id'=>$data['subscription_primary_id'],'level' => 'L1' , 'parent_id'=>$get_first_init->user_id, 'parent_subscription_id'=>$current_parent_id,'side' => 'L','position' => 3, 'status' => '1','created_at' => Yii::$app->params['date'],'updated_at' => Yii::$app->params['date']];
                    $side = 'L';
                    $position = 3;
                }
                $data_id_binary = $this->BinaryUserInsert($data['binary_insert']);
                $binary_insert[] = $data_id_binary;
                
            }
            else if(!empty($get_first_init_left))
            {
                $get_first_init_right = BinaryUser::findOne(['parent_subscription_id'=>$current_parent_id,'side'=>'R']);
                if(empty($get_first_init_right))
                {
                    $data['binary_insert'] = ['user_id'=>$data['refferal_user_id'],'subscription_id'=>$data['subscription_primary_id'],'level' => 'L1' , 'parent_id'=>$get_first_init_left->user_id, 'parent_subscription_id'=>$current_parent_id,'side' => 'R','position' => 2, 'status' => '1','created_at' => Yii::$app->params['date'],'updated_at' => Yii::$app->params['date']];
                    $side = 'R';
                    $position = 2;
                }
                else
                {
                    $data['binary_insert'] = ['user_id'=>$data['refferal_user_id'],'subscription_id'=>$data['subscription_primary_id'],'level' => 'L1' , 'parent_id'=>$get_first_init_left->user_id, 'parent_subscription_id'=>$current_parent_id,'side' => 'L','position' => 3, 'status' => '1','created_at' => Yii::$app->params['date'],'updated_at' => Yii::$app->params['date']];
                    $side = 'L';
                    $position = 3;
                }
                $data_id_binary = $this->BinaryUserInsert($data['binary_insert']);
                $binary_insert[] = $data_id_binary;
                
            }
                       
            $current_parent_id_check = '';
        while(true) 
        {
            
            $level_increment = preg_replace('/[^0-9]/', '', $level);
            $level_increment_one = $level_increment+1;
            $level = 'L'.$level_increment_one;

            $dec = $level;
            $decrement_level_one = preg_replace('/[^0-9]/', '', $dec);
            $decrement_level_one = $decrement_level_one-1;
            $dec  = 'L'.$decrement_level_one;



            $get_binary_data_insert = BinaryUser::findOne(['subscription_id'=>$current_parent_id,'level'=>$dec]);

            

            if(!empty($get_binary_data_insert))
            {
                
                /*$subscription_table_data = SubscriptionTable::findOne($get_binary_data_insert->parent_subscription_id);
                 
                if($level == $end_level)
                {
                    if(array_key_exists($subscription_table_data->subscription_master_id, $get_subscription_master_data))
                    {
                        $get_master_level = $get_subscription_master_data[$subscription_table_data->subscription_master_id]['level'];
                        
                        if($get_master_level == 'L5')
                        {
                            break;
                        }
                    }
                }*/

                if($side == 'L')
                {
                    $position = ($get_binary_data_insert->position*2)+1;
                }
                else if($side == 'R')
                {
                    $position = $get_binary_data_insert->position*2;   
                }

                $data['binary_insert'] = ['user_id'=>$data['refferal_user_id'],'subscription_id'=>$data['subscription_primary_id'],'level' => $level , 'parent_id'=>$get_binary_data_insert->parent_id, 'parent_subscription_id'=>$get_binary_data_insert->parent_subscription_id,'side' => NULL,'position' => $position, 'status' => '1','created_at' => Yii::$app->params['date'],'updated_at' => Yii::$app->params['date']];

                $data_id_binary = $this->BinaryUserInsert($data['binary_insert']);
                $binary_insert[] = $data_id_binary;
                    
            }
            else
            {
                $get_binary_data_insert = BinaryUser::findOne(['subscription_id'=>$current_parent_id_check,'level'=>'L0']);
                if(!empty($get_binary_data_insert))
                {
                    break;    
                }
            }

            if($level == $end_level)
            {
                break;
            }

            $current_parent_id_check = (isset($get_binary_data_insert->parent_subscription_id)) ? $get_binary_data_insert->parent_subscription_id : '';
            if(empty($current_parent_id_check))
            {
                break;
            }
        }
        }
        return $binary_insert;
    }
    
    # Author : Alban
    # Created Date : 27-08-2020 16:24 PM
    # Updated Date : 27-08-2020 16:24 PM
    # Desc : Level Completion Status Updated
    public function LevelCompletionStatusUpdated($binary_insert)
    {
        $data_completed = array();
        
        $binary_user = BinaryUser::findAll($binary_insert); 
        $binary_count = count($binary_user);
                
        if($binary_count == 1)
        {
            $data_check = 'no';
            foreach ($binary_user as $key => $value) 
            {
                 if(empty($value->parent_subscription_id))
                 {
                    $data_check = 'yes';
                 }
                 break;   
            }
            if($data_check == 'yes')
            {
                return $data_completed;       
            }        
        }    

            $binary_user_array = ArrayHelper::toArray($binary_user);   

            $binary_parent_map = ArrayHelper::map($binary_user,'parent_subscription_id','parent_subscription_id');   
            
            $updated_data = $this->level_updated_data($binary_parent_map);
            
            $data_level_array = array('L1','L2','L3','L4','L5','L6');

            $data_parent_count = array();
            foreach ($binary_parent_map as $key => $value) {
                if(!empty($value))
                {
                    $data = array();
                    foreach ($data_level_array as $key_1 => $value_1) 
                    {
                        $binary_user_check = BinaryUser::find()->select(['subscription_id'=>'subscription_id','parent_id'=>'parent_id','level'=>'level','parent_subscription_id'=>'parent_subscription_id'])->where(['parent_subscription_id'=>$value])->andWhere(['level'=>$value_1])->asArray()->all();   

                        if(empty($binary_user_check))
                        {
                            continue;
                        }

                        $binary_user_check_count = count($binary_user_check);
                        $data_parent_count[$value][$value_1] = $binary_user_check_count;
                        
                    }

                    $data = BinaryUser::find()->select(['subscription_id'=>'subscription_id','parent_id'=>'parent_id','level'=>'level','parent_subscription_id'=>'parent_subscription_id'])->where(['status'=>'1'])->andWhere(['and',['IS NOT','parent_subscription_id',NULL],['<>','parent_subscription_id',''],['parent_subscription_id'=> $value]])->asArray()->all();

                    $updated_fetch[$value] = ArrayHelper::index($data,'level'); 
                }   
            }    
            
            $count_key = 0;
            foreach ($updated_fetch as $key => $value) 
            {
                foreach ($value as $key_1 => $value_1) 
                {
                    if(array_key_exists($key, $data_parent_count))
                    {
                        if (array_key_exists($key_1, $data_parent_count[$key])) 
                        {
                            $count_key = $data_parent_count[$key][$key_1];
                            $updated_fetch[$key][$key_1]['parent_id_count'] = $count_key;
                        }
                    }
                }
            }

            //print_r($data_parent_count);
            
            //Get Commision Data
            $commision_data = $this->getCommissionStructureData();
            if(!empty($updated_data))
            {
                foreach ($updated_data as $key => $value) 
                {
                    $is_fully_completed = 'NO';

                    if(array_key_exists($value['subscription_id'], $updated_fetch))
                    {
                        $level_id = isset(Yii::$app->params['level-config'][$value['no_of_levels']]) ? Yii::$app->params['level-config'][$value['no_of_levels']] : '';

                        if(array_key_exists($level_id, $updated_fetch[$value['subscription_id']]))
                        {

                            $data_value =$updated_fetch[$value['subscription_id']][$level_id];

                            $get_total_count =  floor($data_value['parent_id_count']/2);
                            $total_no_of_pairs_each_level = $value['total_no_of_pairs_each_level'];

                            $is_fully_completed = ($get_total_count == $total_no_of_pairs_each_level) ? 'YES' : $is_fully_completed;
                            
                            $data_completed[] = array('subscription_id'=>$value['subscription_id'],'level'=>$value['no_of_levels'],'id'=>$value['id'],'accumulate_price'=>$value['accumulate_price']);

                            LevelCompletionStatus::updateAll(['no_of_pairs_completed'=>$get_total_count,'total_no_of_members_joined'=>$data_value['parent_id_count'],'is_fully_completed'=>$is_fully_completed],['id'=>$value['id']]);
                        }
                    }
                }
            }
        
        return $data_completed;     
    }

    
    /*Binary Insert*/
    public function BinaryUserInsert(array $binary_insert_data) //: bool
    {
        $binary_insert = new BinaryUser();
        $binary_insert->attributes = $binary_insert_data;
        return ($binary_insert->save()) ? $binary_insert->auto_id : '';
    }

    /*Pair Commision Report*/
    public function PairCommisionReport(array $data)
    {
        if(!empty($data))
        {
            $commision_data = $this->getCommissionStructureData();
            foreach ($data as $key => $value) 
            {
                $level_config = Yii::$app->params['level-config'][$value['level']];

                $binary_user = BinaryUser::find()->where(['parent_subscription_id'=>$value['subscription_id']])->andWhere(['level'=>$level_config])->asArray()->all();

                $get_position = ArrayHelper::getColumn($binary_user, 'position');
                $get_position_chunk = array_chunk($get_position, 2);
                $commision_accmulate = (!empty($value['accumulate_price'])) ? $value['accumulate_price'] : 0;
                
                foreach ($get_position_chunk as $key_1 => $value_1) {

                    //data count
                    $data_count = count($value_1);    

                    if($data_count != 2)
                    {
                        continue;
                    }    

                    $get_split = implode($value_1,',');
                    
                    //Data Validated
                    $commision_report = CommisionReport::find()->where(['subscription_tbl_id'=>$value['subscription_id']])->andWhere(['level_id'=>$value['level']])->andWhere(['pair'=>$get_split])->exists();

                    if(!empty($commision_report))
                    {
                        continue;
                    }

                    $amount_cal = $commision_data[$value['level']]['commision']*2;
                    $commision_accmulate = $commision_accmulate+$amount_cal;
                    $data['commision_report_insert'] = ['subscription_tbl_id' => $value['subscription_id'],'level_id' => $value['level'], 'pair' => $get_split , 'commision_value' => $amount_cal , 'commision_settled' => 'NO' , 'is_pair_completed' => 'YES' , 'payment_id' => NULL ,'status' => '1','created_at' => Yii::$app->params['date'],'updated_at' => Yii::$app->params['date']];

                    $this->CommisionReportInsert($data['commision_report_insert']);
                }

                LevelCompletionStatus::updateAll(['accumulate_price'=>$commision_accmulate],['id'=>$value['id']]);

            }
        }   
    }

    /*Commision Report Insert*/
    public function CommisionReportInsert(array $commision_report_insert) //: bool
    {
        $commision_report = new CommisionReport();
        $commision_report->attributes = $commision_report_insert;
        return ($commision_report->save()) ? $commision_report->auto_id : '';
    }

    /*Payment Log*/
    public function PaymentInsert(array $payment_insert_data) //: bool
    {
        $session = Yii::$app->session;
        $connection = \Yii::$app->db;        
        $transaction = $connection->beginTransaction();
        $session = Yii::$app->session;
        try 
        {
            if($session[Yii::$app->params['inputname']] == $payment_insert_data[Yii::$app->params['inputname']])
            {
                $payment_insert = new PaymentLog();
                $payment_insert->attributes = $payment_insert_data['PaymentLog'];
                if($payment_insert->save())
                {

                    $subscription_table = SubscriptionTable::findOne($payment_insert_data['subscripe_id']);
                    $subscription_table->payment_status = 'PAYMENT_SUCCESS';
                    $subscription_table->activation_date = Yii::$app->params['date'];
                    if($subscription_table->save())
                    {
                        $subscription_master_level = SubscriptionMaster::findOne($subscription_table->subscription_master_id);

                        $data_sub_id = '';
                        if(isset($subscription_table->refered_by_code))
                        {
                            $data_sub_id = $subscription_table->refered_by_code;
                        }
                        else
                        {
                            $data_sub_id = $subscription_table->referal_code;
                        }

                        $get_pending_count = SubscriptionTable::findOne(['referal_code'=>$data_sub_id]);
                        $data_calc = 0;
                        if(isset($get_pending_count->pending_users_count) && $get_pending_count->pending_users_count > 0)
                        {
                            $data_calc = $get_pending_count->pending_users_count - 1;    
                        }

                        SubscriptionTable::updateAll(['total_approved_user'=>$this->NewReferalCountPayment($data_sub_id),'pending_users_count'=>$data_calc],['referal_code'=>$data_sub_id]);

                        $CommissionStructureData=$this->getCommissionStructureData();
                        
                        if(!empty($CommissionStructureData))
                        {
                            $inc = 1;
                            foreach ($CommissionStructureData as $key => $value) 
                            {

                                //Level data    
                                $level_id = isset(Yii::$app->params['level-config'][$inc]) ? Yii::$app->params['level-config'][$inc] : '';    

                                $data_commision_array = array('subscription_id'=>$payment_insert_data['subscripe_id'],'no_of_levels'=>$value['id'],'total_no_of_pairs_each_level'=>$value['no_of_pairs'],'total_no_of_members'=>$value['no_of_pairs']*2,'data_type'=>'create');
                                $this->LevelCompletionStatus($data_commision_array);
                                    
                                if($subscription_master_level->level == $level_id)
                                {
                                    break;
                                }
                                $inc++;    
                            }
                        }

                        /* Data Argument Passed*/
                        $user_data = User::findOne($subscription_table->user_id);

                        $data['refferal_id_generated']=$subscription_table->referal_code;    
                        $data['subscription_id']=$payment_insert_data['subscripe_id'];    
                        $data['refferal_user_id'] = $subscription_table->user_id; 
                        $data['user_type_data'] = $subscription_table->user_type_data;
                        $data['subscripe_plan'] = $subscription_table->subscription_master_id;
                        

                        /* Binary User Inserted Before Status Change Payment Success*/
                        $binary_insert  = $this->BinaryUser($data); 
                        /*Level Completion Updated Status*/
                        $data_completed = $this->LevelCompletionStatusUpdated($binary_insert);
                        /*Pair and Commision Updated in Commision Report*/
                        $this->PairCommisionReport($data_completed);
                        /*CLOSED PARENT*/
                        $this->LevelCompletedUpdated($data_completed);
                        
                        //Mail Function
                        $mail_data = array('customer_name'=>$user_data->first_name,'package_name'=>$subscription_master_level->package_name,'package_amount'=>$subscription_master_level->package_amount,'referalid'=>$subscription_table->referal_code);

                        $data = array("to"=>$user_data->email_id,'first_name'=>$user_data->first_name,"subject"=>"Abdhi Registeration","message"=>$this->MailTemplate($mail_data),'user_id'=>$subscription_table->user_id,'customer_name'=>$user_data->first_name,'package_name'=>$subscription_master_level->package_name,'package_amount'=>$subscription_master_level->package_amount,'referalid'=>$subscription_table->referal_code);
                        $this->EmailTemplate($data);

                        $sms_message = $this->RegisterationSMS($data);

                        $this->MessageIntegration($user_data->mobile_number,$sms_message,$data);

                        $transaction->commit();
                        unset($session[Yii::$app->params['inputname']]);
                        return $payment_insert->payment_id;    
                    }
                }
                else
                {
                    $transaction->rollback();
                    return 'error';
                }
            }
        }
        catch(Exception $e) 
        {
            $transaction->rollback();
            return true;
        }   
    }

    public function PaymentFailure($get_subscription_data)
    {
        $session = Yii::$app->session;
        $connection = \Yii::$app->db;        
        $transaction = $connection->beginTransaction();
        $session = Yii::$app->session;
        try 
        {

            $paymentLog = new PaymentLog();

            $paymentLog->user_id=$get_subscription_data->user_id;
            $paymentLog->subscripe_id=$get_subscription_data->subscripe_id;
            $paymentLog->order_id=NULL;
            $paymentLog->transaction_id=NULL;
            $paymentLog->paid_amount=0;
            $paymentLog->payment_status='failure';
            $paymentLog->status='1';
            $paymentLog->created_at=Yii::$app->params['date'];
            $paymentLog->updated_at=Yii::$app->params['date'];

            if($paymentLog->save())
            {
                $get_subscription_data->payment_status = 'PAYMENT_FAILURE';
                $get_subscription_data->mode = 'F';
                $get_subscription_data->current_status = '5';
                $get_subscription_data->reason = 'Payment Failure';
                if($get_subscription_data->save())
                {
                    if(!empty($get_subscription_data->refered_by_code))
                    {
                        $update_pending_count = $this->getSubscriptiondataupdatependingcount($get_subscription_data->refered_by_code); 
                        $get_count = ($update_pending_count['pending_users_count'] == 0 || $update_pending_count['pending_users_count'] == '' || $update_pending_count['pending_users_count'] == NULL) ? 0 : $update_pending_count['pending_users_count']-1;

                        SubscriptionTable::updateAll(['pending_users_count'=>$get_count],['subscripe_id'=>$update_pending_count['subscripe_id']]);
                    }
                    $transaction->commit();
                    return Yii::$app->params['response']['S'];
                }
                else
                {
                    $transaction->rollback();
                    return Yii::$app->params['response']['E'];
                }
            }
            else
            {
                $transaction->rollback();
                return Yii::$app->params['response']['E'];
            }
        }
        catch(Exception $e) 
        {
            $transaction->rollback();
            return Yii::$app->params['response']['E'];
        }
    }

    public function SubscriptionTablePaymentInsert($user)
    {
        $SubscriptionTable = new SubscriptionTable();
        $SubscriptionTable->random_subscribe_no = 'SUB'.$this->add_subscripe_random_no();
        $SubscriptionTable->user_id = $user['refferal_user_id'];
        $SubscriptionTable->subscription_master_id  = $user['subscripe_plan'];
        $SubscriptionTable->commision_data_structure_id   = $this->setCommisionStructureData();
        $SubscriptionTable->registeration_date   = Yii::$app->params['date'];
        $SubscriptionTable->current_status   = $user['current_status'];
        $SubscriptionTable->rejoin_date   = ($user['current_status'] == '3') ? Yii::$app->params['date'] : NULL;
        $SubscriptionTable->max_user_count = $this->getConfigData()['branch'];
        $SubscriptionTable->mode = $user['mode'];
        $SubscriptionTable->referal_code = $user['refferal_id_generated'];
        $SubscriptionTable->refered_by_code = $user['refferal_id'];
        $SubscriptionTable->refered_by_user = $this->getUserReferalNameSubscripttionwise($user['refferal_id'])['user_id'];
        $SubscriptionTable->payment_status = Yii::$app->params['payment_status'][0];
        $SubscriptionTable->created_at   = Yii::$app->params['date'];
        $SubscriptionTable->update_at   = Yii::$app->params['date'];
        $SubscriptionTable->user_type_data = (empty($user['refferal_id'])) ? 'P' : 'C';
        if($SubscriptionTable->save())
        {
            $this->call_procedure();
            SubscriptionTable::updateAll(['pending_users_count'=>$this->NewPendingReferalCountPayment($user['refferal_id'])],['referal_code'=>$user['refferal_id']]);
            return array('data'=>'success','id'=>$SubscriptionTable->subscripe_id);
        }
        else
        {
            return array('data'=>'error');
        }
    }


    public function LevelCompletedUpdated(array $data)
    {
    	$get_subscribe_id = ArrayHelper::map($data,'subscription_id','subscription_id');
    	$subscription_table = SubscriptionTable::find()->where(['IN','subscripe_id',$get_subscribe_id])->indexBy('subscripe_id')->asArray()->all();

    	$get_master_id = ArrayHelper::map($subscription_table,'subscription_master_id','subscription_master_id');
    	$subscripe_master = SubscriptionMaster::find()->where(['IN','subscription_id',$get_master_id])->indexBy('subscription_id')->asArray()->all();

    	$Level = LevelCompletionStatus::find()->select(['total_count'=>'COUNT(is_fully_completed)','subscription_id'=>'subscription_id'])->where(['is_fully_completed'=>'YES'])->andWhere(['IN','subscription_id',$get_subscribe_id])->groupBy(['subscription_id'])->asArray()->all();

    	foreach ($Level as $key => $value) 
    	{
    		if(array_key_exists($value['subscription_id'], $subscription_table))
    		{
    			$get_status = $subscription_table[$value['subscription_id']]['current_status'];
    			$master_id = $subscription_table[$value['subscription_id']]['subscription_master_id'];
    			
    			if($get_status == '2')
    			{
    				continue;
    			}

    			if(array_key_exists($master_id, $subscripe_master))
    			{
    				$get_user_level = $subscripe_master[$master_id]['level'];
    				$present_count = 'L'.$value['total_count'];
    				if($get_user_level == $present_count)
    				{


                        SubscriptionTable::updateAll(['current_status'=>'2'],['subscripe_id'=>$value['subscription_id']]);

                        $user_data = User::findOne($subscription_table[$value['subscription_id']]['user_id']);
                        
                        //Mail Function
                        $mail_data = array('customer_name'=>$user_data->first_name,'package_name'=>$subscripe_master[$master_id]['package_name'],'package_amount'=>$subscripe_master[$master_id]['package_amount'],'referalid'=>$subscription_table[$value['subscription_id']]['referal_code']);

                        $data = array("to"=>$user_data->email_id,'first_name'=>$user_data->first_name,"subject"=>"Abdhi Registeration","message"=>$this->MailClosedRegister($mail_data),'user_id'=>$subscription_table[$value['subscription_id']]['user_id'],'customer_name'=>$user_data->first_name,'package_name'=>$subscripe_master[$master_id]['package_name'],'package_amount'=>$subscripe_master[$master_id]['package_amount'],'referalid'=>$subscription_table[$value['subscription_id']]['referal_code']);

                        $this->EmailTemplate($data);

                        $sms_message = $this->ClosedRegisterationSMS($data);

                        $this->MessageIntegration($user_data->mobile_number,$sms_message,$data);
    				}
    			}
    		}		
    	}
    }


    /* Configuration Validation Token */
    protected function Checkvalidtoken($tokenarray)
    {
        $session = Yii::$app->session;
        return ($session[Yii::$app->params['inputname']] == $tokenarray[Yii::$app->params['inputname']]) ?  true :  false;
    }

    // To Increment Subscribe Random Number
    protected function call_procedure()
    {
        $set_unique_no = \Yii::$app->db->createCommand("CALL increment_product_no()")->execute();
    }
}

<?php
namespace common\models;

use Yii;
use yii\base\Model;
use  yii\web\Session;
use common\models\SwimServicecenterlogin;
use backend\models\CompanyMaster;
/**
 * Login form
 */
class LoginForm extends Model
{
    public $email_id;
    public $password;
    public $rememberMe = true;

    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email_id', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // username is validated by validateUsername()
            ['email_id', 'validateUsername'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],

        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateUsername($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user) {
                $this->addError($attribute, 'Invalid Username.');
            }
        }
    }


    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if(!$user->validatePassword($this->password))
            {
                $this->addError($attribute, 'Invalid Password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        $session = Yii::$app->session;
        unset($session['user_id']);
        unset($session['email_id']);
        $session->destroy();
               
        $email_id = $_REQUEST['LoginForm']['email_id'];
        if ($this->validate()) {
            $user_data = User::find()->where(['email_id' => $email_id])->one();
            if(!empty($user_data))
            {
                $session['user_id']  = $user_data->id;
                $session['user_type']  = $user_data->user_type;
                $session['email_id']  = $user_data->email_id;
                $session['user_role']  = $user_data->user_role;   
                return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
            }
            else
            {
                return false;                
            }
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            //$this->_user = User::findByUsername($this->email_id);
            $user = User::find()->where(['email_id'=>$this->email_id])->one();
            $this->_user = $user;            
        }

        return $this->_user;
    }
}

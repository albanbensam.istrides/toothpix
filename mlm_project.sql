-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3307
-- Generation Time: Feb 08, 2021 at 01:11 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mlm_project`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `increment_product_no` ()  NO SQL
BEGIN
DECLARE increment INT;	
SET increment = (SELECT random_id from random_generation where id=1)+1;
UPDATE random_generation SET random_id=increment WHERE id=1;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `binary_user`
--

CREATE TABLE `binary_user` (
  `auto_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `subscription_id` int(11) DEFAULT NULL,
  `level` varchar(20) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `parent_subscription_id` int(11) DEFAULT NULL,
  `side` enum('R','L') DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `status` enum('0','1','2') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `binary_user`
--

INSERT INTO `binary_user` (`auto_id`, `user_id`, `subscription_id`, `level`, `parent_id`, `parent_subscription_id`, `side`, `position`, `status`, `created_at`, `updated_at`) VALUES
(300, 495, 93, 'L0', NULL, NULL, NULL, 1, '1', '2020-09-23 17:00:38', '2020-09-23 17:00:38'),
(301, 497, 95, 'L1', 495, 93, 'R', 2, '1', '2020-09-23 17:04:53', '2020-09-23 17:04:53'),
(302, 498, 96, 'L1', 495, 93, 'L', 3, '1', '2020-09-23 17:08:27', '2020-09-23 17:08:27');

-- --------------------------------------------------------

--
-- Table structure for table `commision_report`
--

CREATE TABLE `commision_report` (
  `auto_id` int(11) NOT NULL,
  `subscription_tbl_id` int(11) DEFAULT NULL,
  `level_id` int(11) DEFAULT NULL,
  `pair` text,
  `commision_value` float DEFAULT NULL,
  `commision_settled` enum('YES','NO') DEFAULT NULL,
  `is_pair_completed` enum('YES') DEFAULT NULL,
  `payment_id` int(11) DEFAULT NULL,
  `status` enum('0','1','2') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `commision_report`
--

INSERT INTO `commision_report` (`auto_id`, `subscription_tbl_id`, `level_id`, `pair`, `commision_value`, `commision_settled`, `is_pair_completed`, `payment_id`, `status`, `created_at`, `updated_at`) VALUES
(12, 93, 1, '2,3', 500, 'NO', 'YES', NULL, '1', '2020-09-23 17:08:27', '2020-09-23 17:08:27');

-- --------------------------------------------------------

--
-- Table structure for table `commission_data_structure`
--

CREATE TABLE `commission_data_structure` (
  `id` int(11) NOT NULL,
  `commision_data` text,
  `status` enum('0','1') NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `commission_data_structure`
--

INSERT INTO `commission_data_structure` (`id`, `commision_data`, `status`, `created_at`, `updated_at`) VALUES
(1, '{\"Level 1\":\"200\",\"Level 2\":\"230\",\"Level 3\":\"300\",\"Level 4\":\"400\",\"Level 5\":\"500\",\"Level 6\":\"600\"}', '0', '2020-07-23 17:30:11', '2020-07-23 17:30:11'),
(2, '{\"Level 1\":\"200\",\"Level 2\":\"230\",\"Level 3\":\"350\",\"Level 4\":\"400\",\"Level 5\":\"500\",\"Level 6\":\"600\"}', '0', '2020-07-23 17:30:45', '2020-07-23 17:31:54'),
(3, '{\"Level 1\":\"200\",\"Level 2\":\"230\",\"Level 3\":\"350\",\"Level 4\":\"450\",\"Level 5\":\"500\",\"Level 6\":\"600\"}', '0', '2020-07-23 17:31:54', '2020-07-24 08:59:09'),
(4, '{\"Level 1\":\"200\",\"Level 2\":\"230\",\"Level 3\":\"350\",\"Level 4\":\"450\",\"Level 5\":\"500\",\"Level 6\":\"600\"}', '0', '2020-07-24 08:59:09', '2020-07-24 08:59:26'),
(5, '{\"Level 1\":\"210\",\"Level 2\":\"230\",\"Level 3\":\"350\",\"Level 4\":\"450\",\"Level 5\":\"500\",\"Level 6\":\"600\"}', '0', '2020-07-24 08:59:26', '2020-07-24 09:00:12'),
(6, '{\"Level 1\":\"220\",\"Level 2\":\"230\",\"Level 3\":\"350\",\"Level 4\":\"450\",\"Level 5\":\"500\",\"Level 6\":\"600\"}', '0', '2020-07-24 09:00:12', '2020-07-24 09:01:01'),
(7, '{\"Level 1\":\"220\",\"Level 2\":\"245\",\"Level 3\":\"350\",\"Level 4\":\"450\",\"Level 5\":\"500\",\"Level 6\":\"600\"}', '0', '2020-07-24 09:01:01', '2020-07-24 09:06:06'),
(8, '{\"Level 1\":\"245\",\"Level 2\":\"255\",\"Level 3\":\"350\",\"Level 4\":\"450\",\"Level 5\":\"500\",\"Level 6\":\"600\"}', '0', '2020-07-24 09:06:06', '2020-07-28 07:13:59'),
(9, '{\"Level 1\":\"245\",\"Level 2\":\"255\",\"Level 3\":\"350\",\"Level 4\":\"450\",\"Level 5\":\"500\",\"Level 6\":\"600\"}', '0', '2020-07-28 07:13:59', '2020-07-28 07:14:39'),
(10, '{\"Level 1\":\"250\",\"Level 2\":\"255\",\"Level 3\":\"350\",\"Level 4\":\"450\",\"Level 5\":\"500\",\"Level 6\":\"600\"}', '0', '2020-07-28 07:14:39', '2020-07-29 17:14:46'),
(11, '{\"Level 1\":\"250\",\"Level 2\":\"255\",\"Level 3\":\"350\",\"Level 4\":\"450\",\"Level 5\":\"500\",\"Level 6\":\"600\"}', '0', '2020-07-29 17:14:46', '2020-07-29 17:14:58'),
(12, '{\"Level 1\":\"250\",\"Level 2\":\"255\",\"Level 3\":\"350\",\"Level 4\":\"450\",\"Level 5\":\"500\",\"Level 6\":\"600\"}', '0', '2020-07-29 17:14:58', '2020-07-29 17:15:05'),
(13, '{\"Level 1\":\"250\",\"Level 2\":\"255\",\"Level 3\":\"350\",\"Level 4\":\"450\",\"Level 5\":\"400\",\"Level 6\":\"600\"}', '0', '2020-07-29 17:15:05', '2020-07-29 18:01:54'),
(14, '{\"Level 1\":\"250\",\"Level 2\":\"255\",\"Level 3\":\"350\",\"Level 4\":\"450\",\"Level 5\":\"400\",\"Level 6\":\"600\"}', '0', '2020-07-29 18:01:54', '2020-07-30 16:31:35'),
(15, '{\"Level 1\":\"269\",\"Level 2\":\"255\",\"Level 3\":\"350\",\"Level 4\":\"450\",\"Level 5\":\"400\",\"Level 6\":\"600\"}', '0', '2020-07-30 16:31:35', '2020-07-30 16:33:24'),
(16, '{\"Level 1\":\"280\",\"Level 2\":\"255\",\"Level 3\":\"350\",\"Level 4\":\"450\",\"Level 5\":\"400\",\"Level 6\":\"600\"}', '0', '2020-07-30 16:33:24', '2020-07-30 16:34:00'),
(17, '{\"Level 1\":\"270\",\"Level 2\":\"255\",\"Level 3\":\"350\",\"Level 4\":\"450\",\"Level 5\":\"400\",\"Level 6\":\"600\"}', '0', '2020-07-30 16:34:00', '2020-07-30 16:34:10'),
(18, '{\"Level 1\":\"250\",\"Level 2\":\"255\",\"Level 3\":\"350\",\"Level 4\":\"450\",\"Level 5\":\"400\",\"Level 6\":\"600\"}', '0', '2020-07-30 16:34:10', '2020-07-30 16:35:25'),
(19, '{\"Level 1\":\"250\",\"Level 2\":\"260\",\"Level 3\":\"350\",\"Level 4\":\"450\",\"Level 5\":\"400\",\"Level 6\":\"600\"}', '0', '2020-07-30 16:35:25', '2020-07-30 16:35:36'),
(20, '{\"Level 1\":\"250\",\"Level 2\":\"260\",\"Level 3\":\"370\",\"Level 4\":\"450\",\"Level 5\":\"400\",\"Level 6\":\"600\"}', '0', '2020-07-30 16:35:36', '2020-07-30 16:35:52'),
(21, '{\"Level 1\":\"250\",\"Level 2\":\"260\",\"Level 3\":\"370\",\"Level 4\":\"380\",\"Level 5\":\"400\",\"Level 6\":\"600\"}', '0', '2020-07-30 16:35:52', '2020-09-05 09:42:42'),
(22, '{\"Level 1\":\"250\",\"Level 2\":\"260\",\"Level 3\":\"370\",\"Level 4\":\"380\",\"Level 5\":\"400\",\"Level 6\":\"600\"}', '1', '2020-09-05 09:42:42', '2020-09-05 09:42:42');

-- --------------------------------------------------------

--
-- Table structure for table `commission_structure`
--

CREATE TABLE `commission_structure` (
  `id` int(11) NOT NULL,
  `level` varchar(50) DEFAULT NULL,
  `commision` float DEFAULT NULL COMMENT 'Flat Value',
  `status` enum('0','1') DEFAULT NULL COMMENT '0-In-active,1-Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `no_of_pairs` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `commission_structure`
--

INSERT INTO `commission_structure` (`id`, `level`, `commision`, `status`, `created_at`, `updated_at`, `no_of_pairs`) VALUES
(1, 'Level 1', 250, '1', NULL, NULL, 1),
(2, 'Level 2', 260, '1', NULL, NULL, 2),
(3, 'Level 3', 370, '1', NULL, NULL, 4),
(4, 'Level 4', 380, '1', NULL, NULL, 8),
(5, 'Level 5', 400, '1', NULL, NULL, 16),
(6, 'Level 6', 600, '1', NULL, NULL, 32);

-- --------------------------------------------------------

--
-- Table structure for table `configuration_table`
--

CREATE TABLE `configuration_table` (
  `id` int(11) NOT NULL,
  `config_name` varchar(255) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `status` enum('0','1') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `configuration_table`
--

INSERT INTO `configuration_table` (`id`, `config_name`, `key`, `value`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Levels', 'level', '6', '1', NULL, NULL),
(2, 'Branch', 'branch', '2', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `level_completion_status`
--

CREATE TABLE `level_completion_status` (
  `id` int(11) NOT NULL,
  `subscription_id` int(11) DEFAULT NULL,
  `no_of_levels` int(11) DEFAULT NULL,
  `total_no_of_pairs_each_level` int(11) DEFAULT NULL,
  `no_of_pairs_completed` int(11) DEFAULT NULL,
  `total_no_of_members` int(11) DEFAULT NULL,
  `total_no_of_members_joined` int(11) DEFAULT NULL,
  `accumulate_price` float DEFAULT NULL,
  `paid_amount` float DEFAULT NULL,
  `is_fully_completed` enum('NO','YES') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `level_completion_status`
--

INSERT INTO `level_completion_status` (`id`, `subscription_id`, `no_of_levels`, `total_no_of_pairs_each_level`, `no_of_pairs_completed`, `total_no_of_members`, `total_no_of_members_joined`, `accumulate_price`, `paid_amount`, `is_fully_completed`, `created_at`, `updated_at`) VALUES
(823, 93, 1, 1, 1, 2, 2, 500, NULL, 'YES', '2020-09-23 17:00:38', '2020-09-23 17:00:38'),
(824, 93, 2, 2, 0, 4, NULL, NULL, NULL, 'NO', '2020-09-23 17:00:38', '2020-09-23 17:00:38'),
(825, 93, 3, 4, 0, 8, NULL, NULL, NULL, 'NO', '2020-09-23 17:00:38', '2020-09-23 17:00:38'),
(826, 95, 1, 1, 0, 2, NULL, NULL, NULL, 'NO', '2020-09-23 17:04:53', '2020-09-23 17:04:53'),
(827, 95, 2, 2, 0, 4, NULL, NULL, NULL, 'NO', '2020-09-23 17:04:53', '2020-09-23 17:04:53'),
(828, 95, 3, 4, 0, 8, NULL, NULL, NULL, 'NO', '2020-09-23 17:04:53', '2020-09-23 17:04:53'),
(829, 96, 1, 1, 0, 2, NULL, NULL, NULL, 'NO', '2020-09-23 17:08:27', '2020-09-23 17:08:27'),
(830, 96, 2, 2, 0, 4, NULL, NULL, NULL, 'NO', '2020-09-23 17:08:27', '2020-09-23 17:08:27'),
(831, 96, 3, 4, 0, 8, NULL, NULL, NULL, 'NO', '2020-09-23 17:08:27', '2020-09-23 17:08:27');

-- --------------------------------------------------------

--
-- Table structure for table `pair_table`
--

CREATE TABLE `pair_table` (
  `id` int(11) NOT NULL,
  `level` int(11) DEFAULT NULL,
  `pair_1` int(11) DEFAULT NULL,
  `pair_2` int(11) DEFAULT NULL,
  `status` enum('0','1') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `payment_log`
--

CREATE TABLE `payment_log` (
  `payment_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `subscripe_id` int(11) DEFAULT NULL,
  `order_id` text,
  `transaction_id` text,
  `paid_amount` float DEFAULT NULL,
  `payment_status` enum('success','failure') DEFAULT NULL,
  `status` enum('0','1','2') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payment_log`
--

INSERT INTO `payment_log` (`payment_id`, `user_id`, `subscripe_id`, `order_id`, `transaction_id`, `paid_amount`, `payment_status`, `status`, `created_at`, `updated_at`) VALUES
(136, 495, 93, 'aa', '123456', 100, 'success', '1', '2020-09-23 17:00:38', '2020-09-23 17:00:38'),
(137, 497, 95, 'aa', '123456', 100, 'success', '1', '2020-09-23 17:04:53', '2020-09-23 17:04:53'),
(138, 498, 96, 'aa', '123456', 100, 'success', '1', '2020-09-23 17:08:27', '2020-09-23 17:08:27');

-- --------------------------------------------------------

--
-- Table structure for table `random_generation`
--

CREATE TABLE `random_generation` (
  `id` int(11) NOT NULL,
  `random_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `random_generation`
--

INSERT INTO `random_generation` (`id`, `random_id`) VALUES
(1, 100092);

-- --------------------------------------------------------

--
-- Table structure for table `subscription_master`
--

CREATE TABLE `subscription_master` (
  `subscription_id` int(11) NOT NULL,
  `package_name` varchar(255) DEFAULT NULL,
  `package_amount` float DEFAULT NULL,
  `level` varchar(20) DEFAULT NULL,
  `status` enum('0','1','2') DEFAULT NULL,
  `description` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `subscription_master`
--

INSERT INTO `subscription_master` (`subscription_id`, `package_name`, `package_amount`, `level`, `status`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Package 5', 100, 'L5', '1', '', '2020-07-24 10:24:05', '2020-09-23 15:23:58'),
(2, 'Package 6', 200, 'L6', '1', '', '2020-07-24 10:24:21', '2020-09-23 15:23:34'),
(3, 'Package 3', 5000, 'L3', '1', '', '2020-09-23 15:22:59', '2020-09-23 15:22:59'),
(4, 'Package 4', 6000, 'L4', '1', '', '2020-09-23 15:23:21', '2020-09-23 15:23:21');

-- --------------------------------------------------------

--
-- Table structure for table `subscription_table`
--

CREATE TABLE `subscription_table` (
  `subscripe_id` int(11) NOT NULL,
  `random_subscribe_no` varchar(50) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `subscription_master_id` int(11) DEFAULT NULL,
  `commision_data_structure_id` int(11) DEFAULT NULL,
  `registeration_date` datetime DEFAULT NULL,
  `activation_date` datetime DEFAULT NULL,
  `elimination_date` datetime DEFAULT NULL,
  `closing_date` datetime DEFAULT NULL,
  `rejoin_date` datetime DEFAULT NULL,
  `rejected_date` datetime DEFAULT NULL,
  `max_user_count` int(11) DEFAULT NULL,
  `total_approved_user` int(11) DEFAULT NULL,
  `pending_users_count` int(11) DEFAULT NULL,
  `user_type_data` enum('P','C') DEFAULT NULL,
  `referal_code` varchar(30) DEFAULT NULL,
  `refered_by_code` varchar(30) DEFAULT NULL,
  `refered_by_user` int(11) DEFAULT NULL,
  `payment_status` enum('PAYMENT_PENDING','PAYMENT_FAILURE','PAYMENT_SUCCESS') DEFAULT NULL,
  `mode` enum('C','E','O','A','R','F') DEFAULT NULL COMMENT 'C-Create,E-Eliminate,O-Closed,A-Approved,R-Request,J-Rejected,F=Payment Failure',
  `current_status` enum('0','1','2','3','4','5') DEFAULT NULL COMMENT '0=Eliminated,1=Active,2=Closed,3=Rejoin Request,4=Rejoin Rejected,5=Payment Failure',
  `reason` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `update_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `subscription_table`
--

INSERT INTO `subscription_table` (`subscripe_id`, `random_subscribe_no`, `user_id`, `subscription_master_id`, `commision_data_structure_id`, `registeration_date`, `activation_date`, `elimination_date`, `closing_date`, `rejoin_date`, `rejected_date`, `max_user_count`, `total_approved_user`, `pending_users_count`, `user_type_data`, `referal_code`, `refered_by_code`, `refered_by_user`, `payment_status`, `mode`, `current_status`, `reason`, `created_at`, `update_at`) VALUES
(93, 'SUB100069', 495, 3, 22, '2020-09-23 22:29:08', '2020-09-23 22:30:38', NULL, NULL, NULL, NULL, 2, 2, 0, 'P', '5uJuj', '', NULL, 'PAYMENT_SUCCESS', 'C', '1', NULL, '2020-09-23 16:59:08', '2020-09-23 16:59:08'),
(95, 'SUB100071', 497, 3, 22, '2020-09-23 22:32:55', '2020-09-23 22:34:53', NULL, NULL, NULL, NULL, 2, NULL, 2, 'C', 'uatfC', '5uJuj', 495, 'PAYMENT_SUCCESS', 'C', '1', NULL, '2020-09-23 17:02:55', '2020-09-23 17:02:55'),
(96, 'SUB100072', 498, 3, 22, '2020-09-23 22:37:51', '2020-09-23 22:38:27', NULL, NULL, NULL, NULL, 2, NULL, 2, 'C', 'gdHXq', '5uJuj', 495, 'PAYMENT_SUCCESS', 'C', '1', NULL, '2020-09-23 17:07:51', '2020-09-23 17:07:51'),
(99, 'SUB100075', 501, 3, 22, '2020-09-24 15:54:42', NULL, NULL, NULL, NULL, NULL, 2, NULL, 2, 'C', 'fx9Gi', 'uatfC', 497, 'PAYMENT_PENDING', 'C', '1', NULL, '2020-09-24 10:24:42', '2020-09-24 10:24:42'),
(100, 'SUB100076', 502, 3, 22, '2020-09-24 15:55:11', NULL, NULL, NULL, NULL, NULL, 2, NULL, 2, 'C', 'ltEKX', 'uatfC', 497, 'PAYMENT_PENDING', 'C', '1', NULL, '2020-09-24 10:25:11', '2020-09-24 10:25:11'),
(103, 'SUB100079', 505, 3, 22, '2020-09-24 16:03:38', NULL, NULL, NULL, NULL, NULL, 2, NULL, 1, 'C', 't5eTz', 'gdHXq', 498, 'PAYMENT_PENDING', 'C', '1', NULL, '2020-09-24 10:33:38', '2020-09-24 10:33:38'),
(104, 'SUB100080', 506, 3, 22, '2020-09-24 16:04:01', NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 'C', 'VPx8G', 'gdHXq', 498, 'PAYMENT_PENDING', 'C', '1', NULL, '2020-09-24 10:34:01', '2020-09-24 10:34:01'),
(105, 'SUB100081', 507, 3, 22, '2020-09-24 16:04:21', NULL, NULL, NULL, NULL, NULL, 2, NULL, 1, 'C', 'HvGks', 'fx9Gi', 501, 'PAYMENT_PENDING', 'C', '1', NULL, '2020-09-24 10:34:21', '2020-10-03 06:44:40'),
(106, 'SUB100082', 508, 3, 22, '2020-09-24 16:07:34', NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 'C', '5ii2e', 'fx9Gi', 501, 'PAYMENT_PENDING', 'C', '1', NULL, '2020-09-24 10:37:34', '2020-09-24 10:37:34'),
(107, 'SUB100083', 509, 3, 22, '2020-09-24 16:08:47', NULL, NULL, NULL, NULL, NULL, 2, NULL, 2, 'C', 'gs3rr', 'ltEKX', 502, 'PAYMENT_PENDING', 'C', '1', NULL, '2020-09-24 10:38:47', '2020-09-24 10:38:47'),
(108, 'SUB100084', 510, 3, 22, '2020-09-24 16:10:05', NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 'C', 'JOTI2', 'HvGks', 507, 'PAYMENT_PENDING', 'C', '1', NULL, '2020-09-24 10:40:05', '2020-09-24 10:40:05'),
(109, 'SUB100085', 511, 3, 22, '2020-09-24 16:22:39', NULL, NULL, NULL, NULL, NULL, 2, NULL, 2, 'C', 'AC3Hl', 'ltEKX', 502, 'PAYMENT_PENDING', 'C', '1', NULL, '2020-09-24 10:52:39', '2020-10-03 06:44:29'),
(110, 'SUB100086', 512, 3, 22, '2020-09-24 16:23:19', NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 'C', 'FvwKe', 't5eTz', 505, 'PAYMENT_PENDING', 'C', '1', NULL, '2020-09-24 10:53:19', '2020-09-24 10:53:19'),
(111, 'SUB100087', 513, 3, 22, '2020-09-24 16:46:35', NULL, NULL, NULL, NULL, NULL, 2, NULL, 1, 'C', 'TWxIB', 'gs3rr', 509, 'PAYMENT_PENDING', 'C', '1', NULL, '2020-09-24 11:16:35', '2020-09-24 11:16:35'),
(112, 'SUB100088', 514, 3, 22, '2020-09-24 16:52:15', NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 'C', 'ywNqO', 'gs3rr', 509, 'PAYMENT_PENDING', 'C', '1', NULL, '2020-09-24 11:22:15', '2020-09-24 11:22:15'),
(113, 'SUB100089', 515, 3, 22, '2020-09-24 16:52:52', NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 'C', 'LNeyd', 'AC3Hl', 511, 'PAYMENT_PENDING', 'C', '1', NULL, '2020-09-24 11:22:52', '2020-09-24 11:22:52'),
(114, 'SUB100090', 516, 3, 22, '2020-09-24 16:53:11', NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 'C', 'pwFhm', 'AC3Hl', 511, 'PAYMENT_PENDING', 'C', '1', NULL, '2020-09-24 11:23:11', '2020-09-24 11:23:11'),
(115, 'SUB100091', 517, 3, 22, '2020-09-24 16:53:48', NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 'C', '3TNIQ', 'TWxIB', 513, 'PAYMENT_PENDING', 'C', '1', NULL, '2020-09-24 11:23:48', '2020-10-13 13:20:50');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `user_type` enum('A','U','P') COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'A=Admin, U=User, P=Player',
  `city` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile_pdf` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `rights` text COLLATE utf8_unicode_ci,
  `status_flag` enum('A','I') COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'A=Active, I=Inactive',
  `user_level` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `designation` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `aadhar_number` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subscripe_plan` int(11) DEFAULT NULL,
  `refferal_id` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `refferal_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_account_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ifsc` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `branch` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type_data` enum('P','C','O') COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'P=Parent,C=Child,O=Closed',
  `create_at` timestamp NULL DEFAULT NULL,
  `update_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `first_name`, `last_name`, `dob`, `user_type`, `city`, `auth_key`, `profile_image`, `profile_pdf`, `password_hash`, `password_reset_token`, `status`, `created_at`, `updated_at`, `rights`, `status_flag`, `user_level`, `mobile_number`, `designation`, `user_role`, `address`, `aadhar_number`, `subscripe_plan`, `refferal_id`, `refferal_name`, `bank_name`, `bank_account_no`, `ifsc`, `branch`, `email_id`, `user_type_data`, `create_at`, `update_at`) VALUES
(1, 'mlm', 'admin', 'a', '2019-11-01', 'A', 'Chennai', 'VVXWUK7plyXtnf47nHElnAQxEUoEvO4f', NULL, NULL, '$2y$13$ZLZaGB4c5p3dxFgDyG.Qi.4Fc7wxYw1kUVP/CHsVZC.PvZnHTm6r.', NULL, 10, 1567240418, 1567240418, NULL, 'A', '2', '24578887841', 'Developer', 'S', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'mlm@gmail.com', NULL, NULL, NULL),
(495, NULL, 'A', NULL, NULL, NULL, NULL, 'CJzTEtxWJa0wu6hZ2gcv_Ljah1EC_nbf', '/web/uploads/0rCFzRIdriverArun_Madhan.jpg', '/web/uploads/6Mw6LwqdriverEVK_LINGAM.jpg', '$2y$13$fleWBhRNkSUCfhB3pXvYz.ilFy2w/YF.WCeVlrrSTEyIfTX3qsKH.', NULL, 10, NULL, NULL, NULL, NULL, NULL, '9788440809', NULL, NULL, 'aaa', '925666336954', 3, '5uJuj', NULL, 'AAA', 'AAA', 'AAA', 'AAA', 'c1@gmail.com', 'P', '2020-09-23 16:59:08', '2020-09-23 16:59:08'),
(497, NULL, 'c2', NULL, NULL, NULL, NULL, '1uaR7M14Af5lefqn_0-y_wfGyguznIjG', NULL, NULL, '$2y$13$r7jfvq/VEXX9C7RfaxJUUe7sueBQUPMdP2QjgpCsoS4H2n1Tfkv6u', NULL, 10, NULL, NULL, NULL, NULL, NULL, '9788440809', NULL, NULL, 'AXIS', '925666335954', 3, 'uatfC', NULL, 'AXIS', 'AXIS', 'AXIS', 'AXIS', 'c2@gmail.com', 'C', '2020-09-23 17:02:55', '2020-09-23 17:02:55'),
(498, NULL, 'c3', NULL, NULL, NULL, NULL, '70C-GSG6GSRfNqT1_IYvkhnvu9nkvtal', NULL, NULL, '$2y$13$t5RGcRhlrL2kjnskdzvNOu0zARtaS1Pp0i9fViz.VefK/7.7cZnUG', NULL, 10, NULL, NULL, NULL, NULL, NULL, '9788440809', NULL, NULL, 'AXIS', '925666335954', 3, 'gdHXq', NULL, 'AXIS', 'AXIS', 'AXIS', 'AXIS', 'c3@gmail.com', 'C', '2020-09-23 17:07:51', '2020-09-23 17:07:51'),
(501, NULL, 'c5', NULL, NULL, NULL, NULL, '3ET1XWkMH4auMWzb8LfSlmCyPl75dfZM', NULL, NULL, '$2y$13$grv4wjm/4ofbbUbT9VPIeOeYqImAszkamQHVK/n9TlogjfYzOXsUe', NULL, 10, NULL, NULL, NULL, NULL, NULL, '9788440809', NULL, NULL, 'AXIS', '925666335954', 3, 'fx9Gi', NULL, 'AXIS', 'AXIS', 'AXIS', 'AXIS', 'c5@gmail.com', 'C', '2020-09-24 10:24:42', '2020-09-24 10:24:42'),
(502, NULL, 'c6', NULL, NULL, NULL, NULL, 'wFPSr3YShdYvr9a_00AT9PH9tFnVzsld', NULL, NULL, '$2y$13$4YYsgfH9hVvkspkLKWwhy.5sgdRS07ORDCgVJaTsyJQqKJteZ07k2', NULL, 10, NULL, NULL, NULL, NULL, NULL, '9788440809', NULL, NULL, 'AXIS', '925666335954', 3, 'ltEKX', NULL, 'AXIS', 'AXIS', 'AXIS', 'AXIS', 'c6@gmail.com', 'C', '2020-09-24 10:25:11', '2020-09-24 10:25:11'),
(505, NULL, 'c7', NULL, NULL, NULL, NULL, 'YZl3IrwrA_afJoXOcbxCfdL7WX7mQ20z', NULL, NULL, '$2y$13$GtSg7qF3Ckinq/iw.CL7ZOrJU7Stind.C8GpfSof7YUtAWxJO8JpK', NULL, 10, NULL, NULL, NULL, NULL, NULL, '9788440809', NULL, NULL, 'AXIS', '925666335954', 3, 't5eTz', NULL, 'AXIS', 'AXIS', 'AXIS', 'AXIS', 'c7@gmail.com', 'C', '2020-09-24 10:33:38', '2020-09-24 10:33:38'),
(506, NULL, 'c8', NULL, NULL, NULL, NULL, 'mPJCKQebE1s4Gtah1Gqri4N6Xylapqgm', NULL, NULL, '$2y$13$6/Zw5PpNvTv.D8y7EsVuxuaZTZcdTZc68H7S/2cpGqzPVB0hYxdRO', NULL, 10, NULL, NULL, NULL, NULL, NULL, '9788440809', NULL, NULL, 'AXIS', '925666335954', 3, 'VPx8G', NULL, 'AXIS', 'AXIS', 'AXIS', 'AXIS', 'c8@gmail.com', 'C', '2020-09-24 10:34:01', '2020-09-24 10:34:01'),
(507, NULL, 'c9', NULL, NULL, NULL, NULL, 'vimipcpCotgYdwrlGEhAoBciJ6Gh2qXf', NULL, NULL, '$2y$13$pxexcWzDbG4S3hpWdFimUem0q/IyhktPtMcjv/pQpN3OkTdebT5lu', NULL, 10, NULL, NULL, NULL, NULL, NULL, '9788440809', NULL, NULL, 'AXIS', '925666335954', 3, 'HvGks', NULL, 'AXIS', 'AXIS', 'AXIS', 'AXIS', 'c9@gmail.com', 'P', '2020-09-24 10:34:21', '2020-10-03 06:44:40'),
(508, NULL, 'c10', NULL, NULL, NULL, NULL, 'yVsn6TVVC9N6QPESLex0vBsIILJVZyzE', NULL, NULL, '$2y$13$HPiA.rYTgtG5N.VGjzsFnulsk08/ML/gQo3U1UZGzNnpGTVGnSQ/2', NULL, 10, NULL, NULL, NULL, NULL, NULL, '9788440809', NULL, NULL, 'AXIS', '925666335954', 3, '5ii2e', NULL, 'AXIS', 'AXIS', 'AXIS', 'AXIS', 'c10@gmail.com', 'C', '2020-09-24 10:37:34', '2020-09-24 10:37:34'),
(509, NULL, 'c11', NULL, NULL, NULL, NULL, 'Dy9Tp0x_Y05q-6U8KM5ybcza7fBFmtNB', NULL, NULL, '$2y$13$.nNCxGuZQl/uwn.C1wVcA.JCtnwvOrnEVXeamRzfE9.I56oJKQXZe', NULL, 10, NULL, NULL, NULL, NULL, NULL, '9788440809', NULL, NULL, 'AXIS', '925666335954', 3, 'gs3rr', NULL, 'AXIS', 'AXIS', 'AXIS', 'AXIS', 'c11@gmail.com', 'C', '2020-09-24 10:38:47', '2020-09-24 10:38:47'),
(510, NULL, 'c12', NULL, NULL, NULL, NULL, '0ndV9Iwirjy878vDjrWE8Br4ikDnskrt', NULL, NULL, '$2y$13$0hFGtE6Qasv0q0InST8bF.CTs2fkD3z6IokYdRQVg9THmDvV1Dsmy', NULL, 10, NULL, NULL, NULL, NULL, NULL, '9788440809', NULL, NULL, 'AXIS', '925666335954', 3, 'JOTI2', NULL, 'AXIS', 'AXIS', 'AXIS', 'AXIS', 'c12@gmail.com', 'C', '2020-09-24 10:40:05', '2020-09-24 10:40:05'),
(511, NULL, 'c13', NULL, NULL, NULL, NULL, 'v0XQW4pr__dN8zvehzNWpq34-0F7pu00', NULL, NULL, '$2y$13$MYP9z.1iJ4EXyKUH1jej4u87NMSIzBSEYcpHJozrfpJAupJXrfMe6', NULL, 10, NULL, NULL, NULL, NULL, NULL, '9788440809', NULL, NULL, 'AXIS', '925666335954', 3, 'AC3Hl', NULL, 'AXIS', 'AXIS', 'AXIS', 'AXIS', 'c13@gmail.com', 'P', '2020-09-24 10:52:39', '2020-10-03 06:44:29'),
(512, NULL, 'c14', NULL, NULL, NULL, NULL, 'u7HUL3lZL_9vX55mqrVycX9ZPq4AJOLo', NULL, NULL, '$2y$13$YVchKqHfeXnQV10I6/QzeOzK0Ah6WKp1YloiD/GSpTgBPsm2DkH3e', NULL, 10, NULL, NULL, NULL, NULL, NULL, '9788440809', NULL, NULL, 'AXIS', '925666335954', 3, 'FvwKe', NULL, 'AXIS', 'AXIS', 'AXIS', 'AXIS', 'c14@gmail.com', 'C', '2020-09-24 10:53:19', '2020-09-24 10:53:19'),
(513, NULL, 'c15', NULL, NULL, NULL, NULL, 'kzfccqzRu612oiIc5YnghofHRNtGIZch', NULL, NULL, '$2y$13$Zt8HVOqjhoZX6QxiYIsxsuktLn3p9LfdVbv6xyp0B1iRmrXNaIgIW', NULL, 10, NULL, NULL, NULL, NULL, NULL, '9788440809', NULL, NULL, 'AXIS', '925666335954', 3, 'TWxIB', NULL, 'AXIS', 'AXIS', 'AXIS', 'AXIS', 'c15@gmail.com', 'C', '2020-09-24 11:16:35', '2020-09-24 11:16:35'),
(514, NULL, 'c16', NULL, NULL, NULL, NULL, 'vyAmok14bFwPAlIRrvYumhtP6_ZaiONH', NULL, NULL, '$2y$13$4iyHE1aIBMGV30OYdWzDguwiV/K2VXLlPfpcPsSE.hYog2/ZlWGqi', NULL, 10, NULL, NULL, NULL, NULL, NULL, '9788440809', NULL, NULL, 'AXIS', '925666335954', 3, 'ywNqO', NULL, 'AXIS', 'AXIS', 'AXIS', 'AXIS', 'c16@gmail.com', 'C', '2020-09-24 11:22:15', '2020-09-24 11:22:15'),
(515, NULL, 'c17', NULL, NULL, NULL, NULL, '8-gMXqpWuNmyLFTA2v8tYTGOeiJatNDq', NULL, NULL, '$2y$13$8ukoVJAZQMaN0eAtQU8jx.R3NogJ9gjry4iPHSiYNLZYQbnzFhyVS', NULL, 10, NULL, NULL, NULL, NULL, NULL, '9788440809', NULL, NULL, 'AXIS', '925666335954', 3, 'LNeyd', NULL, 'AXIS', 'AXIS', 'AXIS', 'AXIS', 'c17@gmail.com', 'C', '2020-09-24 11:22:52', '2020-09-24 11:22:52'),
(516, NULL, 'c18', NULL, NULL, NULL, NULL, 'svxaMD2AfCISj5KdhKCEiUf8d9G1gWCW', NULL, NULL, '$2y$13$DiP1ojlssevGPsSVQ5qg5eVemlGnOvE1/LqOzGgVvB8r4BtGePOzK', NULL, 10, NULL, NULL, NULL, NULL, NULL, '9788440809', NULL, NULL, 'AXIS', '925666335954', 3, 'pwFhm', NULL, 'AXIS', 'AXIS', 'AXIS', 'AXIS', 'c18@gmail.com', 'C', '2020-09-24 11:23:11', '2020-09-24 11:23:11'),
(517, NULL, 'c19', NULL, NULL, NULL, NULL, 'AUgKY-4-WVvcXSzF5OmSlqPcsn8IV67D', '/backend/web/uploads/WcAL4dwdriverArun_Madhan.jpg', '/backend/web/uploads/SAFpNaodriverArun_Madhan.jpg', '$2y$13$TepzMfbt728W07g/kCM4OOHSXzatpS0HiOgazNaIdfKyupmLHWT4W', NULL, 10, NULL, NULL, NULL, NULL, NULL, '9788440809', NULL, NULL, 'AXIS', '925666335954', 3, '3TNIQ', NULL, 'AXIS', 'AXIS', 'AXIS', 'AXIS', 'c19@gmail.com', 'P', '2020-09-24 11:23:48', '2020-10-13 13:20:50'),
(518, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$13$let8oSe1pFAxew4gnesnH.R/q7egv6xdGuYmGSzNXKXJG3WFJi4uy', NULL, 10, NULL, NULL, NULL, 'A', NULL, NULL, NULL, 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'admin@gmail.com', NULL, NULL, NULL),
(519, 'MLM', 'AAA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$13$pkr5NlvD4hE6xaj4HZUzDOvIaWwIr3PXr5a2LGP2YbqLKxBh1ZsbO', NULL, 10, NULL, NULL, NULL, 'A', NULL, NULL, NULL, 'S', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mlm1@gmail.com', NULL, NULL, NULL),
(520, 'Alban', 'AAA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$13$LPocwNpMNWbJW5wchbrPpeZ5iNEdbpPBDyzrwPT7y/UPzQ6eb969a', NULL, 10, NULL, NULL, NULL, 'A', NULL, NULL, NULL, 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mlm2@gmail.com', NULL, NULL, NULL),
(521, NULL, 'AAA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$13$kphVExyEmiyUF5LOYA5S6etnaJ6q.7lP2stgCWFb1Sc8LPQ0Oros6', NULL, 10, NULL, NULL, NULL, 'A', NULL, NULL, NULL, 'S', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'aaa@gmal.com', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `binary_user`
--
ALTER TABLE `binary_user`
  ADD PRIMARY KEY (`auto_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `subscription_id` (`subscription_id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `parent_subscription_id` (`parent_subscription_id`);

--
-- Indexes for table `commision_report`
--
ALTER TABLE `commision_report`
  ADD PRIMARY KEY (`auto_id`),
  ADD KEY `subscription_tbl_id` (`subscription_tbl_id`),
  ADD KEY `level_id` (`level_id`);

--
-- Indexes for table `commission_data_structure`
--
ALTER TABLE `commission_data_structure`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commission_structure`
--
ALTER TABLE `commission_structure`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `configuration_table`
--
ALTER TABLE `configuration_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `level_completion_status`
--
ALTER TABLE `level_completion_status`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subscription_id` (`subscription_id`),
  ADD KEY `no_of_levels` (`no_of_levels`);

--
-- Indexes for table `pair_table`
--
ALTER TABLE `pair_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `level` (`level`);

--
-- Indexes for table `payment_log`
--
ALTER TABLE `payment_log`
  ADD PRIMARY KEY (`payment_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `subscripe_id` (`subscripe_id`);

--
-- Indexes for table `random_generation`
--
ALTER TABLE `random_generation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscription_master`
--
ALTER TABLE `subscription_master`
  ADD PRIMARY KEY (`subscription_id`);

--
-- Indexes for table `subscription_table`
--
ALTER TABLE `subscription_table`
  ADD PRIMARY KEY (`subscripe_id`),
  ADD UNIQUE KEY `random_subscribe_no` (`random_subscribe_no`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `subscription_id` (`subscription_master_id`),
  ADD KEY `refered_by_user` (`refered_by_user`),
  ADD KEY `commision_data_structure_id` (`commision_data_structure_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_id` (`email_id`),
  ADD KEY `subscripe_plan` (`subscripe_plan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `binary_user`
--
ALTER TABLE `binary_user`
  MODIFY `auto_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=303;

--
-- AUTO_INCREMENT for table `commision_report`
--
ALTER TABLE `commision_report`
  MODIFY `auto_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `commission_data_structure`
--
ALTER TABLE `commission_data_structure`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `commission_structure`
--
ALTER TABLE `commission_structure`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `configuration_table`
--
ALTER TABLE `configuration_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `level_completion_status`
--
ALTER TABLE `level_completion_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=832;

--
-- AUTO_INCREMENT for table `pair_table`
--
ALTER TABLE `pair_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_log`
--
ALTER TABLE `payment_log`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT for table `random_generation`
--
ALTER TABLE `random_generation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `subscription_master`
--
ALTER TABLE `subscription_master`
  MODIFY `subscription_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `subscription_table`
--
ALTER TABLE `subscription_table`
  MODIFY `subscripe_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=522;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `binary_user`
--
ALTER TABLE `binary_user`
  ADD CONSTRAINT `binary_user_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `binary_user_ibfk_2` FOREIGN KEY (`subscription_id`) REFERENCES `subscription_table` (`subscripe_id`),
  ADD CONSTRAINT `binary_user_ibfk_3` FOREIGN KEY (`parent_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `binary_user_ibfk_4` FOREIGN KEY (`parent_subscription_id`) REFERENCES `subscription_table` (`subscripe_id`);

--
-- Constraints for table `commision_report`
--
ALTER TABLE `commision_report`
  ADD CONSTRAINT `commision_report_ibfk_1` FOREIGN KEY (`subscription_tbl_id`) REFERENCES `subscription_table` (`subscripe_id`),
  ADD CONSTRAINT `commision_report_ibfk_2` FOREIGN KEY (`level_id`) REFERENCES `commission_structure` (`id`);

--
-- Constraints for table `level_completion_status`
--
ALTER TABLE `level_completion_status`
  ADD CONSTRAINT `level_completion_status_ibfk_1` FOREIGN KEY (`subscription_id`) REFERENCES `subscription_table` (`subscripe_id`),
  ADD CONSTRAINT `level_completion_status_ibfk_2` FOREIGN KEY (`no_of_levels`) REFERENCES `commission_structure` (`id`);

--
-- Constraints for table `pair_table`
--
ALTER TABLE `pair_table`
  ADD CONSTRAINT `pair_table_ibfk_1` FOREIGN KEY (`level`) REFERENCES `commission_structure` (`id`);

--
-- Constraints for table `payment_log`
--
ALTER TABLE `payment_log`
  ADD CONSTRAINT `payment_log_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `payment_log_ibfk_3` FOREIGN KEY (`subscripe_id`) REFERENCES `subscription_table` (`subscripe_id`);

--
-- Constraints for table `subscription_table`
--
ALTER TABLE `subscription_table`
  ADD CONSTRAINT `subscription_table_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `subscription_table_ibfk_4` FOREIGN KEY (`subscription_master_id`) REFERENCES `subscription_master` (`subscription_id`),
  ADD CONSTRAINT `subscription_table_ibfk_5` FOREIGN KEY (`refered_by_user`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `subscription_table_ibfk_6` FOREIGN KEY (`commision_data_structure_id`) REFERENCES `commission_data_structure` (`id`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`subscripe_plan`) REFERENCES `subscription_master` (`subscription_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

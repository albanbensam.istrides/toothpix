<?php
return [
    'inputname' => 'hidden_token',
    /* Subscription Status */
    'sub-hist' => array('0'=>'Eliminated','1'=>'Active','2'=>'Closed','3'=>'Rejoin Request','4'=>'Rejoin Rejected','5'=>'Payment Failure'),
    /*API Authentication Key*/
    'authentication' => '638a02b2-0ebc-4996-9e05-2b52d5f1b157',
    /*API Service Response Message*/
    'response' => array(
            'E' =>'error',
            'S'=>'success',
            'F'=> 'failure',
            'X'=>'Exceeded',
            'P'=>'Pending',
            'SU' => 'Success Data',
            'PAYMENT_PENDING' => 'Register User Payment Pending',
            'PAYMENT_FAILURE' => 'Register User Payment Failure',
            'PAYMENT_SUCCESS' => 'Payment Success',
            'message_1'=>'No Data to Be Found in Subscription Master',
            'message_2'=>'No Data to Be Found in Refferal Code',
            'message_3'=>'Invalid Field Mapping',
            'message_4'=>'Successfully Saved',
            'message_5'=>'Invalid Username',
            'message_6'=>'Invalid Password',
            'message_7'=>'Login Success',
            'message_8'=>'Invalid Referral Key!!!',
            'message_9' => 'Bad Request Authorization',
            'message_10' => 'Invalid Authentication',
            'message_11' => 'Invalid Authorization',
            'message_12' => 'Bad Request Authentication',
            'message_13' => 'No Data Found in Subscription History',
            'message_14' => 'Data Not Saved Correctly',
            'message_15' => 'Refferal Id Not Found',
            'message_16' => 'Proper Response Not Come',
            'message_17' => 'Payment Log Insert Success',
            'message_18' => 'Payment Log Insert Error',
            'message_19' => 'This User Payment Already Done',
            'message_20' => 'This User Payment Failure',
            'message_21' => 'Level Key Not Found in Request',
            'message_22' => 'Subscription ID Not Found in Request',
            'message_23' => 'Subscription ID Data Not Found or Admin Not Approved Please Contact Administrator',
            'message_24' => 'Parent Subscription Data Not Found',
            'message_25' => 'No Level Data Found in this Subscription',
            'message_26' => 'Already Payment Done or Payment Failure in this Subscription',
            'message_27' => 'Data Updated Successfully',
            'message_28' => 'Data Not Updated',
            'message_29' => 'Request Data Not Found',
            'message_30' => 'Invalid Email-ID',
            'message_31' => 'Parent Subscriber Payment Pending',
            'message_32' => 'Invalid Parent Subscriber',               
                    ),

    /*Set Refferal Column Array*/
    'setrefferalvalid_data' => array('first_name','id','payment_status','subscription_master_id'),

    /*User Register Array*/
    'userregister_data' => array('auth_key','payment_status','random_subscribe_no','subscripe_id'),

    /*Login Array*/
    'login_data' => array('auth_key','first_name','password_hash','mobile_number','address','aadhar_number','subscription_id','package_name','refferal_id','bank_name','bank_account_no','ifsc','branch','email_id','profile_image','profile_pdf'),

    /*Subscription History*/
    'subscriptionhistory_data' => array('subscripe_id','user_id','subscription_master_id','commision_data_structure_id','registeration_date','activation_date','elimination_date','closing_date','rejoin_date','rejected_date','max_user_count','total_approved_user','pending_users_count','referal_code','refered_by_code','refered_by_user','current_status','reason','random_subscribe_no','payment_status'),

    /*Payment Log*/
    'paymentvalidated_data' => array('user_id','subscripe_id','order_id','transaction_id','paid_amount','payment_status','status','created_at','updated_at'),

    /*Payment Log*/
    'profileupdate_data' => array('first_name','mobile_number','address','aadhar_number','bank_account_no','ifsc','branch','bank_name','profile_pdf','profile_image'),

    /*Payment Status*/
    'payment_status' => array(0=>'PAYMENT_PENDING',1=>'PAYMENT_FAILURE',2=>'PAYMENT_SUCCESS'),

    'level_pairs_data' => array(
        'total_no_of_pairs_each_level',
        'no_of_pairs_completed',
        'pending_no_of_pairs_completed',
        'total_no_of_members',
        'total_no_of_members_joined',
        'no_of_levels',
        'accumulate_price',
        'level_id'),

    'commision_details' => array(
         'level_id' => 'level',
         'pair' => 'position',
         'commision_value' => 'commision_amount'
    ),

    'user_type_data' => array('P'=>'P','C'=>'C'),

    'level-fixed' => 'L6',

    'level-config' => array(1=>'L1',2=>'L2',3=>'L3',4=>'L4',5=>'L5',6=>'L6'),

    'payment-rejoin' =>array('referal_code','subscripe_plan'),

    'scenario' => 'SCENARIO_API_CREATE',

    'date' => date('Y-m-d H:i:s'),

    'image-type' => array('Top_Left','Top_Right','Bottom_Left','Bottom_Right'), 

    'image_type_api' => array('Top Left'=>"topLeft",'Top Right'=>"topRight",'Bottom Left'=>"bottomLeft",'Bottom Right'=>"bottomRight"),    
];

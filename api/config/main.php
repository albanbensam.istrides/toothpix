<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),    
    'bootstrap' => ['log'],
    'modules' => [
        'v1' => [
            'basePath' => '@app/modules/v1',
            'class' => 'api\modules\v1\Module'
        ],
         /*'v2' => [
            'basePath' => '@app/modules/v2',
            'class' => 'api\modules\v2\Module'
        ],*/
    ],
    'components' => [        
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => false,
        ],
        'response' => [
            // 'format' => yii\web\Response::FORMAT_JSON, # This line used for Send Every Response as JSON Data Only
            'charset' => 'UTF-8',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'request' => [
                    'enableCookieValidation' => true,
                    'enableCsrfValidation' => false,
                    'cookieValidationKey' => 'sdfsdf'
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                
                /*classlist and subject list API*/
                'POST v1/login' => 'v1/api/login',
                'POST v1/pictureupload' => 'v1/api/pictureupload',
                'POST v1/registeration' => 'v1/api/register',
                'POST v1/recommentall' => 'v1/api/recommentationall',
                
            ],  
        ]
    ],
    'params' => $params,
];




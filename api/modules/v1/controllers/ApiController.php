<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\filters\VerbFilter;
use common\models\CommonSave;
use backend\models\User;
use backend\components\QueryBehaviour;
use backend\models\ToothPicture;
use backend\models\Enquiry;

/**
 * API Controller API
 *
 * @author Alban
 */
class ApiController extends Controller
{
	public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],

            'QueryBehaviour' => QueryBehaviour::className(),
        ];
    }

	public function beforeAction($action) {    
		$params = (Yii::$app->request->headers);
       
        if(isset($params['authentication']))
        {
            if(Yii::$app->params['authentication'] == $params['authentication']){        
                $this->enableCsrfValidation = false;
                return parent::beforeAction($action);
            }else{
                $list['status'] = Yii::$app->params['response']['E'];
                $list['message'] = Yii::$app->params['response']['message_10'];
                echo json_encode($list);die;
            }
        }
        else
        {

            $list['status'] = Yii::$app->params['response']['E'];
            $list['message'] = Yii::$app->params['response']['message_9'];
            echo json_encode($list);die; 
        }
	}

	public function dateformat($date){
		$newdate = strtotime($date); 
		$newdate=date('Y-m-d H:i:s', $newdate); 
        return $newdate;
	}

    # Author : Alban
    # Timestamp : 11-02-2021 07:36 PM
    # Desc : Log in Page Service
    public function actionLogin() 
    {
        $list['status'] = Yii::$app->params['response']['E'];
        $list['message'] = Yii::$app->params['response']['message_3'];
        $postd=(Yii::$app ->request ->rawBody);
        $requestInput = json_decode($postd,true); 
        $user = $this->getUser($requestInput['e-mailid']);
        /* Get User Data */
        if($user)
        {
            if (Yii::$app->getSecurity()->validatePassword($requestInput['password'], $user->password_hash)) 
            {
                $list['status'] = Yii::$app->params['response']['S'];
                $list['message'] = 'Success';
                $list['auth-key'] = $user->auth_key;
                $list['client_name'] = $user->first_name." ".$user->last_name;
                $list['gender'] = $user->gender;
                $list['age'] = $user->age;
                $list['email_id'] = $user->email_id;
                $list['mobile_number'] = $user->mobile_number;
            } else {
                $list['message'] = 'Invalid Password';               
            }
        }
        else
        {
            $list['message'] = 'Invalid Username';
        } 
        return json_encode($list);
    }

    # Author : Alban
    # Timestamp : 11-02-2021 07:36 PM
    # Desc : Get User
    protected function getUser($e_mailid)
    {
        $user = User::find()->where(['email_id'=>$e_mailid])->one();
        return $user;
    }

    # Author : Alban
    # Timestamp : 11-02-2021 07:36 PM
    # Desc : Validate Password
    public function validatePassword($password,$e_mailid)
    {
        $user = $this->getUser($e_mailid);
        if(!$this->validatePassword($password))
        {
            return 'Invalid Password.';
        }
    }

	# Author : Alban
    # Timestamp : 06-08-2020 22:23 PM
    # Desc : Write an Error Messange
    public function ErrorMessage($error) 
    {
    	$error_msg = "";
    	foreach ($error as $key => $value) {
    		$error_msg = $value;
    		break;
    	}
    	return $error_msg;
    }

    # Author : Alban
    # Timestamp : 11-02-2021 09:09 PM
    # Desc : Picture Upload
    public function actionPictureupload()
    {
        $list['status'] = Yii::$app->params['response']['E'];
        $params = (Yii::$app->request->headers);
        $authorization=$params['auth-key'];
        $user_data = $this->getAuthorization($authorization);
        $image_type = Yii::$app->params['image-type'];
        if(!empty($user_data))
        {
            $postd=$_FILES;
            $connection = \Yii::$app->db;        
            $transaction = $connection->beginTransaction();
            $session = Yii::$app->session;
            try 
            {
                $data_image = 'success';
                $enquiry = new Enquiry();
                $enquiry->image_date = Yii::$app->params['date'];
                $enquiry->created_at = Yii::$app->params['date'];
                $enquiry->updated_at = Yii::$app->params['date'];
                $enquiry->user_id = $user_data['id'];
                if($enquiry->save())
                {
                    foreach ($_FILES as $key => $value) 
                    {
                        if (in_array($key, $image_type))
                        {
                            $temp = str_replace('api', 'backend', Yii::$app->basePath);
                            if ($value["error"] == 0){
                                $file_name = preg_replace('/\s+/', '_', $value['name']);
                                $aa6 = "/web/uploads/".$this->getRandomString(7).$file_name;
                                $aa6_new = $temp.$aa6;
                                move_uploaded_file($value['tmp_name'], $aa6_new);
                                $toothpicture = new ToothPicture();
                                $toothpicture->user_id =  $user_data['id'];
                                $toothpicture->image_url =  'backend'.$aa6;
                                $toothpicture->image_type =  str_replace('_', ' ', $key);
                                $toothpicture->status =  'A';
                                $toothpicture->created_at =  Yii::$app->params['date'];
                                $toothpicture->updated_at = Yii::$app->params['date'];
                                $toothpicture->enq_id = $enquiry->enq_id;
                                $toothpicture->save();
                                $list['data'][] = 'backend'.$aa6;    
                            }
                        }
                        else
                        {
                            $data_image = 'error';
                            break;
                        }
                    }
                }
                if($data_image == 'error')
                {
                    $list['message'] = 'Invalid Image Position';    
                    $transaction->rollback();       
                }
                else
                {
                    $list['status'] = Yii::$app->params['response']['S'];
                    $list['message'] = 'Image Upload Success';    
                    $transaction->commit();
                }
            }
            catch(Exception $e) 
            {
                $transaction->rollback();
                $list['message'] = Yii::$app->params['response']['message_28'];   
            }
            
        }
        else if(isset($user_data['message']))
        {
            $list['message'] = $user_data['message'];
        }
        return json_encode($list);  
    }

    # Author : Alban
    # Timestamp : 03-03-2021 12:54 PM
    # Desc : Register Api
    public function actionRegister()
    {
        $list['status'] = Yii::$app->params['response']['E'];
        $params = (Yii::$app->request->headers);
        $authorization=$params['auth-key'];
        //$user_data = $this->getAuthorization($authorization);
        $postd=(Yii::$app ->request ->rawBody);
        $requestInput = json_decode($postd,true); 
            $connection = \Yii::$app->db;        
            $transaction = $connection->beginTransaction();
            $session = Yii::$app->session;
            try 
            {
                $data_image = 'success';
                $model = new User(['scenario' => User::SCENARIO_CREATE]);
                foreach ($requestInput as $key => $value) {
                    if($key == 'password_hash')
                    {
                        $model->password_hash = Yii::$app->security->generatePasswordHash($value);
                        continue;
                    }
                    $model->$key = $value;  
                }
                $model->auth_key= Yii::$app->security->generateRandomString();
                if($model->save())
                {
                    $list['status'] = Yii::$app->params['response']['S'];
                    $list['message'] = 'Registeration Success';
                    $list['auth-key'] = $model->auth_key;
                    
                    $transaction->commit();    
                }
                else{
                    $list['message'] = current(current($model->getErrors()));  
                }
            }
            catch(Exception $e) 
            {
                $transaction->rollback();
                $list['message'] = Yii::$app->params['response']['message_28'];   
            }
        return json_encode($list);  
    }

    # Author : Alban
    # Timestamp : 11-02-2021 10:56 PM
    # Desc : Recommentation ALL
    public function actionRecommentationall()
    {
        $list['status'] = Yii::$app->params['response']['E'];
        $params = (Yii::$app->request->headers);
        $authorization=$params['auth-key'];
        $user_data = $this->getAuthorization($authorization);
        if(!empty($user_data))
        {
            $enquiry = Enquiry::find()->where(['user_id'=>$user_data['id']])->orderBy(['enq_id'=>SORT_DESC])->asArray()->all();
            $data = array();
            $image_type = Yii::$app->params['image-type'];
            foreach ($enquiry as $key => $value) 
            {
                $recoment_date = (!empty($value['enq_date'])) ? date('d.m.Y',strtotime($value['enq_date'])) : "";
                $recoment_status = (!empty($value['is_recommentation'])) ? $value['is_recommentation']  : "No";
                $image_upload = ToothPicture::find()->where(['enq_id'=>$value['enq_id']])->asArray()->all();
                $data_array = array();
                if(!empty($image_upload)){

                    foreach ($image_upload as $key_url => $value_url) {
                        if(array_key_exists($value_url['image_type'], Yii::$app->params['image_type_api']))
                        {
                            $value_url['image_type'] = Yii::$app->params['image_type_api'][$value_url['image_type']];
                        }
                        $data_array[] = array($value_url['image_type']=>$value_url['image_url']);
                    }
                }
                $data[] = array('id'=>$value['enq_id'],'upload_date'=>date("F d, Y",strtotime($value['image_date'])),'recommentation_date'=>$recoment_date,'recomment_status'=>$recoment_status,'image_url'=>$data_array);
            }    
            $list['status'] = $data;
        }
        else
        {
            $list['message'] = $user_data['message'];
        }
        return json_encode($list);  
    }   
}



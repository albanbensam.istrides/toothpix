<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\filters\VerbFilter;
use common\models\CommonSave;
use backend\models\User;
use backend\models\PaymentLog;
use backend\components\QueryBehaviour;
use api\modules\v1\controllers\ApiController;
use backend\models\BinaryUser;
use backend\models\SubscriptionTable;
/**
 * Alban Controller API
 *
 * @author Alban
 */
class ApiauthController extends ApiController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],

            'QueryBehaviour' => QueryBehaviour::className(),

        ];
    }

    # Author : Alban
    # Timestamp : 11-08-2020 00:08 AM
    # Desc : Authorization Check Based On Auth Key For Every API Service
    public function beforeAction($action) {    
        $params = (Yii::$app->request->headers);
        if(isset($params['authentication']))
        {
            if(Yii::$app->params['authentication'] == $params['authentication']){        
                if($authorization=$params['authorization']){        
                    $this->enableCsrfValidation = false;
                    return parent::beforeAction($action);
                }else{
                    $list['status'] = Yii::$app->params['response']['E'];
                    $list['message'] = Yii::$app->params['response']['message_9'];
                    echo json_encode($list);die;
                }
            }else{
                $list['status'] = Yii::$app->params['response']['E'];
                $list['message'] = Yii::$app->params['response']['message_10'];
                echo json_encode($list);die;
            }
        }
        else
        {
            $list['status'] = Yii::$app->params['response']['E'];
            $list['message'] = Yii::$app->params['response']['message_12'];
            echo json_encode($list);die; 
        }
    }

    # Author : Alban
    # Timestamp : 11-08-2020 00:13 AM
    # Desc : Auth Key Based Get An Subscription History
    public function actionGetSubscriptionHistory()
    {

        $list['status'] = Yii::$app->params['response']['E'];
        
        $user_data = $this->authorizationonly();
        if(isset($user_data['data']))
        {
            $user_data = $user_data['data'];

            $user_primary_key = $user_data['id'];
            $get_subscription_history = $this->getSubscriptionHistory($user_primary_key);
            $get_referal_id = ArrayHelper::map($get_subscription_history,'refered_by_user','refered_by_user');
            $getUserData =  $this->getUserData($get_referal_id);
            
            $get_subscription_master = $this->getSubscriptiondata();
            $list['message'] = Yii::$app->params['response']['message_13'];
            $sub_history = Yii::$app->params['sub-hist'];
            if($get_subscription_history)
            {
                $list['status'] = Yii::$app->params['response']['S'];
                $list['message'] = Yii::$app->params['response']['SU'];
                foreach ($get_subscription_history as $key => $value) 
                {
                    if(array_key_exists($value['current_status'], Yii::$app->params['sub-hist']))
                    {
                        $value['current_status']=$sub_history[$value['current_status']];
                        $value['subscription_master_name']=$get_subscription_master[$value['subscription_master_id']];
                        
                        if(array_key_exists($value['refered_by_user'], $getUserData))
                        {
                            $value['refered_by_user'] = $getUserData[$value['refered_by_user']]['first_name'];    
                        }
                        else
                        {
                            $value['refered_by_user'] = "";
                        }   
                        
                        $list['data'][] = $value;    
                    }
                }
            }
        }
        else if(isset($user_data['message']))
        {
            $list['message'] = $user_data['message'];
        }   
        return json_encode($list);
    }

    # Author : Alban
    # Timestamp : 22-08-2020 02:12 AM
    # Desc : Payment Validated
    public function actionPaymentvalidated()
    {
        $list['status'] = Yii::$app->params['response']['E'];

        $user_data = $this->authorizationonly();
        if(isset($user_data['data']))
        {

            $postd=(Yii::$app ->request ->rawBody);
            $requestInput = json_decode($postd,true); 
            $data_request = array();
            if($requestInput['payment_status'] == Yii::$app->params['response']['S'])
            {
                if(isset($requestInput['subscripe_id']))
                {
                    $get_subscription_data = $this->getSubscriptiondatapayementonly($requestInput['subscripe_id']);
                    
                    if(!empty($get_subscription_data))
                    {

                    $subscription_table = SubscriptionTable::find()->where(['referal_code'=>$get_subscription_data['refered_by_code']])->andWhere(['payment_status'=>'PAYMENT_SUCCESS'])->asArray()->one();        
                    if(empty($get_subscription_data['refered_by_code']) || !empty($subscription_table))
                    {

                        $user_validated = User::find()->where(['id'=>$get_subscription_data['user_id']])->asArray()->one();

                        $data_binary_count = BinaryUser::find()->count();
                        if($data_binary_count == 0 && !empty($user_validated) && $get_subscription_data['user_type_data'] == 'C')
                        {
                            
                            $list['message'] = Yii::$app->params['response']['message_24'];
                        }
                        else
                        {
                                   
                            $check_payment_status = $this->checkpaymentstatusonly($requestInput['subscripe_id'],Yii::$app->params['payment_status'][0]);

                        if(!empty($check_payment_status))
                        {
                            $requestInput['user_id']  = $check_payment_status['user_id'];
                            $requestInput['status']  = '1';
                            $requestInput['created_at']  = Yii::$app->params['date'];
                            $requestInput['updated_at']  = Yii::$app->params['date'];

                            $payment_validated_field = $this->getTableColumn('payment_log');

                            foreach (Yii::$app->params['paymentvalidated_data'] as $key => $value) 
                            {
                                if(array_key_exists($value, $payment_validated_field))
                                {
                                    $data_request['PaymentLog'][$value]= $requestInput[$value]; 
                                }
                            }

                            $payment_log = new PaymentLog();
                            
                            if($payment_log->load($data_request) && $payment_log->validate())
                            {
                                $CommonSave = new CommonSave();
                                $data_request[Yii::$app->params['inputname']] = $this->setSessionToken();
                                $data_request['subscripe_id'] = $requestInput['subscripe_id'];
                                
                                $data_response = $CommonSave->PaymentInsert($data_request);
                                if($data_response != 'error')
                                {
                                    $list['status'] = Yii::$app->params['response']['S'];
                                    $list['message'] = Yii::$app->params['response']['message_17'];
                                }
                                else
                                {
                                    $list['message'] = Yii::$app->params['response']['message_18'];
                                }    
                            }
                            else
                            {
                                $list['message'] = current($payment_log->getFirstErrors());
                            }
                        }
                        else
                        {
                            $list['message'] = Yii::$app->params['response']['message_19'];
                        }
                        }
                    }
                    else
                    {
                        $list['message'] = Yii::$app->params['response']['message_31'];
                    }    
                    }
                    else
                    {
                        $list['message'] = Yii::$app->params['response']['message_23'];
                    }
                }
                else
                {
                     $list['message'] = Yii::$app->params['response']['message_22'];
                }       
            }
            else if($requestInput['payment_status'] == Yii::$app->params['response']['E'])
            {
                if(isset($requestInput['subscripe_id']))
                {
                    $get_subscription_data = $this->getSubscriptiondatapayementcancel($requestInput['subscripe_id']); 
                    
                    if(!empty($get_subscription_data))
                    {
                        
                        $CommonSave = new CommonSave();
                        $data = $CommonSave->PaymentFailure($get_subscription_data);

                        $list['status'] = ($data == Yii::$app->params['response']['S']) ? Yii::$app->params['response']['S'] : Yii::$app->params['response']['E'];
                        $list['message'] = ($data == Yii::$app->params['response']['S']) ? Yii::$app->params['response']['message_27'] : Yii::$app->params['response']['message_28'];
                    }
                    else
                    {
                        $list['message'] = Yii::$app->params['response']['message_26'];
                    }
                }
                else
                {
                    $list['message'] = Yii::$app->params['response']['message_22'];
                }
            }
            else
            {
                $list['message'] = Yii::$app->params['response']['message_16'];
            }
        }
        else if(isset($user_data['message']))
        {
            $list['message'] = $user_data['message'];
        }

        return json_encode($list);  
    }

    # Author : Alban
    # Timestamp : 28-08-2020 00:14 AM
    # Desc : Level Completion Data
    public function actionLevelPairs()
    {
        $list['status'] = Yii::$app->params['response']['E'];

        $user_data = $this->authorizationonly();
        if(isset($user_data['data']))
        {
            $postd=(Yii::$app ->request ->rawBody);
            $requestInput = json_decode($postd,true); 
            $data_request = array();
            if(isset($requestInput['subscripe_id']))
            {
                $get_subscription_data = $this->getSubscriptiondatapayement($requestInput['subscripe_id']);   
                if(!empty($get_subscription_data))
                {
                    $check_payment_status = $this->checkpaymentstatus($requestInput['subscripe_id'],Yii::$app->params['payment_status'][2]);
                    if(!empty($check_payment_status))
                    {
                        $list['status'] = Yii::$app->params['response']['S'];
                        $list['data'] = $this->level_fetch_data($check_payment_status['subscripe_id']);
                    }
                    else
                    {
                        $list['message'] = Yii::$app->params['response']['message_19'];
                    }
                }
                else
                {
                    $list['message'] = Yii::$app->params['response']['message_23'];
                }
            }
            else
            {
                $list['message'] = Yii::$app->params['response']['message_22'];
            }    
        }
        else if(isset($user_data['message']))
        {
            $list['message'] = $user_data['message'];
        }
        return json_encode($list);  
    }

    # Author : Alban
    # Timestamp : 28-08-2020 00:14 AM
    # Desc : Level Completion Data
    public function actionCommisionSummary()
    {
        $list['status'] = Yii::$app->params['response']['E'];

        $user_data = $this->authorizationonly();
        if(isset($user_data['data']))
        {
            $postd=(Yii::$app ->request ->rawBody);
            $requestInput = json_decode($postd,true); 
            $data_request = array();
            
            if(isset($requestInput['subscripe_id']))
            {
                $get_subscription_data = $this->getSubscriptiondatapayement($requestInput['subscripe_id']);   
                if(!empty($get_subscription_data))
                {
                    $check_payment_status = $this->checkpaymentstatus($requestInput['subscripe_id'],Yii::$app->params['payment_status'][2]);
                    if(!empty($check_payment_status))
                    {
                        $list['status'] = Yii::$app->params['response']['S'];
                        $list['data'] = $this->commision_summary_fetch_data($check_payment_status['subscripe_id']);
                    }
                    else
                    {
                        $list['message'] = Yii::$app->params['response']['message_19'];
                    }
                }
                else
                {
                    $list['message'] = Yii::$app->params['response']['message_8'];
                }
            }
            else
            {
                $list['message'] = Yii::$app->params['response']['message_22'];
            }
        }
        else if(isset($user_data['message']))
        {
            $list['message'] = $user_data['message'];
        }
        return json_encode($list);  
    }

    # Author : Alban
    # Timestamp : 29-08-2020 15:59 PM
    # Desc : Commision Details
    public function actionCommisionDetails()
    {
        $list['status'] = Yii::$app->params['response']['E'];

        $user_data = $this->authorizationonly();

        if(isset($user_data['data']))
        {
            $postd=(Yii::$app ->request ->rawBody);
            $requestInput = json_decode($postd,true); 
            $data_request = array();
            if(isset($requestInput['level_id']))
            {
                if(isset($requestInput['subscripe_id']))
                {
                    $get_subscription_data = $this->getSubscriptiondatapayement($requestInput['subscripe_id']);   
                    if(!empty($get_subscription_data))
                    {
                        $check_payment_status = $this->checkpaymentstatus($requestInput['subscripe_id'],Yii::$app->params['payment_status'][2]);
                        if(!empty($check_payment_status))
                        {
                            
                            if(!empty($this->commision_details_fetch_data($check_payment_status['subscripe_id'],$requestInput['level_id'])))
                            {
                            	$list['status'] = Yii::$app->params['response']['S'];
                            	$list['data'] = $this->commision_details_fetch_data($check_payment_status['subscripe_id'],$requestInput['level_id']);	
                            }
                            else
                            {
                            	$list['message'] = Yii::$app->params['response']['message_25'];
                            }
                        }
                        else
                        {
                            $list['message'] = Yii::$app->params['response']['message_19'];
                        }
                    }
                    else
                    {
                        $list['message'] = Yii::$app->params['response']['message_8'];
                    }
                }
                else
                {
                    $list['message'] = Yii::$app->params['response']['message_22'];
                }
            }
            else
            {
                $list['message'] = Yii::$app->params['response']['message_21'];
            }    
            
        }
        else if(isset($user_data['message']))
        {
            $list['message'] = $user_data['message'];
        }
        return json_encode($list);  
    }

    # Author : Alban
    # Timestamp : 30-08-2020 21:40 AM
    # Desc : Binary Flow Details
    public function actionBinaryTree()
    {
        $list['status'] = Yii::$app->params['response']['E'];

        $user_data = $this->authorizationonly();

        if(isset($user_data['data']))
        {
            $postd=(Yii::$app ->request ->rawBody);
            $requestInput = json_decode($postd,true); 
            $data_request = array();
            
                if(isset($requestInput['subscripe_id']))
                {
                    $get_subscription_data = $this->getSubscriptiondatapayement($requestInput['subscripe_id']);   
                    if(!empty($get_subscription_data))
                    {
                        $check_payment_status = $this->checkpaymentstatus($requestInput['subscripe_id'],Yii::$app->params['payment_status'][2]);
                        if(!empty($check_payment_status))
                        {
                            $list['status'] = Yii::$app->params['response']['S'];
                            $list['data'] = $this->BinaryChartTree($requestInput['subscripe_id']);
                        }
                        else
                        {
                            $list['message'] = Yii::$app->params['response']['message_19'];
                        }
                    }
                    else
                    {
                        $list['message'] = Yii::$app->params['response']['message_8'];
                    }
                }
                else
                {
                    $list['message'] = Yii::$app->params['response']['message_22'];
                }    
        }
        else if(isset($user_data['message']))
        {
            $list['message'] = $user_data['message'];
        }
        return json_encode($list);  
    }

    # Author : Alban
    # Timestamp : 09-08-2020 21:40 AM
    # Desc : Binary Flow Details
    public function actionProfileupdate()
    {
        $list['status'] = Yii::$app->params['response']['E'];

        $user_data = $this->authorizationonly();

        if(isset($user_data['data']))
        {
            $postd=(Yii::$app ->request ->rawBody);
            $requestInput = json_decode($postd,true); 
            $data_request = array();
            $requestInputUser['User'] = $requestInput;
            $user = User::findOne($user_data['data']['id']);
            $user->scenario = User::SCENARIO_API_UPDATE;
            if($user->load($requestInputUser) && $user->validate())
            {
                $connection = \Yii::$app->db;        
                $transaction = $connection->beginTransaction();
                $session = Yii::$app->session;
                try 
                {
                    $user->first_name = $requestInputUser['User']['first_name'];
                    //$user->email_id = $requestInputUser['User']['email_id'];
                    $user->mobile_number = $requestInputUser['User']['mobile_number'];
                    $user->address = $requestInputUser['User']['address'];
                    $user->aadhar_number = $requestInputUser['User']['aadhar_number'];
                    $user->bank_account_no = $requestInputUser['User']['bank_account_no'];
                    $user->ifsc = $requestInputUser['User']['ifsc'];
                    $user->branch = $requestInputUser['User']['branch'];
                    $user->bank_name = $requestInputUser['User']['bank_name'];

                    if($user->save())
                    {
                        $list['status'] = Yii::$app->params['response']['S'];
                        $list['message'] = Yii::$app->params['response']['message_27'];
                        
                        $transaction->commit();
                    }
                    else
                    {
                        $list['message'] = Yii::$app->params['response']['message_28'];
                        $transaction->rollback();
                    }
                }
                catch(Exception $e) 
                {
                    $transaction->rollback();
                    $list['message'] = Yii::$app->params['response']['message_28'];   
                }
            }
            else
            {
                 $list['message'] = current($user->getFirstErrors());
            }            
        }
        else if(isset($user_data['message']))
        {
            $list['message'] = $user_data['message'];
        }
        return json_encode($list);  
    }

    public function actionFileupload()
    {
        $list['status'] = Yii::$app->params['response']['E'];
        $user_data = $this->authorizationonly();
        if(isset($user_data['data']))
        {

            $postd=$_FILES;
            $user = User::findOne($user_data['data']['id']);
            $user->scenario = User::SCENARIO_API_UPDATE;
            $requestInput = $_POST; 
            $fileInput = $_FILES;

            $check_array = array();
            $set_request_array = array_keys($requestInput);
            $set_values = array_merge($set_request_array,array_keys($fileInput));
            foreach (Yii::$app->params['profileupdate_data'] as $key => $value) 
            {
                if(!in_array($value, $set_values))
                {
                    $check_array[] = $value;
                }
            }

            if(empty($check_array))
            {
                $requestInputUser['User'] = $requestInput;
                if($user->load($requestInputUser) && $user->validate())
                {

                    $connection = \Yii::$app->db;        
                    $transaction = $connection->beginTransaction();
                    $session = Yii::$app->session;
                    try 
                    {
                        $user->first_name = $requestInputUser['User']['first_name'];
                        //$user->email_id = $requestInputUser['User']['email_id'];
                        $user->mobile_number = $requestInputUser['User']['mobile_number'];
                        $user->address = $requestInputUser['User']['address'];
                        $user->aadhar_number = $requestInputUser['User']['aadhar_number'];
                        $user->bank_account_no = $requestInputUser['User']['bank_account_no'];
                        $user->ifsc = $requestInputUser['User']['ifsc'];
                        $user->branch = $requestInputUser['User']['branch'];
                        $user->bank_name = $requestInputUser['User']['bank_name'];


                        $aa6 = $aa61 = "";
                        if(array_key_exists('profile_image', $postd)){
                                $temp = str_replace('api', 'backend', Yii::$app->basePath);
                                if ($postd["profile_image"]["error"] == 0){
                                    $file_name = preg_replace('/\s+/', '_', $postd['profile_image']['name']);
                                    $aa6 = "/web/uploads/".$this->getRandomString(7).$file_name;
                                    $aa6_new = $temp.$aa6;
                                    move_uploaded_file($postd['profile_image']['tmp_name'], $aa6_new);
                                    $user->profile_image="/backend".$aa6; 
                                }                            
                            }
                            if(array_key_exists('profile_pdf', $postd)){
                                $temp1 = str_replace('api', 'backend', Yii::$app->basePath);
                                if ($postd["profile_pdf"]["error"] == 0){
                                    $file_name1 = preg_replace('/\s+/', '_', $postd['profile_pdf']['name']);
                                    $aa61 = "/web/uploads/".$this->getRandomString(7).$file_name1;
                                    $aa61_new = $temp1.$aa61;
                                    move_uploaded_file($postd['profile_pdf']['tmp_name'], $aa61_new);
                                    $user->profile_pdf="/backend".$aa61; 

                                }                            
                            }



                        if($user->save())
                        {
                            $list['status'] = Yii::$app->params['response']['S'];
                            $list['message'] = Yii::$app->params['response']['message_27'];
                            $list['data'] = array('profile_image'=>"/backend".$aa6,'profile_pdf'=>"/backend".$aa61);
                            $transaction->commit();
                        }
                        else
                        {
                            $list['message'] = Yii::$app->params['response']['message_28'];
                            $transaction->rollback();
                        }
                    }
                    catch(Exception $e) 
                    {
                        $transaction->rollback();
                        $list['message'] = Yii::$app->params['response']['message_28'];   
                    }
                }
                else
                {
                     $list['message'] = current($user->getFirstErrors());
                }
            }
            else
            {
                $list['message'] = implode(',', $check_array).' this name reponse not found';
            }
        }
        else if(isset($user_data['message']))
        {
            $list['message'] = $user_data['message'];
        }
        return json_encode($list);  
    }

    # Author : Alban
    # Timestamp : 09-08-2020 21:40 AM
    # Desc : User Rejoin Payment
    public function actionRejoinPayment()
    {
        $list['status'] = Yii::$app->params['response']['E'];

        $user_data = $this->authorizationonly();

        if(isset($user_data['data']))
        {
            $postd=(Yii::$app ->request ->rawBody);
            $requestInput = json_decode($postd,true); 
            $data_request = array();
            $user = User::findOne($user_data['data']['id']);

            $payment =  Yii::$app->params['payment-rejoin'];
            $error_list = 0;
            foreach ($payment as $key => $value) {
                if(!array_key_exists($value, $requestInput))
                {
                    $error_list++;
                    $list['message'] = Yii::$app->params['response']['message_29'];
                    break;
                }
            }

            if($error_list == 0)
            {   
                
                $data_payment_message = $user->validatedRecordPayement($requestInput['referal_code']);  
                $data_payment_message = ($data_payment_message == 'success') ? $user->validatedRecordPayementSubscription($user_data['data']['id']) : $data_payment_message;

                $data_payment_message = ($data_payment_message == 'success') ? $user->validatedRecordSubscriptionPlan($requestInput['subscripe_plan']) : $data_payment_message;

                if($data_payment_message == 'success')
                {
                    $data_check = $user->validatedSubscription($user_data['data']['id']);    

                    if($data_check['result'] == 'success')
                    {
                        $connection = \Yii::$app->db;        
                        $transaction = $connection->beginTransaction();
                        $session = Yii::$app->session;
                        try 
                        {
                            $data_push = array();
                            $CommonSave = new CommonSave();
                            $data_push['refferal_user_id'] = $user_data['data']['id'];
                            $data_push['subscripe_plan'] = $requestInput['subscripe_plan'];
                            $data_push['refferal_id_generated'] = $this->getRandomString();
                            $data_push['refferal_id'] = $requestInput['referal_code'];
                            $data_push['mode'] = $data_check['mode'];
                            $data_push['current_status'] = $data_check['current_status'];    

                            $get_sub_id = $CommonSave->SubscriptionTablePaymentInsert($data_push);
                            
                            if($get_sub_id['data'] == 'success')
                            {
                                $transaction->commit();
                                $list['status'] = Yii::$app->params['response']['S'];
                                $list['data'] = array('subscripe_id'=>$get_sub_id['id'],'subscription-status'=>Yii::$app->params['sub-hist'][$data_push['current_status']]);
                                $list['message'] = Yii::$app->params['response']['message_27'];    
                            }
                            else
                            {
                                $list['message'] = Yii::$app->params['response']['message_14'];   
                                $transaction->rollback();
                            }
                        }
                        catch(Exception $e) 
                        {
                            $transaction->rollback();
                            $list['message'] = Yii::$app->params['response']['message_28'];   
                        }    
                    }

                    if($data_check['result'] == 'error')
                    {
                        $list['message'] = $data_check['message']; 
                    }   
                    
                }
                else
                {
                     $list['message'] = $data_payment_message;
                }
            }
            else
            {
                $list['message'] = Yii::$app->params['response']['message_29'];
            }            
        }
        else if(isset($user_data['message']))
        {
            $list['message'] = $user_data['message'];
        }
        return json_encode($list);  
    }


}



<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::$app->params['title']['user-index'];
?>

<div class="user-form">
 
    <?php $form = ActiveForm::begin(['enableAjaxValidation' => true,'enableClientValidation' => true],['options' => ['enctype'=>'multipart/form-data','autocomplete'=>'off']]); ?>

    <div class="card-header">
        <h4 class="card-title"><?php echo $model->isNewRecord ? '<span class="fa fa-plus"></span> Add User' : '<span class="fa fa-edit"></span> Update User'; ?></h4>
            <!-- <span class="close"><i  class="ti-close"></i></span> -->
			<a href=" <?php echo Url::base(true) ?>/user-index.html"> <button type="button" class="btn btn-sm btn-info" ><i class="fa fa-arrow-left"></i> Back</button></a>
    
    </div>
    <div class="card-body">
      <div class="row">
        <div class='col-sm-12 form-group'>
            <div class='col-sm-6 form-group'>
                <?= $form->field($model, 'first_name')->textInput(['maxlength' => true])->label('First Name') ?>
                <?= $form->field($model, 'hidden_token')->hiddenInput(['id'=>'hidden_Input','class'=>'form-control','value'=>$formTokenName])->label(false)?>
            </div>
            <div class='col-sm-6 form-group'>
                 <?= $form->field($model, 'last_name')->textInput(['maxlength' => true])->label('Last Name') ?>
                 
                
            </div>

        </div>

        <div class='col-sm-12 form-group'>
            <div class='col-sm-6 form-group'>
                <?= $form->field($model, 'gender')->dropdownlist(['Male'=>'Male','Female'=>'Female'],['rows' => 2]) ?>
            </div>
            <div class='col-sm-6 form-group'>
                <?= $form->field($model, 'age')->textInput(['maxlength' => true]) ?>
           
                
            </div>

        </div>

        <div class='col-sm-12 form-group'>
            <div class='col-sm-6 form-group'>
                    <?= $form->field($model, 'mobile_number')->textInput(['maxlength' => true]) ?>
            
                
            </div>
            <div class='col-sm-6 form-group'>
                            <?= $form->field($model, 'email_id')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class='col-sm-12 form-group'>
            <div class='col-sm-6 form-group'>
           
            <?= $form->field($model, 'password_hash')->passwordInput(['maxlength' => true,'value'=>''])->label('Password') ?>
            </div>
        </div>    

        </div>
   
    </div>

    <div class="card-footer text-right">
        <div class="form-group ">
            <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? ' btn btn-create createBtn btn-success clssdyna ' : ' btn btn-primary ','required'=>true]) ?>  
        </div>
    </div>
    <?php ActiveForm::end(); ?>
	
</div> 

<script type="text/javascript">
$('select').select2({
  selectOnClose: true
});
</script>
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Admin Users';
?>

<div class="user-index">
<div class="card-body">
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <h4 class="text-right">
        <?= Html::a('<i class="fa fa-plus"></i> Add', 'adminuser-create.html', ['class' => 'btn btn-success btn-xs text-white','title'=>'Add']) 

           //Html::button('<i class="fa fa-plus"></i>', ['style'=>'margin-right:4px;','class' => 'btn btn-success btn-xs', 'data-toggle'=>'quickview', 'title' =>'Create','data-target'=>"#subscription-add",'data-url'=> Url::base(true)."/user-create.html"]);

        ?>
    </h4>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn',

                 'header'=> 'Action',
               'template'=>'{update}',
                 'headerOptions' => ['style' => 'width:150px;color:#337ab7;'],
                'buttons'=>[
                  'view' => function ($url, $model, $key) {
                        
                       return Html::button('<i class="glyphicon glyphicon-eye-open"></i>', ['value' => $url, 'style'=>'margin-right:4px;','class' => 'btn btn-primary btn-xs view view gridbtncustom modalView', 'data-toggle'=>'tooltip', 'title' =>'View' ]);
                    }, 
                    
                    'delete' => function ($url, $model, $key) {
                    $url=Url::base(true)."/adminuser-delete/".$model->id.".html";
                                    
                                     return Html::button('<i class="fa fa-trash"></i>', ['value' => $url, 'style'=>'margin-right:4px;','class' => 'btn btn-danger btn-xs delete gridbtncustom modalDelete', 'data-toggle'=>'tooltip', 'title' =>'Delete' ]);
                                
                              },
                 'update' => function ($url, $model, $key) {
                             $url = Url::base(true)."/adminuser-update/".$model->id.".html";
                             return Html::a('<span class="fa fa-edit"></span>',$url, [
                                        'value' => "Update",
                                        'title' => Yii::t('yii', 'Update'),
                                        'class' => 'btn btn-warning btn-xs gridbtncustom updatedata',
                                        'data-toggle'=> 'tooltip',
                                        'style'=>'margin-right:4px;']);
                            //return Html::button('<i class="fa fa-edit"></i>', ['style'=>'margin-right:4px;','class' => 'btn btn-warning btn-xs', 'data-toggle'=>'quickview', 'title' =>'Update','data-target'=>"#subscription-add",'data-url'=> Url::base(true)."/user-update/".$model->id.".html"]);
                         
                        },
              ]],
            //'id',
            'first_name',
            'email_id:email',
            
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
</div>

<div id="subscription-add" class="quickview quickview-xxl backdrop-dark" data-fullscreen-on-mobile></div>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $form yii\widgets\ActiveForm */

//echo Url::base(true);die;
?>
<style type="text/css">
     

 

/* Add a hover effect (blue shadow) */
img:hover {
   opacity:0.6
}
.fill{border: 1px solid #f9f9f9;}
.fill img{
    width: 200px;
  height: 300px;
  object-fit: contain;
  
}

.bg-card{
	    background-color: #f5f6fa;
		padding:1.25rem 0;
}
.blue-text{
	color:#2b71bc;
}
.prr-5{position:relative;right:5px;}

 
 
	

</style>
<div class="user-form">
 
    <?php $form = ActiveForm::begin(['enableAjaxValidation' => true,'enableClientValidation' => true],['options' => ['enctype'=>'multipart/form-data','autocomplete'=>'off']]); ?>

    <div class="card-header" style="background-color:#fff;">
        <h4 class="card-title"><span class="fa fa-edit"></span> Client Information</h4>
             <a href="../enquiry-index.html"> <button type="button" class="btn btn-sm btn-info" ><i class="fa fa-arrow-left"></i> Back</button></a>
    </div>
    <div class="card-body bg-card">
      <div class="card ">
    		<!-- <fieldset class="scheduler-border">
    				<legend class="scheduler-border">User Information</legend>

    		</fieldset> --> 
    		<div class="card-header">
        		<h4 class="card-title"> User Information</h4>
    		</div>
    		<div class="card-body">
    			<div class="row">
				   <div class="col-md-3">
            <div class="ca rd card-body mb-0  ">
              <div class="flexbox">
                 <div class=" ">
                  <span class="fw-400  "><?php echo $enquiry->getAttributeLabel('client_name'); ?></span><br>
                  <span>
                     
                    <span class="fs-14 blue-text"><strong ><?php echo $enquiry->user->first_name." ".$enquiry->user->last_name  ; ?></strong>	</span>
                  </span>
                </div>
              </div>
            </div>
          </div>
		  <div class="col-md-3">
            <div class="ca rd card-body mb-0  ">
              <div class="flexbox">
                 <div class=" ">
                  <span class="fw-400   "><?php echo $enquiry->getAttributeLabel('gender_age'); ?></span><br>
                  <span>
                     
                    <span class="fs-14 blue-text"><strong><?php echo $enquiry->user->gender." & ".$enquiry->user->age." Yrs"  ; ?></strong></span>
                  </span>
                </div>
              </div>
            </div>
          </div>
		  <div class="col-md-3">
            <div class="c ard card-body mb-0  ">
              <div class="flexbox">
                 <div class=" ">
                  <span class="fw-400  "><?php echo $enquiry->user->getAttributeLabel('mobile_number'); ?></span><br>
                  <span>
                     
                    <span class="fs-14 blue-text "><strong><?php echo $enquiry->user->mobile_number; ?></strong></span>
                  </span>
                </div>
              </div>
            </div>
          </div>
		  <div class="col-md-3">
            <div class="ca rd card-body mb-0 ">
              <div class="flexbox">
                 <div class=" ">
                  <span class="fw-400  "><?php echo $enquiry->user->getAttributeLabel('email_id'); ?></span><br>
                  <span>
                     
                    <span class="fs-14 blue-text"><strong><?php echo $enquiry->user->email_id; ?></strong></span>
                  </span>
                </div>
              </div>
            </div>
          </div>
		  <div class="col-md-3">
            <div class="ca rd card-body mb-0">
              <div class="flexbox">
                 <div class=" ">
                  <span class="fw-400  "><?php echo $enquiry->getAttributeLabel('image_date'); ?></span><br>
                  <span>
                     
                    <span class="fs-14 blue-text  "><strong><?php echo date('d-m-Y H:i:s',strtotime($enquiry->image_date)); ?></strong></span>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="ca rd card-body mb-0">
              <div class="flexbox">
                 <div class=" ">
                  <span class="fw-400  "><?php echo $enquiry->getAttributeLabel('is_recommentation'); ?></span><br>
                  <span>
                     
                    <span class="fs-14 blue-text"><strong><?php echo (empty($enquiry->is_recommentation)) ? '-' : $enquiry->is_recommentation; ?></strong></span>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="c ard card-body mb-0">
              <div class="flexbox">
                 <div class=" ">
                  <span class="fw-400  "><?php echo $enquiry->getAttributeLabel('enq_date'); ?></span><br>
                  <span>
                     
                    <span class="fs-14 blue-text"><strong><?php echo (empty($enquiry->enq_date))? '-' :date('d-m-Y H:i:s',strtotime($enquiry->enq_date)); ?></strong></span>
                  </span>
                </div>
              </div>
            </div>
          </div>		  
			
      			</div>
    		</div>
    		</div>
			
      
           <div class="card">
		   
		    <div class="card-header">
        		<h4 class="card-title"> Update Recommendation</h4>
    		</div>
		   
		   <div class="card-body">
		   <div class="row">
            <?php 
				$title=array();
				$title= "";
                foreach ($picture as $key => $value) { 
          				  $title= $value['image_type'];	
                    /*if (in_array(str_replace(' ', '_', $value['image_type']), Yii::$app->params['image-type']))
                    {
                    		$title= str_replace('_', ' ', $value['image_type']); 
          					}*/
                    $yes_cavity = ($value->cavity == 'Yes') ? 'selected': '';
                    $no_cavity = ($value->cavity == 'No' ) ? 'selected': '';
                    
                    $yes_broken_filing = ($value->broken_filing == 'Yes') ? 'selected': '';
                    $no_broken_filing = ($value->broken_filing == 'No' ) ? 'selected': '';
                    

                    $url = Url::base(true)."/toothpixpictureshow/".$value->id.".html";
                    $image = str_replace("backend/web", "", $value->image_url);
                  echo "<div class='col-sm-6 text-center '>
				  <div class='card'><span>".$title."</span><div class='card-body fill'>
          <button type='button' data-toggle='quickview' data-target='#photo-update' data-title='".$title."' data-url='".$url."'><div class=''><img src=".Url::base(true).$image." ></div></button>
				  
				  
				  </div>
                    <div class='card-footer'>
                    <div class='row text-left'>
					 <div class='col-sm-6'>
					    <div class='form-group'>
						   <label>Label</label> 
						    <input type='text' class='form-control' name='label[".$value->id."]' value=".$value->label.">
						</div>
					 </div>
					 <div class='col-sm-3'>
					   <div class='form-group'>
					     <label>Cavity</label>
							<select class='form-control' name='cavity[".$value->id."]' >
							  <option ''>--SELECT--</option>
							  <option ".$yes_cavity.">Yes</option>
							  <option ".$no_cavity.">No</option>
							</select>
					   </div>
					 </div>
					 <div class='col-sm-3'>
					   <div class='form-group'>
					      <label>Broken</label>
							<select class='form-control' name='brokenfilling[".$value->id."]' >
							  <option ''>--SELECT--</option>
							  <option ".$yes_broken_filing.">Yes</option>
							  <option ".$no_broken_filing.">No</option>
							</select>
					   </div>
					 
					 </div>
					   
					</div>

				   
					
					
					<div class='row text-left'>
					<div class='col-sm-12'>
					<div class='form-group'>
                       <label>Recommendation</label> 
                     <textarea class='form-control' name='Recommentation[".$value->id."]'>".$value->comments."</textarea> 
					</div>
					</div>
					</div>
					</div>
                  </div></div>";
                    
                }

            ?>

        </div>
        </div>
        </div>
   
    </div>

     <div class="card-footer text-right">
        <div class="form-group ">
            <?= Html::submitButton(($enquiry->is_recommentation == '') ? 'Save' : 'Update', ['class' => ($enquiry->is_recommentation == '') ? ' btn btn-create createBtn btn-success clssdyna ' : ' btn btn-primary ','required'=>true]) ?>  
        </div>
    </div> 
    <?php ActiveForm::end(); ?>
	
</div> 

<div id="photo-update"  class="quickview quickview-xxl backdrop-dark" data-fullscreen-on-mobile></div>



<!-- <script type="text/javascript">
$('select').select2({
  selectOnClose: true
});
</script> -->
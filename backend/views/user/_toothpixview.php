<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\User */

//$this->title = $model->id;
//$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
             [
                'attribute' => 'first_name',
                'label'=>'First Name',
                'value'=>function($models, $keys){
                 if(isset($models->user)){
                    return $models->user->first_name;
                 }},
             ],
             [
                'attribute' => 'last_name',
                'label'=>'Last Name',
                'value'=>function($models, $keys){
                 if(isset($models->user)){
                    return $models->user->last_name;
                 }},
             ],
             [
                'attribute' => 'gender',
                'label'=>'Gender',
                'value'=>function($models, $keys){
                 if(isset($models->user)){
                    return $models->user->gender;
                 }},
             ],
             [
                'attribute' => 'age',
                'label'=>'Age',
                'value'=>function($models, $keys){
                 if(isset($models->user)){
                    return $models->user->age;
                 }},
             ],
             [
                'attribute' => 'mobile_number',
                'value'=>function($models, $keys){
                 if(isset($models->user)){
                    return $models->user->mobile_number;
                 }},
             ],
             [
                'attribute' => 'email_id',
                'value'=>function($models, $keys){
                 if(isset($models->user)){
                    return $models->user->email_id;
                 }},
             ],   
        ],
    ]) ?>

</div>

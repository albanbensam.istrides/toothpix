<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::$app->params['title']['user-index'];
?>

<div class="user-index">
<div class="card-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <h4 class="text-right">
        <?= Html::a('<i class="fa fa-plus"></i> Add', 'user-create.html', ['class' => 'btn btn-success btn-xs text-white','title'=>'Add']) 

           //Html::button('<i class="fa fa-plus"></i>', ['style'=>'margin-right:4px;','class' => 'btn btn-success btn-xs', 'data-toggle'=>'quickview', 'title' =>'Create','data-target'=>"#subscription-add",'data-url'=> Url::base(true)."/user-create.html"]);

        ?>
    </h4>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn',

               'header'=> 'Action',
               'template'=>'{update}',
                 'headerOptions' => ['style' => 'width:150px;color:#337ab7;'],
                'buttons'=>[
                  'view' => function ($url, $model, $key) {
                       $url=Url::base(true)."/enquiry-view/".$model->id.".html";
                                        
                       return Html::button('<i class="glyphicon glyphicon-eye-open"></i>', ['value' => $url, 'style'=>'margin-right:4px;','class' => 'btn btn-primary btn-xs view view gridbtncustom modalView', 'data-toggle'=>'tooltip', 'title' =>'View' ]);
                    }, 
                    
                    'delete' => function ($url, $model, $key) {
                    $url=Url::base(true)."/user-delete/".$model->id.".html";
                                    
                                     return Html::button('<i class="fa fa-trash"></i>', ['value' => $url, 'style'=>'margin-right:4px;','class' => 'btn btn-danger btn-xs delete gridbtncustom modalDelete', 'data-toggle'=>'tooltip', 'title' =>'Delete' ]);
                                
                              },
                 'update' => function ($url, $model, $key) {
                             $url = Url::base(true)."/toothpixdata/".$model->id.".html";
                             return Html::a('<span class="fa fa-edit"></span>',$url, [
                                        'value' => "Update",
                                        'title' => Yii::t('yii', 'Update'),
                                        'class' => 'btn btn-warning btn-xs gridbtncustom updatedata',
                                        'data-toggle'=> 'tooltip',
                                        'style'=>'margin-right:4px;']);
                        },

                  'profile' => function ($url, $model, $key) {
                             $url = Url::base(true)."/profile/".$model->id.".html";
                             return Html::a('<span class="fa fa-user"></span>',$url, [
                                        'target' => "_blank",
                                        'value' => "Print",
                                        'title' => Yii::t('yii', 'Print'),
                                        'class' => 'btn btn-success btn-xs gridbtncustom updatedata',
                                        'data-toggle'=> 'tooltip',
                                        'style'=>'margin-right:4px;']);
                        },      
                  
              ]],
            //'id',
            //'username',
            
            //'random_subscribe_no_data',
            [
                'attribute' => 'clients_name',
                'value'=>function($models, $keys){
                    return $models->first_name.' '.$models->last_name;
                 },
             ],

             [
                'attribute' => 'gender_age',
                'value'=>function($models, $keys){
                    return $models->gender.','.$models->age;
                 },
             ],
            
            //'dob',
            //'user_type',
            //'city',
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
            //'status',
            //'created_at',
            //'updated_at',
            //'rights:ntext',
            //'status_flag',
            //'user_level',
            'mobile_number',
            'email_id:email',
            //'designation',
            //'user_role',
            //'address:ntext',
            //'aadhar_number',
            //'subscripe_plan',
            //'refferal_id',

            //'refferal_name',
            //'bank_name',
            //'bank_account_no',
            //'ifsc',
            //'branch',
            
            //'create_at',
            //'update_at',

            /*['class' => 'yii\grid\ActionColumn',

                 'header'=> 'Profile Image',
               'template'=>'{image}',
                 'headerOptions' => ['style' => 'color:#337ab7;'],
                'buttons'=>[
                  
                  'image' => function ($url, $model, $key) {
                             if(!empty($model->profile_image))
                             {
                             $temp = str_replace('/backend/web', '', $model->profile_image);
                             $url = Url::base(true).$temp;
                             return Html::a('View',$url, [
                                        'value' => "Image",
                                        'target' => "_blank",
                                        'title' => Yii::t('yii', 'Image'),
                                        'class' => 'btn btn-success btn-xs gridbtncustom updatedata',
                                        'data-toggle'=> 'tooltip',
                                        'style'=>'margin-right:4px;']);
                            }
                        },
              ]],*/

              /*['class' => 'yii\grid\ActionColumn',

                 'header'=> 'KYC',
               'template'=>'{pdf}',
                 'headerOptions' => ['style' => 'color:#337ab7;'],
                'buttons'=>[
                 
                        'pdf' => function ($url, $model, $key) {
                             if(!empty($model->profile_pdf))
                             {
                             $temp = str_replace('/backend/web', '', $model->profile_pdf);
                             $url = Url::base(true).$temp;
                             return Html::a('Download',$url, [
                                        'download' => true,
                                        'value' => "Image",
                                        'target' => "_blank",
                                        'title' => Yii::t('yii', 'Image'),
                                        'class' => 'btn btn-success btn-xs gridbtncustom updatedata',
                                        'data-toggle'=> 'tooltip',
                                        
                                        'style'=>'margin-right:4px;']);
                            }
                        },
              ]],*/
        ],
    ]); ?>
    
</div>
</div>

<?php
   use yii\helpers\Html;
   use yii\widgets\ActiveForm;
   use yii\helpers\Url;
   /* @var $this yii\web\View */
   /* @var $model backend\models\User */
   /* @var $form yii\widgets\ActiveForm */
   
   $image = str_replace("backend/web", "", $picture->image_url);
   ?>
<script src="<?php echo Url::base(true)."/zoom/tasks/panzoom.min.js"; ?>"></script>
<style>
  #panzoom img{
 width: 400px;
  height: 300px;
  object-fit: contain;	
}
</style>
<div class="user-form">


<div class="enquiry-view">
<header class="quickview-header">
        <h4><p class="quickview-title"><span class="fa fa-edit"></span> <?php echo $picture->image_type." Picture"; ?>  </p></h4>
        <span class="close"><i class="ti-close"></i></span>
      </header>
 <div class="quickview-body center-vh ps-container ps-theme-default">
 <div class="quickview-block">

   <div class="card">
      
      <div class="card-body text-center">
        <div class="row">
		<div class="col-sm-offset-3 col-sm-6">
           <div id="panzoom">
            <?php echo "<img src=".Url::base(true).$image.">"; ?>
           </div>
         </div>
        </div>
      </div>
      <footer class="card-footer flexbox">
         <div class="text-right flex-grow">
            <button id="zoom-in" class="btn btn-info btn-sm">Zoom In</button>
            <button class="zoom-out btn btn-info btn-sm">Zoom Out</button>
            <button class="reset btn  btn-sm">Reset</button>
         </div>
      </footer>
   </div>
   </div>
   </div>
   </div>
</div>
 
<script type="text/javascript">
   var  element = document.getElementById('panzoom')
   var panzoom = Panzoom(element, {});
   
   $("#zoom-in").click(function(){
      panzoom.zoomIn(element);
   });
   
   $(".zoom-out").click(function(){
      panzoom.zoomOut(element);
   });
   
   $(".reset").click(function(){
      panzoom.reset(element);
   }); 
   $('.dashboard-card').removeClass('card');
</script>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">
 
    <?php $form = ActiveForm::begin(['enableAjaxValidation' => true,'enableClientValidation' => true]); ?>

    <div class="card-header">
        <h4 class="card-title"><?php echo $model->isNewRecord ? '<span class="fa fa-plus"></span> Add Admin User' : '<span class="fa fa-edit"></span> Update Admin User'; ?></h4>
            <!-- <span class="close"><i  class="ti-close"></i></span> -->
    </div>
    <div class="card-body">
      <div class="row">
        <div class='col-sm-12 form-group'>
            
            <div class='col-sm-6 form-group'>
                <?= $form->field($model, 'user_role')->dropDownList(['S' => 'Super Admin','A' => 'Admin'],['maxlength' => true]) ?>
            </div>



            <div class='col-sm-6 form-group'>
                <?= $form->field($model, 'first_name')->textInput(['maxlength' => true])->label('First Name') ?>
                <?= $form->field($model, 'hidden_token')->hiddenInput(['id'=>'hidden_Input','class'=>'form-control','value'=>$formTokenName])->label(false)?>
            </div>
  
        </div>


        <div class='col-sm-12 form-group'>
            
            <div class='col-sm-6 form-group'>
                <?= $form->field($model, 'email_id')->textInput(['maxlength' => true]) ?>
            </div>



            <div class='col-sm-6 form-group'>
                <?= $form->field($model, 'password_hash')->passwordInput(['maxlength' => true,'value' => ""])->label('Password') ?>
            </div>
        
            
        </div>
        </div>
   
    </div>

    <div class="card-footer text-right">
        <div class="form-group ">
            <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? ' btn btn-create createBtn btn-success clssdyna ' : ' btn btn-primary ','required'=>true]) ?>  
        </div>
    </div>
    <?php ActiveForm::end(); ?>
	
</div> 

<script type="text/javascript">
$('select').select2({
  selectOnClose: true
});
</script>
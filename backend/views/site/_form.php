<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model backend\models\SubscriptionMaster */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data','autocomplete'=>'off']]); ?>
<?php $form = ActiveForm::begin([

                    'enableAjaxValidation' => true,
                    'enableClientValidation' => true,
                    ]); ?>
    
<div class="subscription-master-form">
    
   <header class="quickview-header">
        <h4 class="quickview-title"><?php echo $model->isNewRecord ? '<span class="fa fa-plus"></span> ' : '<span class="fa fa-edit"></span> '; ?>Subscription Master</h4>
    <!-- <span class="close"><i  class="ti-close"></i></span> -->
  </header>
  <div class="quickview-body">
    <div class="quickview-block">
        
        <div class='col-sm-12 form-group'>
            <div class='col-sm-6 form-group'>
                <?= $form->field($model, 'package_name')->textInput(['maxlength' => true,'data-bv-notempty'=>true,'data-bv-notempty-message'=>'Package Name Is Required and Cannot be Empty']) ?>

                <?= $form->field($model, 'hidden_token')->hiddenInput(['id'=>'hidden_Input','class'=>'form-control','value'=>$formTokenName])->label(false)?>
            </div>
            <div class='col-sm-6 form-group'>
                <?= $form->field($model, 'package_amount')->textInput(['maxlength' => 10,"type"=>"number",'data-bv-notempty'=>true,'data-bv-notempty-message'=>'Package Amount Is Required and Cannot be Empty',"data-bv-integer-message"=>"Value is Not an Integer",'class'=>' form-control']) ?>
            </div>
        </div>  

        <div class='col-sm-12 form-group'>
            <div class='col-sm-6 form-group'>
                <?= $form->field($model, 'description')->textarea(['rows' => 1]) ?>
            </div>
            <div class='col-sm-6 form-group'>
                <br>
                <?php 
                  if($model->isNewRecord){
                  $checked = 'checked';
                  }else{
                    
                    if($model->status == '0')
                    {
                      $checked = '';
                    }
                    else
                    {
                      $checked = 'checked';
                    }

                  }?>
             <?= $form->field($model, 'status', [
                     'template' => " <label class='switch switch-border switch-info'>
                        <input type='checkbox' ".$checked." name='SubscriptionMaster[status]'>
                        <span class='switch-indicator'></span>
                        <span class='switch-description'>Is Active</span>
                      </label>",
                     ])->checkbox([],false) ?>
            </div>
        </div>
    
    
</div>
</div>

    <footer class="quickview-footer">
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-create createBtn btn-success clssdyna btn-xs' : 'btn btn-primary btn-xs','required'=>true]) ?>  
        </div>
        <button type="button"  class="btn btn-danger btn-xs"  data-dismiss="quickview" data-always-reload="true">Close</button>
    </footer>


</div>
 <?php ActiveForm::end(); ?>

<script type="text/javascript">
      $('input[maxlength]').maxlength({
            alwaysShow: true,
            placement: 'bottom-right'
        });
$(document).ready(function() {
    $('#w1').bootstrapValidator();
});
 </script> 

<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
$this->title = 'Toothpix - LOGIN';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet"> -->
<link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/images/favicon.ico" type="image/x-icon" />
<link href="<?php echo Url::base(); ?>/Lato/lato.css" rel="stylesheet">
    <div class="row no-gutters min-h-fullscreen bg-white">
      <div class="col-md-6 col-lg-7 col-xl-8 d-none d-md-block bg-img"  style="background-image: url(<?php echo Url::base(); ?>/images/images/services.jpg)" data-overlay="7">

        <div class="row h-100 pl-50">
          <div class="col-md-10 col-lg-8 align-self-end">
            <!-- <img src="<?php echo Url::base(); ?>/theme-plugin/assets/img/logo-light-lg.png" alt="...">-->
			
          </div>
        </div>

      </div>
      <div class="col-md-6 col-lg-5 col-xl-4 al ign-self-center">
	    <center style="padding:10px 0px 15px 0px;"> <h1>Toothpix</h1> <!-- <img src="<?php //echo  Url::base(); ?>/images/abdhi-logo.png" alt=" " width="200"  > --> </center>
        <div class="px-80 py-30 br-top-logo">
			<br>
		  <center><h4>LOGIN</h4></center>
          <!-- <p><small>Sign into your account</small></p> -->
           <br>
          <?php $form = ActiveForm::begin(['options'=> ['autocomplete'=>'off','class' => 'form-type-material']]); ?>  
          <?= Html::csrfMetaTags() ?>
         <!--  <form class="form-type-material"> -->
            <div class="form-group">
              <!-- <input type="text" class="form-control" id="username">
              <label for="username">Username</label> -->
              <?= $form->field($model, 'email_id')->textInput(['class'=>'form-control','required'=>true])->label('Email');  ?> 
            </div>

            <div class="form-group">
              <!-- <input type="password" class="form-control" id="password">
              <label for="password">Password</label> -->
              <?= $form->field($model, 'password')->passwordInput(['class'=>'form-control','required'=>true])->label('Password');  ?> 
            </div>

            <!-- <div class="form-group flexbox">
              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" checked>
                <label class="custom-control-label">Remember me</label>
              </div>

              <a class="text-muted text-firstman1 fs-13" href="#">Forgot password?</a>
            </div> -->

            <div class="form-group">
              <button class="btn btn-bold btn-block btn-firstman" type="submit">Login</button>
            </div>
             <?php ActiveForm::end(); ?>
          <!-- </form> -->
        <!--
          <div class="divider">Or Sign In With</div>
          <div class="text-center">
            <a class="btn btn-square btn-facebook" href="#"><i class="fa fa-facebook"></i></a>
            <a class="btn btn-square btn-google" href="#"><i class="fa fa-google"></i></a>
            <a class="btn btn-square btn-twitter" href="#"><i class="fa fa-twitter"></i></a>
          </div>

          <hr class="w-30px">

          <p class="text-center text-muted fs-13 mt-20">Don't have an account? <a class="text-primary fw-500" href="#">Sign up</a></p>
        -->
		
		
		</div>
      </div>
    </div>

 

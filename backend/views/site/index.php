<?php
use yii\helpers\Url;
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Toothpix - Dashboard';

//Set Default Timezone
date_default_timezone_set('Asia/Kolkata');
?>

<style type="text/css">
  .inforide {
  box-shadow: 1px 2px 8px 0px #f1f1f1;
  background-color: white;
  border-radius: 8px;
  height: 125px;
  }
  .rideone {
  background-color: #88397D;
  color:#fff;
  padding-top: 40px;
  border-radius: 8px 0px 0px 8px;
  text-align: center;
  height: 125px;
  margin-left: 15px;
  }
  .ridetwo {
  background-color: #206D21;
  color:#fff;
  padding-top: 40px;
  border-radius: 8px 0px 0px 8px;
  text-align: center;
  height: 125px;
  margin-left: 15px;
  }
  .ridethree {
  background-color: #C53600;
  color:#fff;
  padding-top: 40px;
  border-radius: 8px 0px 0px 8px;
  text-align: center;
  height: 125px;
  margin-left: 15px;
  }
  .ridefour {
  background-color: #4EBCE5;
  color:#fff;
  padding-top: 40px;
  border-radius: 8px 0px 0px 8px;
  text-align: center;
  height: 125px;
  margin-left: 15px;
  }
  .fontsty {
  margin-right: -15px;
  }
  .fontsty h2{
  color: #444343;
  font-size: 25px;
  margin-top: 15px;
  text-align: right;
  margin-right: 30px;
  }
  .fontsty h4{
  color: #505050;
    font-size: 15px;
    margin-top: 20px;
    text-align: right;
    margin-right: 30px;
	min-height: 44px;
  }
  .widget-visitor-card {
  overflow: hidden;
  padding: 10px 0;
  }
  .bg-c-green {
  background: -webkit-gradient(linear,left top,right top,from(#0ac282),to(#0df3a3));
  background: linear-gradient(to right,#0ac282,#0df3a3);
  }
  .bg-c-blue {
  background: -webkit-gradient(linear,left top,right top,from(#01a9ac),to(#01dbdf));
  background: linear-gradient(to right,#01a9ac,#01dbdf);
  }
  .bg-c-pink {
  background: -webkit-gradient(linear,left top,right top,from(#fe5d70),to(#fe909d));
  background: linear-gradient(to right,#fe5d70,#fe909d);
  }
  .bg-c-yellow {
  background: -webkit-gradient(linear,left top,right top,from(#fe9365),to(#feb798));
  background: linear-gradient(to right,#fe9365,#feb798);
  }
  .widget-visitor-card i {
  color: #fff;
  font-size: 70px;
  position: absolute;
  bottom: 0px;
  opacity: .3;
  left: 25px;
  -webkit-transform: rotate(15deg);
  transform: rotate(15deg);
  -webkit-transition: all .3s ease-in-out;
  transition: all .3s ease-in-out;
  }
  .text-white{color:#fff;}
  .widget-visitor-card:hover i {
  -webkit-transform: rotate(0deg) scale(1.4);
  transform: rotate(0deg) scale(1.4);
  opacity: .5;
  }
  .widget-visitor-card:hover h3{font-weight:bold;}
  h2.panel-title{font-size:18px; }
</style>
 
 <div class="row">
  <div class="col-lg-3 col-md-4 col-sm-6 col-12 mb-2 mt-4">
    <div class="inforide">
      <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-4 col-4 rideone">
          <i class="fa fa-users   fa-3x"></i>                
        </div>
        <div class="col-lg-9 col-md-8 col-sm-8 col-8 fontsty">
          <h4>Total Clients</h4>
          <h2><?= $data_array['active_user']; ?></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-md-4 col-sm-6 col-12 mb-2 mt-4">
    <div class="inforide">
      <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-4 col-4 ridefour">
          <i class="fa fa-book   fa-3x"></i>                
        </div>
        <div class="col-lg-9 col-md-8 col-sm-8 col-8 fontsty">
          <h4>New Toothpix</h4>
          <h2><?= $data_array['new_toothpix']; ?></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-md-4 col-sm-6 col-12 mb-2 mt-4">
    <div class="inforide">
      <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-4 col-4 ridetwo">
          <i class="fa fa-check-square   fa-3x"></i>                
        </div>
        <div class="col-lg-9 col-md-8 col-sm-8 col-8 fontsty">
          <h4>Total Recommendation</h4>
          <h2><?= $data_array['total_recomment']; ?></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-md-4 col-sm-6 col-12 mb-2 mt-4">
    <div class="inforide">
      <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-4 col-4 ridethree">
          <i class="fa fa-spinner fa-spin   fa-3x"></i>                
        </div>
        <div class="col-lg-9 col-md-8 col-sm-8 col-8 fontsty">
          <h4>Total Toothpix</h4>
          <h2><?= $data_array['total_toothpix']; ?></h2>
        </div>
      </div>
    </div>
  </div>
  
</div>
 <br>
<!-- <div class="panel panel-primary">
  <div class="panel-heading">
    <h2 class="panel-title">  Latest 5 Active User</h2>
  </div>
  <div class="panel-body">
    <table class="table table-bordered table-striped">
      <thead>
        <tr>
          <th scope="col">User Name</th>
          <th scope="col">Email ID</th>
          <th scope="col">Mobile No</th>
          <th scope="col">Refered By</th>
        </tr>
      </thead>
      <tbody>
        
      </tbody>
    </table>
  </div>
</div>
<div class="panel panel-primary">
  <div class="panel-heading">
    <h2 class="panel-title"> Latest 5 Transaction History</h2>
  </div>
  <div class="panel-body">
    <table class="table table-bordered table-striped">
      <thead>
        <tr>
          <th scope="col">User Name</th>
          <th scope="col">Subscription ID</th>
          <th scope="col">Email ID</th>
          <th scope="col">Mobile No</th>
          <th scope="col">Refered By</th>
          <th scope="col">Transaction ID</th>
          <th scope="col">Package Name</th>
          <th scope="col">Amount</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
</div> -->

<script>
$(document).ready(function(){
	 $('.dashboard-card').removeClass('card');
});

</script> 
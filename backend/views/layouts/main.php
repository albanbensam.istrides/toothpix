<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\DashboardAsset;
use backend\controllers\SiteController;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use  yii\web\Session;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\helpers\Url;
use yii\bootstrap\Model;
use yii\widgets\Pjax;

DashboardAsset::register($this);
$session = Yii::$app->session;
?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content=" ">
    <meta name="keywords" content="">
<title>Toothpix</title>	
	<link href="<?php echo Url::base(); ?>/Lato/lato.css" rel="stylesheet">
  <!--  favicon -->
<!-- <link rel="shortcut icon" href="<?php //echo Yii::$app->request->baseUrl; ?>/images/favicon.ico" type="image/x-icon" /> -->
<style type="text/css">
  #date_time {
    color: #928989;
    position: relative;
    font-weight: bold;
}
 @media (max-width: 767px){
.topbar .lookup-circle.lookup-right.tit-bar::before {
    display: none !important;
}
}
</style>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!-- Fonts -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,300i|Dosis:300,500" rel="stylesheet"> -->

    <!-- Styles -->
    <!-- <link href="theme-plugin/assets/css/core.min.css" rel="stylesheet">
    <link href="theme-plugin/assets/css/app.min.css" rel="stylesheet">
    <link href="theme-plugin/assets/css/style.min.css" rel="stylesheet"> -->

    <!-- Favicons
    <link rel="apple-touch-icon" href="<?php // echo Url::base(); ?>/theme-plugin/assets/img/apple-touch-icon.png">  -->
   <!--  <link rel="icon" href="<?php // echo Url::base(); ?>/theme-plugin/assets/img/favicon.png"> -->

    <!--  Open Graph Tags -->
   <!--  <meta property="og:title" content="The Admin Template of 2018!">
    <meta property="og:description" content="TheAdmin is a responsive, professional, and multipurpose admin template powered with Bootstrap 4.">
    <meta property="og:image" content="http://thetheme.io/theadmin/assets/img/og-img.jpg">
    <meta property="og:url" content="http://thetheme.io/theadmin/">
    <meta name="twitter:card" content="summary_large_image"> -->
  </head>

  <body data-provide="pace">

  <?php $this->beginBody() ?>

  <!-- Preloader -->
    <!-- <div class="preloader">
      <div class="spinner-dots">
        <span class="dot1"></span>
        <span class="dot2"></span>
        <span class="dot3"></span>
      </div>
    </div> -->


     <!-- Sidebar -->


     <?php  echo $this->render('left_menu.php'); ?>
 


     <!-- Topbar -->
    <header class="topbar">
	<div class="topbar-left">
        <span class="topbar-btn sidebar-toggler"><i>☰</i></span>
       
        <ul class="topbar-btns">

         <li>  <span class="lookup lookup-circle lookup-right tit-bar">
		    <span class="page-headr" id="title" style="font-weight: 600;"><span><?= strtoupper(Html::encode($this->title)) ?></span> 
                <input type="text" id="shortcode" placeholder="Search" class="hidden-xs">
          </span>
        </span></li>

          

        </ul>
      </div>
	
	
	
	
	
      <!-- <div class="topbar-left">
        <span class="topbar-btn sidebar-toggler"><i>&#9776;</i></span>

         <a class="topbar-btn d-none d-md-block" href="#" data-provide="fullscreen">
           <i class="material-icons fullscreen-default">fullscreen</i>
          <i class="material-icons fullscreen-active">fullscreen_exit</i> 
        </a> -->

        <!-- <div class="dropdown d-none d-md-block">
          <span class="topbar-btn" data-toggle="dropdown"><i class="ti-layout-grid3-alt"></i></span>
          <div class="dropdown-menu dropdown-grid">
            <a class="dropdown-item" href="../dashboard/general.html">
              <span data-i8-icon="home"></span>
              <span class="title">Dashboard</span>
            </a>
            <a class="dropdown-item" href="../page/gallery.html">
              <span data-i8-icon="stack_of_photos"></span>
              <span class="title">Gallery</span>
            </a>
            <a class="dropdown-item" href="../page/search.html">
              <span data-i8-icon="search"></span>
              <span class="title">Search</span>
            </a>
            <a class="dropdown-item" href="../page-app/calendar.html">
              <span data-i8-icon="calendar"></span>
              <span class="title">Calendar</span>
            </a>
            <a class="dropdown-item" href="../page-app/chat.html">
              <span data-i8-icon="sms"></span>
              <span class="title">Chat</span>
            </a>
            <a class="dropdown-item" href="../page-app/mailbox.html">
              <span data-i8-icon="invite"></span>
              <span class="title">Emails</span>
            </a>
            <a class="dropdown-item" href="../page-app/users.html">
              <span data-i8-icon="contacts"></span>
              <span class="title">Contacts</span>
            </a>
            <a class="dropdown-item" href="../widget/chart.html">
              <span data-i8-icon="bar_chart"></span>
              <span class="title">Charts</span>
            </a>
            <a class="dropdown-item" href="../page/profile.html">
              <span data-i8-icon="businessman"></span>
              <span class="title">Profile</span>
            </a>
          </div>
        </div> 

</div> -->
        <!-- <div class="topbar-divider d-none d-md-block"></div> -->
           

			<!--   <span class="lookup lookup-circle lookup-right">
		    <span class="page-headr" id="title" style="font-weight: 600;"><span><?= strtoupper(Html::encode($this->title)) ?></span> 
                <input type="text" id="shortcode" placeholder="Search">
          </span>
        </span> -->
		   
        <!-- <div class="lookup d-none d-md-block topbar-search col-sm-4" id="theadmin-search">
          <input class="form-control srch" type="text" id="shortcode" placeholder="Search">
          <div class="lookup-placeholder">
            <i class="ti-search"></i>
            <span><strong></strong></span>
          </div>
        </div> -->
		
		<div class="user-block">
		    <h6 class="fw-900 text-center text-black ">
		      <b class="hidden-xs"><?php echo ucfirst('Admin');?></b>
		    </h6>
    </div>
		
		 
      <div class="user-block"><h6 class="text-center hidden-xs"><?php echo ucfirst('Admin');?></h6></div>
    
      <div class="topbar-right">
       <!-- <a class="topbar-btn" href="#qv-global" data-toggle="quickview"><i class="ti-align-right"></i></a> -->
      
        
       <div class="topbar-divider"></div>  
       
        <ul class="topbar-btns">
		 
          <li class="dropdown ">
		  
             <span class="topbar-btn" data-toggle="dropdown"><img class="avatar" src="<?php echo Url::base(); ?>/theme-plugin/assets/img/avatar/1.jpg" alt="..."></span>
            <div class="dropdown-menu custom-log-dropdown dropdown-menu-right ">
              <!-- <a class="dropdown-item" href="#"><i class="ti-user"></i>Profile</a> -->
            <?php if (Yii::$app->user->isGuest) {
                  echo Html::a('<i class="ti-power-off"></i>Logout', Url::base().'/logout.html', ['class'=>'dropdown-item']);
                 }
                 else {
                echo Html::a('<i class="ti-power-off"></i>Logout', Url::base().'/logout.html', ['class'=>'dropdown-item']);
                 }
                ?>
              
            </div>
          </li>

          <!-- Notifications -->
          <li class="dropdown d-none d-md-block">
            <!-- <span class="topbar-btn has-new" data-toggle="dropdown"><i class="ti-bell"></i></span> -->
            <div class="dropdown-menu dropdown-menu-right">

              <div class="media-list media-list-hover media-list-divided media-list-xs">
                <a class="media media-new" href="#">
                  <span class="avatar bg-success"><i class="ti-user"></i></span>
                  <div class="media-body">
                    <p>New user registered</p>
                    <time datetime="2018-07-14 20:00">Just now</time>
                  </div>
                </a>

                <a class="media" href="#">
                  <span class="avatar bg-info"><i class="ti-shopping-cart"></i></span>
                  <div class="media-body">
                    <p>New order received</p>
                    <time datetime="2018-07-14 20:00">2 min ago</time>
                  </div>
                </a>

                <a class="media" href="#">
                  <span class="avatar bg-warning"><i class="ti-face-sad"></i></span>
                  <div class="media-body">
                    <p>Refund request from <b>Ashlyn Culotta</b></p>
                    <time datetime="2018-07-14 20:00">24 min ago</time>
                  </div>
                </a>

                <a class="media" href="#">
                  <span class="avatar bg-primary"><i class="ti-money"></i></span>
                  <div class="media-body">
                    <p>New payment has made through PayPal</p>
                    <time datetime="2018-07-14 20:00">53 min ago</time>
                  </div>
                </a>
              </div>

              <div class="dropdown-footer">
                <div class="left">
                  <a href="#">Read all notifications</a>
                </div>

                <div class="right">
                  <a href="#" data-provide="tooltip" title="Mark all as read"><i class="fa fa-circle-o"></i></a>
                  <a href="#" data-provide="tooltip" title="Update"><i class="fa fa-repeat"></i></a>
                  <a href="#" data-provide="tooltip" title="Settings"><i class="fa fa-gear"></i></a>
                </div>
              </div>

            </div>
          </li>
          <!-- END Notifications -->

          <!-- Messages -->
          
          <!-- END Messages -->

        </ul>

      </div>
    </header>
    <!-- END Topbar -->


    <!-- Main container -->
    <main class="main-container">

      <!-- <header class="header bg-ui-general">
        <div class="header-info">
          <h1 class="header-title">
            <strong>Sidebar</strong> documentation
            <small>Sidebar is the main navigation for most of admin templates and web apps.</small>
          </h1>
        </div>

        <div class="header-action">
          <nav class="nav">
            <a class="nav-link" href="sidebar.html">Variations</a>
            <a class="nav-link active" href="sidebar-doc.html">Documentation</a>
          </nav>
        </div>
      </header> --><!--/.header -->


      <div class="main-content">

 <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>


        <div class="card dashboard-card">
          
             
        <?= Alert::widget() ?>
        <?= $content ?>
           <!--  <h4 class="card-title"><strong>Dashboard</strong></h4> -->
          
 
          <div class="media-list media-list-divided media-list-hover">
          </div>
        </div>
      </div><!--/.main-content -->


      <!-- Footer -->
       <?php $this->beginContent('@backend/views/layouts/fooder.php'); ?>
       <?php $this->endContent(); ?>
      <!-- END Footer -->

    </main>
    <!-- END Main container -->


    <!-- Global quickview -->
    <div id="qv-global" class="quickview" data-url="../assets/data/quickview-global.html">
      <div class="spinner-linear">
        <div class="line"></div>
      </div>
    </div>


<script type="text/javascript" src="<?php echo Url::base(); ?>/boot/bootstrap3-typeahead.js"></script>
<script type="text/javascript" src="<?php echo Url::base(); ?>/boot/bootstrap3-typeahead.min.js"></script>
   
    <!-- END Global quickview -->
  <?php  $this->endBody() ?>
  </body>


</html>
<?php $this->endPage() ?>

<script type="text/javascript">
   function date_time(id)
   {
      date = new Date;
      year = date.getFullYear();
      month = date.getMonth();
      months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
      d = date.getDate();
      day = date.getDay();
      days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
      h = date.getHours();
      if(h<10)
      {
      h = "0"+h;
      }
      m = date.getMinutes();
      if(m<10)
      {
      m = "0"+m;
      }
      s = date.getSeconds();
      if(s<10)
      {
      s = "0"+s;
      }
      result = ''+days[day]+', '+months[month]+' '+d+' '+year+' <br> '+h+':'+m+':'+s;
      document.getElementById(id).innerHTML = result;
      setTimeout('date_time("'+id+'");','1000');
      return true;
   }
   
   $("body").on('keypress', '.number', function (e) 
      {
          //if the letter is not digit then display error and don't type anything
          if( e.which!=8 && e.which!=0 && (e.which<48 || e.which>57))
          {
            return false;
          }
          });
   
</script>
<script type="text/javascript">window.onload = date_time('date_time');</script>





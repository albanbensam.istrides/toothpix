<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;


?>


    <!-- <footer class="site-footer">
        <div class="row">
          <div class="col-md-6">
            <p class="text-center text-md-left">Copyright © <?= date('Y') ?> FirstMan CRM. All rights reserved.</p>
          </div>
        </div>
      </footer> -->


<!--Common Modal Starts For Custom Operation -->
<?php 
    Modal::begin([ 
                    'id' => 'operationalmodal',
                    'size' => 'modal-lg',					
                ]);
      echo '
            <div class="modal modal-fill fade" id="enquiry-center0" tabindex="-1">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title">Test Header</h5>
                            <button type="button" class="close" data-dismiss="modal">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                                
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-bold btn-pure btn-secondary" data-dismiss="modal">Close</button>
                            <!-- <button type="button" class="btn btn-bold btn-pure btn-primary">Save changes</button> -->
                          </div>
                        </div>
                      </div>   
                      </div>';
    Modal::end();

?>

<div class="modal modal-fill fade" id="enquiry-center" tabindex="-1">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title">Test Header</h5>
                            <button type="button" class="close" data-dismiss="modal">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                                
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-bold btn-pure btn-secondary" data-dismiss="modal">Close</button>
                            <!-- <button type="button" class="btn btn-bold btn-pure btn-primary">Save changes</button> -->
                          </div>
                        </div>
                      </div>   
                      </div>   
<!--Common Modal End -->






<div class="se-pre-con"></div>
 <style>
  /* Paste this css to your style sheet file or under head tag */
/* This only works with JavaScript, 
if it's not present, don't show loader */
.se-pre-con {
  position: fixed;
  left: 0px;
  top: 0px;
  width: 100%;
  height: 100%;
  z-index: 9999;
  opacity:0.6;
  background: url(<?php echo Url::base();?>/images/loader/45.gif) center no-repeat #fff;
}
 </style>
<!--Common Modal End -->
<script type="text/javascript">
  $(".se-pre-con").fadeOut(1500);


  $(document).on('click', 'button', function(e){
    
    var data=$(this).attr('data-toggle');
    if(data === 'quickview')
    {
      $(".se-pre-con").fadeIn(); 

       setInterval(function(){ 
           $(".se-pre-con").fadeOut(); 
       }, 500);
    }
    
  });

  $('#modal-popup').modal('hide');

$(document).on('click', '.modalDelete', function(e){
       // e.preventDefault();
         var url = $(this).val();
            $('.deletatag').attr("href", url);
            $('#customheader').html(' <h3 class="modal-title" id="myModalLabel"><span style="color:red;"> <i class="fa fa-trash"></i> Delete Confirmation</span></h3>');
            $('#textContent').html('<h4>Are you sure you want to <span style="color:red;">DELETE</span> this item ?</h4>');
            $('#modal-popup').modal('show');
    });

$("body").on('keypress', '.number', function (e) 
{
  //if the letter is not digit then display error and don't type anything
  if( e.which!=8 && e.which!=0 && (e.which<48 || e.which>57))
  {
    return false;
  }
});



function RemoveClass()
{
   $(".alert").removeClass("alert-warning");
   $(".alert").removeClass("alert-success");
   $(".alert").removeClass("alert-danger");
}

</script>

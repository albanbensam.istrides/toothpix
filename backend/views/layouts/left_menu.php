<?php


use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;

$session = Yii::$app->session;
//$session['user_logintype'];
$url=Url::base(true);

$controler_url_id = Yii::$app ->controller->id;
$active_url_id = Yii::$app ->controller->action->id;
//print_r($controler_url_id.'/'.$active_url_id);die;
                echo '<aside class="sidebar sidebar-icons-right sidebar-icons-boxed sidebar-expand-sm">
                      <header class="sidebar-header">
                        <!--  <a class="logo-icon" href="../index.html">
                         <img src="Url::base();/theme-plugin/assets/img/logo-icon-light.png" alt="logo icon"></a> -->
                        <span class="logo">
                         <h2> Toothpix <!--a href="#">
                             <img src="'.Url::base().'/images/abdhi-logo.png" alt="logo" width="150" height="40"> 
                           
                          </a-->
						  
                          </h2>
						  
                        </span>
						
                        <span class="sidebar-toggle-fold"></span>
                      </header> 
<strong><span class="pull-right" id="date_time"></span></strong>
                      <nav class="sidebar-navigation">

                          <ul class="menu">';
                        
                      foreach (Yii::$app->params['left_menu'] as $key => $value) {
                            foreach ($value as $key_1 => $value_1) 
                            {
                                $admin_restriction  = Yii::$app->params['admin_restriction'];
                                if($session['user_role'] == 'A')
                                {
                                    if(isset($value_1['name']))
                                    {
                                        if(in_array($value_1['name'], $admin_restriction['name']))
                                        {
                                            continue;
                                        }
                                    }
                                    if(isset($value_1['menu-name']))
                                    {
                                        if(in_array($value_1['menu-name'], $admin_restriction['menu-name']))
                                        {
                                            continue;
                                        }
                                    }
                                }
                                
                                left_menu_function($key_1,$value_1); 
                            }
                      }    
                      echo '</ul></nav></aside>';



      function left_menu_function($key,$value_1)
      {
          $admin_restriction  = Yii::$app->params['admin_restriction'];
          $session = Yii::$app->session;



          $left_menu_associated = Yii::$app->params['left-menu-data'];

          $controler_url_id = Yii::$app ->controller->id;
          $active_url_id = Yii::$app ->controller->action->id;
          $concated = $controler_url_id.'/'.$active_url_id;
          
          $url=Url::base(true);
          if($key == 'identify')
          {
              echo '<li class="menu-category">'.$value_1['name'].'</li>';
          }
          else if($key == 'one')
          {
              $active = "";
              if(array_key_exists($value_1['action'], $left_menu_associated))
              {
                  //$active =  'active'; 
                  /*if('user/adminindex' == $value_1['action'])
                  {  */
                     // print_r($value_1['action']);die;
                      if(in_array($concated, $left_menu_associated[$value_1['action']]))
                      {
                          $active = 'active';
                      }
                  //}
              }
              //$active = ($concated == $value_1['action']) ? 'active' : '';
              echo '<li class="menu-item '.$active.'">
                          <a class="menu-link" href="'.$url.''.$value_1['url'].'">
                            <span class="icon"></span>
                            <span class="title">'.$value_1['name'].'</span>
                          </a>
                        </li>';
          }
          else if($key == 'more')
          {
                $data_action = ArrayHelper::map($value_1['sub-menu'],'action','action');
                $active = '';
                if(array_key_exists($concated, $data_action))
                {
                    $active = 'active open';
                }
                
                if(empty($active))
                {
                    foreach ($data_action as $key => $value) 
                    {
                        if(array_key_exists($value, $left_menu_associated))
                        {
                            if(in_array($concated, $left_menu_associated[$value]))
                            {
                                $active = 'active open';
                                break;
                            }
                        }
                    }
                    
                }
                          
                echo '<li class="menu-item '.$active.'">
                            <a class="menu-link" href="#">
                              <span class="icon "></span>
                              <span class="title">'.$value_1['menu-name'].'</span>
                              <span class="arrow"></span>
                            </a>';
                echo '<ul class="menu-submenu" >';
                foreach ($value_1['sub-menu'] as $key_data => $value_data) {
                      $sub_active = ($value_data['action'] == $concated) ? 'active' : '';

                      if($session['user_role'] == 'A')
                      {
                          if(isset($value_data['name']))
                          {
                              if(in_array($value_data['name'], $admin_restriction['name']))
                              {
                                  continue;
                              }
                          }
                      }


                      if(empty($sub_active))
                      {
                          if(in_array($concated, $left_menu_associated[$value_data['action']]))
                          {
                              $sub_active = 'active';
                          }
                      }

                      echo '<li class="menu-item '.$sub_active.'">
                                  <a class="menu-link" href="'.$url.''.$value_data['url'].'">
                                    <span class="dot"></span>
                                    <span class="title">'.$value_data['name'].'</span>
                                  </a>
                                </li>';
                                 
                }     
                echo '</ul>';
                echo '</li>';
          }
      }    
    ?>
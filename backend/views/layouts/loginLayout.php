<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\LoginAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

LoginAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Toothpix ">
    <meta name="keywords" content="login, signin">

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
   <!-- <link rel="shortcut icon" href="<?php //echo Yii::$app->request->baseUrl; ?>/images/favicon.ico" type="image/x-icon" /> -->
</head>
<body>
<?php $this->beginBody() ?>

   
        <?= Alert::widget() ?>
        <?= $content ?>
   
 <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

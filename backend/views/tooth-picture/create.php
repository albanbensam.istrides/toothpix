<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\ToothPicture */

$this->title = 'Create Tooth Picture';
$this->params['breadcrumbs'][] = ['label' => 'Tooth Pictures', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tooth-picture-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

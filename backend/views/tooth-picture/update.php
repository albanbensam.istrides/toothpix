<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ToothPicture */

$this->title = 'Update Tooth Picture: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tooth Pictures', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tooth-picture-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

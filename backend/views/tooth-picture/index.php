<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ToothPictureSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::$app->params['title']['tooth-index'];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
<div class="card-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn',

                'header'=> 'Action',
                'template'=>'{update}{profile-pdf}',
                'headerOptions' => ['style' => 'width:150px;color:#337ab7;'],
                'buttons'=>[  
                 'update' => function ($url, $model, $key) {
                            $url = Url::base(true)."/tooth-update/".$model->id.".html";
                            return Html::a('<span class="fa fa-edit"></span>',$url, [
                                        'value' => "Update",
                                        'title' => Yii::t('yii', 'Update'),
                                        'class' => 'btn btn-warning btn-xs gridbtncustom updatedata',
                                        'data-toggle'=> 'tooltip',
                                        'style'=>'margin-right:4px;']);
                        },]],

            [
                'attribute' => 'first_name',
                'value'=>function($models, $keys){
                 if(isset($models->user)){
                    return $models->user->first_name;
                 }},
             ],
             [
                'attribute' => 'email_id',
                'value'=>function($models, $keys){
                 if(isset($models->user)){
                    return $models->user->email_id;
                 }},
             ],
             [
                'attribute' => 'gender',
                'value'=>function($models, $keys){
                 if(isset($models->user)){
                    return $models->user->gender;
                 }},
             ],
             [
                'attribute' => 'age',
                'value'=>function($models, $keys){
                 if(isset($models->user)){
                    return $models->user->age;
                 }},
             ],
             [
                'attribute' => 'mobile_number',
                'value'=>function($models, $keys){
                 if(isset($models->user)){
                    return $models->user->mobile_number;
                 }},
             ],
        ],
    ]); ?>

</div>
</div>
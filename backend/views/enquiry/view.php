<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Enquiry */

$this->title = "User Information";
$this->params['breadcrumbs'][] = ['label' => 'Enquiries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="enquiry-view">
<header class="quickview-header">
        <h4><p class="quickview-title"><?php echo $this->title; ?> </p></h4>
        <span class="close"><i class="ti-close"></i></span>
      </header>
 <div class="quickview-body center-vh ps-container ps-theme-default">
 <div class="quickview-block">
   <div class="table-responsive">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'enq_id',
            [
                'attribute' => 'first_name',
                'label'=>'Client Name',
                'value'=>function($models, $keys){
                 if(isset($models->user)){
                    return $models->user->first_name." ".$models->user->last_name;
                 }},
             ],

            [
                'attribute' => 'gender',
                'label'=>'Gender & Age',
                'value'=>function($models, $keys){
                 if(isset($models->user)){
                    return $models->user->gender." & ".$models->user->age." Yrs";
                 }},
             ],
             [
                'attribute' => 'mobile_number',
                'value'=>function($models, $keys){
                 if(isset($models->user)){
                    return $models->user->mobile_number;
                 }},
             ],
             [
                'attribute' => 'email_id',
                'value'=>function($models, $keys){
                 if(isset($models->user)){
                    return $models->user->email_id;
                 }},
             ],
            //'image_date',
            //'enq_date',
            //'is_recommentation',
            //'created_at',
            //'updated_at',
           // 'user_id',
        ],
    ]) ?>
</div>
</div>
</div>
</div>

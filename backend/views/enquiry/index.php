<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\EnquirySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Toothpix Uploads';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-index">
<div class="card-body">
<div class="table-responsive">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn',

                'header'=> 'Action',
                'template'=>'{update}{view}',
                'headerOptions' => ['style' => 'width:150px;color:#337ab7;'],
                'buttons'=>[  

                 'view' => function ($url, $model, $key) {
                       $url=Url::base(true)."/enquiry-view/".$model->enq_id.".html";
                                        
                       return Html::button('<i class="fa fa-eye"></i>', ['style'=>'margin-right:4px;','class' => 'btn btn-primary btn-xs', 'data-toggle'=>'quickview', 'title' =>'Update','data-target'=>"#company-update",'data-url'=> $url ]);
                       //Html::button('<i class="glyphicon glyphicon-eye-open"></i>', ['value' => $url, 'style'=>'margin-right:4px;','class' => 'btn btn-primary btn-xs view view gridbtncustom modalView','title' =>'View' ]);
                    },

                 'update' => function ($url, $model, $key) {
                            $url = Url::base(true)."/toothpixdata/".$model->enq_id.".html";
                            return Html::a('<span class="fa fa-edit"></span>',$url, [
                                        'value' => "Update",
                                        'title' => Yii::t('yii', 'Update'),
                                        'class' => 'btn btn-warning btn-xs gridbtncustom updatedata',
                                        'data-toggle'=> 'tooltip',
                                        'style'=>'margin-right:4px;']);
                        },]],
            [
                'attribute' => 'first_name',
                'label'=>'Client Name',
                'value'=>function($models, $keys){
                 if(isset($models->user)){
                    return $models->user->first_name." ".$models->user->last_name;
                 }},
             ],
             
             [
                'attribute' => 'gender',
                'label'=>'Gender & Age',
                'value'=>function($models, $keys){
                 if(isset($models->user)){
                    return $models->user->gender." & ".$models->user->age." Yrs";
                 }},
             ],
             [
                'attribute' => 'mobile_number',
                'value'=>function($models, $keys){
                 if(isset($models->user)){
                    return $models->user->mobile_number;
                 }},
             ],
             [
                'attribute' => 'email_id',
                'value'=>function($models, $keys){
                 if(isset($models->user)){
                    return $models->user->email_id;
                 }},
             ],
             [
                'attribute' => 'image_date',
                'value'=>function($models, $keys){
                 if(isset($models->image_date)){
                    return date('d-m-Y H:i:s',strtotime($models->image_date));
                 }},
             ],
            
            'is_recommentation',
            [
                'attribute' => 'enq_date',
                'value'=>function($models, $keys){
                 if(isset($models->enq_date)){
                    return date('d-m-Y H:i:s',strtotime($models->enq_date));
                 }},
             ],
            //'created_at',
            // 'updated_at',
            // 'user_id',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
</div>
</div>

<div id="company-update"  class="quickview quickview-xxl backdrop-dark" data-fullscreen-on-mobile></div>

<!-- <script type="text/javascript">
    $(document).ready(function(){
             $('body').on("click",".modalView",function(){
             var PageUrl = $(this).attr('value');
             $('#operationalheader').html('<span> <i class="fa fa-fw fa-th-large"></i>User Information</span>');
             $('#operationalmodal').modal('show').find('#modalContenttwo').load(PageUrl);
         });
    });
</script> -->

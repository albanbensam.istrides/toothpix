<?php

namespace backend\components;
use Yii;
use yii\base\Behavior;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Url;
use backend\models\SubscriptionMaster;
use backend\models\User;
use backend\models\SubscriptionTable;
use backend\models\CommissionDataStructure;
use backend\models\CommissionStructure;
use backend\models\ConfigurationTable;
use backend\models\LevelCompletionStatus;
use backend\models\PairTable;
use backend\models\BinaryUser;
use backend\models\CommisionReport;
use backend\models\PaymentLog;
use backend\models\TriggerLog;
use backend\models\Enquiry;

class QueryBehaviour extends Behavior {
   
   public $status_data = '0';
   public $user_type_data_parent = 'P';
   public $user_type_data_child = 'C';
   
   public function getSubscriptiondata() 
   {
         return ArrayHelper::map(SubscriptionMaster::find()->select('subscription_id,package_name')->where(['status'=>'1'])->asArray()->all(),'subscription_id','package_name');   
   }

   public function getSubscriptiondataAmount() 
   {
         return SubscriptionMaster::find()->select('subscription_id,package_name,package_amount')->where(['status'=>'1'])->asArray()->all();   
   }

   public function getSubscriptiondataid($subscription_id) 
   {
         return SubscriptionMaster::find()->select('subscription_id,package_name')->andWhere(['subscription_id'=>$subscription_id])->asArray()->one();   
   }

   public function getActiveReferalCode() 
   {
         return ArrayHelper::map(SubscriptionTable::find()->select('subscripe_id,referal_key')->where(['current_status'=>'1'])->asArray()->all(),'referal_key','referal_key');   
   }

   public function getReferalCodeExist($referal_key) 
   {
         return SubscriptionTable::find()->where(['referal_code'=>$referal_key])->andWhere(['current_status'=>'1'])->exists();   
   }

   public function getReferalCodeData($referal_key) 
   {
         return SubscriptionTable::find()->where(['referal_code'=>$referal_key])->andWhere(['current_status'=>'1'])->asArray()->one();   
   }

   public function getSubscriptionDataCheck($user_id) 
   {
         return SubscriptionTable::find()->where(['user_id'=>$user_id])->andWhere(['current_status'=>'1'])->asArray()->one();   
   }

   public function getSubscriptiondatapayement($subscribe_id) 
   {
         return SubscriptionTable::find()->where(['subscripe_id'=>$subscribe_id])->asArray()->one();   
   }

   public function getSubscriptiondatapayementonly($subscribe_id) 
   {
         return SubscriptionTable::find()->where(['subscripe_id'=>$subscribe_id])->andWhere(['current_status'=>'1'])->asArray()->one();   
   }

   public function getSubscriptiondatapayementcancel($subscribe_id) 
   {
         return SubscriptionTable::find()->where(['subscripe_id'=>$subscribe_id])->andWhere(['current_status'=>'1'])->andWhere(['payment_status'=>'PAYMENT_PENDING'])->one();    
   }

   public function getSubscriptiondataupdatependingcount($referal_code) 
   {
         return SubscriptionTable::find()->where(['referal_code'=>$referal_code])->asArray()->one();    
   }

   public function checkpaymentstatus($subscripe_id,$payment_status) 
   {
         return SubscriptionTable::find()->where(['subscripe_id'=>$subscripe_id])->asArray()->one();   
   }

   public function checkpaymentstatusonly($subscripe_id,$payment_status) 
   {
         return SubscriptionTable::find()->where(['subscripe_id'=>$subscripe_id])->andWhere(['current_status'=>'1'])->andWhere(['payment_status'=>$payment_status])->asArray()->one();   
   }

   public function validatePaymentStatus($referal_key) 
   {
        return SubscriptionTable::find()->select('payment_status')->where(['referal_code'=>$referal_key])->andWhere(['IN','payment_status',[Yii::$app->params['payment_status'][0],Yii::$app->params['payment_status'][1]]])->column();   
   }

   public function getRandomString($length = 5)
   {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
         $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
   }

   public function setNavigation($parameter)
   {
      $url =  Url::base(true);
      $navigation="<header><div class='header-action'><ul class='nav nav-pills nav-justified'>";
      foreach (Yii::$app->params['method_name'] as $key => $value) 
      {
         $active = ($key == $parameter) ? 'active' : '';
         $navigation.= "<li class='nav-item'><a class='nav-link ".$active."' href=".$url."/".$value[1].".html".">".$value[0]."</a></li>";
      }
      $navigation.="</ul></div><br></header>";
      return $navigation;
   }

   public function setUserConfig($data)
   {
      $get_referal_name = $data->refered_by_user;
      $user_name = $data->getUserNameData($get_referal_name);

      $first_name=isset($data->user->first_name) ? $data->user->first_name : '';
      $mobile_number=isset($data->user->mobile_number) ? $data->user->mobile_number : '';
      $email_id=isset($data->user->email_id) ? $data->user->email_id : '';
      $data_first_name=isset($user_name->first_name) ? $user_name->first_name : '';

      $table = "<table class='table table-bordered'>
               <thead class='table-dark'>
                   <tr>
                     <th scope='col'>User Name</th>
                     <th scope='col'>Phone No</th>
                     <th scope='col'>Email</th>
                     <th scope='col'>Refferal Name</th>
                   </tr>
                 </thead>
                 <tbody>
                   <tr>
                     <th scope='row'>".$first_name."</th>
                     <td>".$mobile_number."</td>
                     <td>".$email_id."</td>
                     <td>".$data_first_name."</td>
                   </tr>
                 </tbody>
               </table>";
      return $table;
   }

   public function setCommisionData()
   {
        return ArrayHelper::map(CommissionStructure::find()->select('*')->asArray()->all(),'level','commision'); 
   }

   public function getCommissionStructureData()
   {
        return CommissionStructure::find()->select('id,level,commision,status,no_of_pairs')->asArray()->indexBy('id')->all(); 
   }

   public function setCommisionStructureData() : int
   {
        $get_strcture_id=CommissionDataStructure::find()->select('*')->where(['status'=>'1'])->asArray()->one();
        return $get_strcture_id['id']; 
   }

   public function getUserReferalName($referal_key)
   {
        return User::find()->where(['refferal_id'=>$referal_key])->asArray()->one();
   } 

   public function getUserReferalNameSubscripttionwise($referal_key)
   {
        return SubscriptionTable::find()->where(['referal_code'=>$referal_key])->asArray()->one();
   } 

   public function getReferelGeneratedKey()
   {
        $query = new Query;
        $query  ->select(['subscription_table.referal_code as referal_code', 'CONCAT(subscription_table.referal_code," - ",user.first_name) as code'])->from('subscription_table')->join('inner join','user' ,'user.id = subscription_table.user_id')->where(['subscription_table.current_status'=>'1']); 
         $command = $query->createCommand();
         $data = $command->queryAll();
         return ArrayHelper::map($data,'referal_code','code');
   }

   public function getReferelGeneratedKeyNotUser($user_id)
   {
        $query = new Query;
        $query  ->select(['subscription_table.referal_code as referal_code', 'CONCAT(subscription_table.referal_code," - ",user.first_name) as code'])->from('subscription_table')->join('inner join','user' ,'user.id = subscription_table.user_id')->where(['subscription_table.current_status'=>'1'])->andWhere(['<>','user.id',$user_id]); 
         $command = $query->createCommand();
         $data = $command->queryAll();
         return ArrayHelper::map($data,'referal_code','code');
   } 

   public function getConfigData()
   {
        return ArrayHelper::map(ConfigurationTable::find()->where(['status'=>'1'])->asArray()->all(),'key','value'); 
   }

   public function ReferalCount($referal_key)
   {
      $subscription_tble = SubscriptionTable::find()->where(['refered_by_code'=>$referal_key])->andWhere(['current_status'=>'1'])->count();
      return $subscription_tble;
   }

   public function NewReferalCountPayment($referal_key)
   {
      $subscription_tble = SubscriptionTable::find()->where(['refered_by_code'=>$referal_key])->andWhere(['current_status'=>'1'])->andWhere(['payment_status'=>Yii::$app->params['payment_status'][2]])->count();
      return $subscription_tble;
   }

   public function NewPendingReferalCountPayment($referal_key)
   {
      $subscription_tble = SubscriptionTable::find()->where(['refered_by_code'=>$referal_key])->andWhere(['current_status'=>'1'])->andWhere(['payment_status'=>Yii::$app->params['payment_status'][0]])->count();
      return $subscription_tble;
   }

   public function PendingReferalCount($referal_key)
   {
      $subscription_tble = SubscriptionTable::find()->where(['refered_by_code'=>$referal_key])->andWhere(['current_status'=>'3'])->count();
      return $subscription_tble;
   }

   public function ParentUser()
   {
      $parent_user = User::find()->select('id,user_type_data')->where(['user_type_data'=>$this->user_type_data_parent])->asArray()->one();
      $data_array = array();
      if(!empty($parent_user))
      {
          $data_array = array('id'=>$parent_user['id'],'user_type_data' => $this->user_type_data_child);
      }
      else
      {
          $data_array = array('id'=>'','user_type_data' =>$this->user_type_data_parent);
      }
      return $data_array;
   }


   public function ParentSubscriptionData()
   {
      $get_parent_user = $this->ParentUser();
      if(isset($get_parent_user['id']))
      {
        $subscription_user = SubscriptionTable::find()->select('subscripe_id')->where(['refered_by_code'=>''])->andWhere(['user_id'=>$get_parent_user['id']])->asArray()->one();
        $data_array = array();
        if(!empty($subscription_user))
        {
            $data_array = array('subscribe_id'=>$subscription_user['subscripe_id']);
        }
        return $data_array;
      }
      return false;
   }

   public function getCommissionData()
   {
      $get_parent_user = $this->ParentUser();
      $get_subscribe_id_data = $this->ParentSubscriptionData();
      
      if(isset($get_parent_user['id']) && isset($get_subscribe_id_data['subscribe_id']))
      {
          return PairTable::find()->where(['status'=>'1'])->andWhere(['parent_user_id'=>$get_parent_user['id']])->andWhere(['subscribe_id'=>$get_subscribe_id_data['subscribe_id']])->orderBy(['id'=>SORT_DESC])->one();
      }
      return false;
   }

   public function getCommissionDataLast()
   {
      $get_parent_user = $this->ParentUser();
      $get_subscribe_id_data = $this->ParentSubscriptionData();
      
      if(isset($get_parent_user['id']) && isset($get_subscribe_id_data['subscribe_id']))
      {
          return PairTable::find()->where(['status'=>'0'])->andWhere(['parent_user_id'=>$get_parent_user['id']])->andWhere(['subscribe_id'=>$get_subscribe_id_data['subscribe_id']])->orderBy(['id'=>SORT_DESC])->one();
      }
      return false;
   }

   public function getPairTableLevel($data)
   {
       $pair_data = PairTable::find()->where(['level'=>$data['no_of_levels']])->andWhere(['parent_user_id'=>$data['id']])->andWhere(['subscribe_id'=>$data['subscription_id']])->asArray()->all();
       
       $increment_no = 0;
       $level_data_hardcode = array_flip(Yii::$app->params['pair-data']);
       $data_array = array();
       if($pair_data)
       {
          foreach ($pair_data as $key => $value) 
          {
              $increment_no = (!empty($value['pair_1'])) ? $increment_no+1 : $increment_no;
              $increment_no = (!empty($value['pair_2'])) ? $increment_no+1 : $increment_no;
          }
       }
       $data_array = array('no_of_pairs_completed'=>$increment_no);
       return $data_array;
   }

   public function getLevelCompletionStatus($data)
   {
        $data_query = LevelCompletionStatus::find()->where(['user_id'=>$data['id']])->andWhere(['subscription_id'=>$data['subscription_id']])->all(); 
        return $data_query;    
   }

   public function getLevelCompletionStatusCheck($data)
   {
        $data_query = LevelCompletionStatus::find()->where(['user_id'=>$data['id']])->andWhere(['subscription_id'=>$data['subscription_id']])->andWhere(['is_fully_completed'=>'NO'])->orderBy(['id'=>SORT_ASC])->one(); 
        return $data_query;    
   }

   public function setSessionToken()
   {
      $session = Yii::$app->session;
      $formTokenName = uniqid();
      $session[Yii::$app->params['inputname']]=$formTokenName;
      return $formTokenName; 
   }

   /* Verify Authenticate AuthKey Query*/
   public function getAuthorization($authorization)
   {
        $user_data_role = User::find()->where(['auth_key'=>$authorization])->asArray()->one();
        return $user_data_role;
   }

   /* Verify Authenticate Username->Email Query*/
   public function getUserEmail($email_id)
   {
        $user_email_role = User::find()->where(['email_id'=>$email_id])->asArray()->one();
        return $user_email_role;
   }

   public function getTableColumn($table_name)
   {
      $table = Yii::$app->db->getTableSchema($table_name);
      $get_column_name = array_combine(array_keys($table->columns),array_keys($table->columns));
      return $get_column_name;
   }

    # Author : Alban
    # Timestamp : 11-08-2020 13:16 PM
    # Desc : Fetch Subscription History
    public function getSubscriptionHistory($user_id)
    {
        $data_string = implode(',', Yii::$app->params['subscriptionhistory_data']);
        return SubscriptionTable::find()->select("$data_string")->where(['user_id'=>$user_id])->asArray()->all(); 
    }

    # Author : Alban
    # Timestamp : 23-08-2020 00:14 AM
    # Desc : Data User
    public function getUserData($user_id)
    {
        return User::find()->select("first_name,id")->where(['IN','id',$user_id])->indexBy('id')->asArray()->all(); 
    }


    # Author : Alban
    # Timestamp : 07-08-2020 15:24 PM
    # Desc : Checking Authorization at each Time
    public function authorization(){  
        $params = (Yii::$app->request->headers);
        $authorization=$params['authorization'];
        $authorization=str_replace('Bearer', '', $authorization);
        $authorization=trim($authorization);
        $user_data_role = $this->getAuthorization($authorization);
        $response  = array();
        if($user_data_role){

            /*Validated Data*/ 
            $validated_data = $this->validatePaymentStatus($user_data_role['refferal_id']);

            if(!empty($validated_data))
            {
                foreach (Yii::$app->params['payment_status'] as $key => $value) {
                    if(in_array($value, $validated_data))
                    {
                        $response['message'] = Yii::$app->params['response'][$value];
                        break;
                    }
                }
            }
            else
            {
                $response['data'] = $user_data_role;  
            }
        }
        else
        { 
            $response['message'] = Yii::$app->params['response']['message_11']; 
        }

        return $response;
    }

    # Author : Alban
    # Timestamp : 07-08-2020 15:24 PM
    # Desc : Checking Authorization at each Time
    public function authorizationonly(){  
        $params = (Yii::$app->request->headers);
        $authorization=$params['authorization'];
        $authorization=str_replace('Bearer', '', $authorization);
        $authorization=trim($authorization);
        $user_data_role = $this->getAuthorization($authorization);
        $response  = array();
        if($user_data_role)
        {
            $response['data'] = $user_data_role;  
        }
        else
        { 
            $response['message'] = Yii::$app->params['response']['message_11']; 
        }

        return $response;
    }

    # Author : Alban
    # Timestamp : 11-08-2020 12:00 PM
    # Desc : Checking Authentication at each Time
    public function authentication() :bool
    {  
        $params = (Yii::$app->request->headers);
        $authorization=$params['authentication'];
        if(Yii::$app->params['authentication'] == $authorization){
            return true;
        }else{ 
            return false;
        }
    }

    # Author : Alban
    # Timestamp : 18-08-2020 16:69 PM
    # Desc : Checking Binary User 
    public function binary_user_count()
    {
        return BinaryUser::find()->count();
    }

    # Author : Alban
    # Timestamp : 24-08-2020 21:12 PM
    # Desc : Add Subscripe Number
    public function add_subscripe_random_no()
    {
        $get_unique_no = \Yii::$app->db->createCommand('select random_id from random_generation WHERE id=1')->queryOne();
        return $get_unique_no['random_id'];
    }

    # Author : Alban
    # Timestamp : 27-08-2020 14:55 PM
    # Desc : Binary User Level Count 
    public function binary_users_level_count($binary_parent_map)
    {
        $data = BinaryUser::find()->select(['parent_id_count'=>'COUNT(parent_id)','parent_id'=>'parent_id','level'=>'level','parent_subscription_id'=>'parent_subscription_id'])->where(['status'=>'1'])->andWhere(['and',['IS NOT','parent_subscription_id',NULL],['<>','parent_subscription_id',''],['IN','parent_subscription_id',$binary_parent_map]])->groupBy(['parent_id','level'])->asArray()->all();
        return ArrayHelper::index($data,'level','parent_subscription_id');
    }

    # Author : Alban
    # Timestamp : 27-08-2020 17:22 PM
    # Desc : Level Completion Status 
    public function level_updated_data($subscribe_id)
    {
        return LevelCompletionStatus::find()->where(['is_fully_completed'=>'NO'])->andWhere(['IN','subscription_id',$subscribe_id])->indexBy(['id'])->asArray()->all();
    }

    # Author : Alban
    # Timestamp : 28-08-2020 00:37 PM
    # Desc : Level Completion Status Subscripe Wise 
    public function level_fetch_data($subscription_id)
    {
        $data = array();
        //Fetch Level Completion Data
        $level_completion_data = LevelCompletionStatus::find()->where(['subscription_id'=>$subscription_id])->indexBy(['id'])->asArray()->all();
        //Get Commision Data
        $commision_data = $this->getCommissionStructureData();
        //Set Config 
        $params_set = Yii::$app->params['level_pairs_data'];
        
        if(!empty($level_completion_data))
        {
          foreach ($level_completion_data as $key => $value) 
          {
              $total_no_of_pairs = $value[$params_set[0]];
              $no_of_pairs = $value[$params_set[1]];
              $pending_pairs = $total_no_of_pairs - $no_of_pairs;
              $data[] = array($params_set[5]=>$commision_data[$value[$params_set[5]]]['level'],$params_set[0]=>$total_no_of_pairs,$params_set[1]=>$no_of_pairs,$params_set[2]=>$pending_pairs,$params_set[3]=>$value[$params_set[3]],$params_set[4]=>$value[$params_set[4]]);
          }
        }
        return $data;
    }

    # Author : Alban
    # Timestamp : 29-08-2020 15:18 PM
    # Desc : Level Completion Status Subscripe Wise 
    public function commision_summary_fetch_data($subscription_id)
    {
        $data = array();
        //Fetch Level Completion Data
        $level_completion_data = LevelCompletionStatus::find()->where(['subscription_id'=>$subscription_id])->indexBy(['id'])->asArray()->all();
        //Get Commision Data
        $commision_data = $this->getCommissionStructureData();
        //Set Config 
        $params_set = Yii::$app->params['level_pairs_data'];
        
        if(!empty($level_completion_data))
        {
          foreach ($level_completion_data as $key => $value) 
          {
              $total_no_of_pairs = $value[$params_set[0]];
              $no_of_pairs = $value[$params_set[1]];
              $pending_pairs = $total_no_of_pairs - $no_of_pairs;
              $data[] = array($params_set[5]=>$commision_data[$value[$params_set[5]]]['level'],$params_set[0]=>$total_no_of_pairs,$params_set[1]=>$no_of_pairs,$params_set[2]=>$pending_pairs,$params_set[6]=>$value[$params_set[6]],$params_set[7]=>$value['no_of_levels']);
          }
        }
        return $data;
    }

    # Author : Alban
    # Timestamp : 29-08-2020 16:00 PM
    # Desc : Level Completion Details Status Subscripe Wise 
    public function commision_details_fetch_data($subscription_id,$level_id)
    {
        $data = array();
        //Fetch Level Completion Data
        $commision_report_data = CommisionReport::find()->where(['subscription_tbl_id'=>$subscription_id])->andWhere(['level_id'=>$level_id])->indexBy(['auto_id'])->asArray()->all();
        //Get Commision Data
        $commision_structure_data = $this->getCommissionStructureData();
        //Set Config 
        $params_set = Yii::$app->params['commision_details'];
        
        if(!empty($commision_report_data))
        {
          foreach ($commision_report_data as $key => $value) 
          {
              
              $data[] = array($params_set['level_id']=>$commision_structure_data[$value['level_id']]['level'],$params_set['pair']=>$value['pair'],$params_set['commision_value']=>$value['commision_value'],'pair_completed_date'=>date('d-m-Y H:i:s',strtotime($value['created_at'])));
          }
        }
        
        return $data;
    }

    # Author : Alban
    # Timestamp : 30-08-2020 21:43 AM
    # Desc : Binary Flow Details 
    public function binary_details_fetch_data($subscription_id)
    {
        $data = array();
        $position= "1";
        //Get Full Data
        $get_parent_data = BinaryUser::find()->where(['parent_subscription_id'=>$subscription_id])->asArray()->all();
        
        $get_user_name = User::find()->select('id,first_name')->asArray()->indexBy('id')->all();
        
        /*if(empty($get_parent_data))
        {
          $get_parent_data_one = BinaryUser::find()->where(['subscription_id'=>$subscription_id])->asArray()->one();

          $data [] = array('name'=>$get_user_name[$get_parent_data_one['user_id']]['first_name'],'level'=>'L0','side'=>"","position"=>$position);
        }*/
       
        $get_parent_index = ArrayHelper::index($get_parent_data,'subscription_id');

        
        
        $get_side = BinaryUser::find()->where(['IN','subscription_id',ArrayHelper::map($get_parent_data,'subscription_id','subscription_id')])->andWhere(['level'=>'L1'])->asArray()->all();

        $get_side_index = ArrayHelper::index($get_side,'position','parent_subscription_id');
        
        //Get Data For Parent
        $data_parent = SubscriptionTable::findOne($subscription_id);
        $parent_user_id = $data_parent->user_id;

        $data [] = array('name'=>$get_user_name[$parent_user_id]['first_name'],'level'=>'L0','side'=>"","position"=>$position);
        if(!empty($get_side_index))
        {
            
            foreach ($get_side_index as $key => $value) 
            {
                if(array_key_exists(2, $value))
                {
                    $parent_user_name=$get_user_name[$value[2]['user_id']]['first_name'];
                    $get_level_data = $get_parent_index[$value[2]['subscription_id']]['level'];
                    $get_side_data = $value[2]['side'];
                    $get_position = $get_parent_index[$value[2]['subscription_id']]['position'];

                    $data [] = array('name'=>$parent_user_name,'level'=>$get_level_data,'side'=>$get_side_data,"position"=>$get_position);
                }

                if(array_key_exists(3, $value))
                {
                    $parent_user_name=$get_user_name[$value[3]['user_id']]['first_name'];
                    $get_level_data = $get_parent_index[$value[3]['subscription_id']]['level'];
                    $get_side_data = $value[3]['side'];
                    $get_position = $get_parent_index[$value[3]['subscription_id']]['position'];

                    $data [] = array('name'=>$parent_user_name,'level'=>$get_level_data,'side'=>$get_side_data,"position"=>$get_position);
                }
                
            }
        }

        return $data;
    }

    public function dashboard_report()
    {
        $data_array = array();

        //Active User count
        $data_array['active_user'] = User::find()->where(['status_flag'=>null])->asArray()->count();

        $data_array['new_toothpix'] = Enquiry::find()->where(['is_recommentation'=>null])->asArray()->count();   

        $data_array['total_toothpix'] = Enquiry::find()->asArray()->count();

        $data_array['total_recomment'] = Enquiry::find()->where(['is_recommentation'=>'Yes'])->asArray()->count();            
        return $data_array;
    }    

   public function getSubscriptionDataLevelGrid()
   {
        $query = new Query;
        $query ->select(['subscripe_id','CONCAT(random_subscribe_no," - ",first_name) as code'])->from('subscription_table')->join('inner join','user' ,'user.id = subscription_table.user_id')->where(['subscription_table.payment_status'=>'PAYMENT_SUCCESS']); 
         $command = $query->createCommand();
         $data = $command->queryAll();
         return ArrayHelper::map($data,'subscripe_id','code');
   }

   public function getLeveldata()
   {
        $query = new Query;
        $query ->select(['subscripe_id','CONCAT(random_subscribe_no," - ",first_name) as code'])->from('subscription_table')->join('inner join','user' ,'user.id = subscription_table.user_id')->where(['subscription_table.payment_status'=>'PAYMENT_SUCCESS']); 
         $command = $query->createCommand();
         $data = $command->queryAll();
         return ArrayHelper::map($data,'subscripe_id','code');
   }

   public function BinaryChart($subscription_id)
   {
      $string = "";
      $root_parent = SubscriptionTable::findOne($subscription_id);
      if(!empty($root_parent))
      {
          
         
      $root_user = User::findOne($root_parent->user_id);
      $string.= "['".$root_user->first_name.$root_parent->random_subscribe_no."','',''],";

      $get_data = BinaryUser::find()->where(['IN','parent_subscription_id',$subscription_id])->orderBy(['level'=>SORT_ASC,'position'=>SORT_DESC])->asArray()->all();
      $get_data_map = ArrayHelper::map($get_data,'subscription_id','subscription_id');

      $get_data_index = ArrayHelper::index($get_data,'position','level');

      $get_current_data = BinaryUser::find()->where(['IN','subscription_id',$get_data_map])->andWhere(['IN','side',['R','L']])->orderBy(['position'=>SORT_DESC])->asArray()->indexBy(['auto_id'])->all();
      
      $get_parent_subscribe_id = ArrayHelper::map($get_current_data,'parent_subscription_id','parent_subscription_id');
        
      $get_subscribe_id_query =  SubscriptionTable::find()->where(['IN','subscripe_id',$get_data_map])->asArray()->indexBy(['subscripe_id'])->all();

      $get_user_map = ArrayHelper::map($get_subscribe_id_query,'user_id','user_id');

      $get_user_id_query = User::find()->where(['IN','id',$get_user_map])->asArray()->indexBy(['id'])->all();

      $get_parent_subscribe_id_query =  SubscriptionTable::find()->where(['IN','subscripe_id',$get_parent_subscribe_id])->asArray()->indexBy(['subscripe_id'])->all();
      
      $get_parent_user_map = ArrayHelper::map($get_parent_subscribe_id_query,'user_id','user_id');

      $get_parent_user_id_query = User::find()->where(['IN','id',$get_parent_user_map])->asArray()->indexBy(['id'])->all();
      

      $get__cdata_index = ArrayHelper::index($get_current_data,'position','parent_subscription_id');

      $position_array = array(3,2);
      foreach ($get__cdata_index as $key => $value) 
      {
          foreach ($position_array as $key_1 => $value_1) 
          {
              if(array_key_exists($value_1, $value))
              {
                  $get_subscribe_id = $value[$value_1]['subscription_id'];
                  $get_parent_subscribe_id = $value[$value_1]['parent_subscription_id'];

                  $random_subscribe_no = $get_subscribe_id_query[$get_subscribe_id]['random_subscribe_no'];

                  $get_user_id = $get_subscribe_id_query[$get_subscribe_id]['user_id'];

                  $first_name = $get_user_id_query[$get_user_id]['first_name'];

                  //Parent  
                  $random_parent_subscribe_no = $get_parent_subscribe_id_query[$get_parent_subscribe_id]['random_subscribe_no'];

                  $get_parent_user_id = $get_parent_subscribe_id_query[$get_parent_subscribe_id]['user_id'];

                  $parent_first_name = $get_parent_user_id_query[$get_parent_user_id]['first_name'];
                  


                  $string.= "['".$first_name.$random_subscribe_no."','".$parent_first_name.$random_parent_subscribe_no."',''],";
              }            
          } 
      }
      }

       return $string; 
    }

    public function convertToTree(array $flat, $idField = 'id',$parentIdField = 'parent_id',$childNodesField = 'children') {
    $indexed = array();
    // first pass - get the array indexed by the primary id  
    foreach ($flat as $row) {
        $indexed[$row[$idField]] = $row;
       // $indexed[$row[$idField]][$childNodesField] = array();
    }
    //second pass  
    $root = null;
    foreach ($indexed as $id => $row) {
        $indexed[$row[$parentIdField]][$childNodesField][] =& $indexed[$id];
        if (!$row[$parentIdField]) {
            $root = $id;
        }
    }

    return array( $indexed[$root]);
}

   public function BinaryChartTree($subscription_id)
   {
      $root_parent = SubscriptionTable::findOne($subscription_id);
      $strings = array();
      if(!empty($root_parent))
      {
         
      $root_user = User::findOne($root_parent->user_id);
      
      $get_data = BinaryUser::find()->where(['IN','parent_subscription_id',$subscription_id])->orderBy(['level'=>SORT_ASC,'position'=>SORT_DESC])->asArray()->all();
      $get_data_map = ArrayHelper::map($get_data,'subscription_id','subscription_id');

      $get_data_index = ArrayHelper::index($get_data,'position','level');

      $get_current_data = BinaryUser::find()->where(['IN','subscription_id',$get_data_map])->andWhere(['IN','side',['R','L']])->orderBy(['parent_subscription_id'=>SORT_ASC,'position'=>SORT_DESC])->asArray()->indexBy(['auto_id'])->all();
      
        
      $get_subscribe_id_query =  SubscriptionTable::find()->where(['IN','subscripe_id',$get_data_map])->asArray()->indexBy(['subscripe_id'])->all();

      $get_user_map = ArrayHelper::map($get_subscribe_id_query,'user_id','user_id');

      $get_user_id_query = User::find()->where(['IN','id',$get_user_map])->asArray()->indexBy(['id'])->all();

      
      $get__cdata_index = ArrayHelper::index($get_current_data,'position','parent_subscription_id');

      $first_children = array();
      
      $position_array = array(2,3);
      $inc = 1;
       
      $strings_in=array();
      foreach ($get__cdata_index as $key => $value) 
      {
          foreach ($value as $key_1 => $value_1) {
            $sub_id = $value[$key_1]['subscription_id'];
            $user_id = $get_subscribe_id_query[$sub_id]['user_id'];
            $first_name = $get_user_id_query[$user_id]['first_name'];
            $random_subscribe_no = $get_subscribe_id_query[$sub_id]['random_subscribe_no'];
            $parent_id = $value[$key_1]['parent_subscription_id'];
         

        $strings_in[$sub_id] = array(
          'head'=>$first_name,
          'id'=>$sub_id,
          'parent_id'=>$parent_id,
          'contents'=>$random_subscribe_no,
          //'children'=>$first_children
        );} 
      }
         $p_array=array('head'=>$root_user->first_name,'id'=>$root_parent->subscripe_id,'contents'=>$root_parent->random_subscribe_no,'parent_id'=>null);
         array_unshift($strings_in,$p_array);
          $strings= $this->convertToTree($strings_in,'id','parent_id');
          return json_encode($strings);  
      
      }

      
    }

    public function CommisionExcel($subscription_id)
    {

        $objPHPExcel = new \PHPExcel();
        $sheet = 0;
        
        $objPHPExcel -> setActiveSheetIndex($sheet);
        $objPHPExcel -> getActiveSheet() -> setTitle("Commision Report") 
        -> setCellValue('A1', 'Subscription ID & Name')
        -> setCellValue('B1', 'No Of Levels')
        -> setCellValue('C1', 'Total No Of Pairs Each Level')
        -> setCellValue('D1', 'No Of Pairs Completed')
        -> setCellValue('E1', 'Total No Of Members')
        -> setCellValue('F1', 'Total No Of Members Joined')
        -> setCellValue('G1', 'Accumulate Price');

        $level_completion_status = LevelCompletionStatus::find()->where(['IN','subscription_id',$subscription_id])->asArray()->all();
        $get_subscription_id_map = ArrayHelper::map($level_completion_status,'subscription_id','subscription_id');
        $subscription_table = SubscriptionTable::find()->select('subscripe_id,random_subscribe_no,user_id')->where(['IN','subscripe_id',$get_subscription_id_map])->asArray()->indexBy('subscripe_id')->all();
        $get_user_id_map = ArrayHelper::map($subscription_table,'user_id','user_id');
        $user_table = User::find()->select('id,first_name')->where(['IN','id',$get_user_id_map])->asArray()->indexBy('id')->all(); 
        $r_a=65;$r_a1=64;
        $row = 2;
        foreach ($level_completion_status as $key => $value) 
        {
            $cell_char=chr($r_a);
            //Subscription
            $name = '';
            $random_id = '';
            if(isset($subscription_table[$value['subscription_id']]))
            {
                $random_id = $subscription_table[$value['subscription_id']]['random_subscribe_no'];
                if(isset($subscription_table[$value['subscription_id']]['user_id']))
                {
                    $name = $user_table[$subscription_table[$value['subscription_id']]['user_id']]['first_name'];
                }
            }

            $objPHPExcel -> getActiveSheet() -> setCellValue($cell_char++ . $row, $name.'-'.$random_id);
            $objPHPExcel -> getActiveSheet() -> setCellValue($cell_char++ . $row, $value['no_of_levels']);
            $objPHPExcel -> getActiveSheet() -> setCellValue($cell_char++ . $row, $value['total_no_of_pairs_each_level']);
            $objPHPExcel -> getActiveSheet() -> setCellValue($cell_char++ . $row, $value['no_of_pairs_completed']);
            $objPHPExcel -> getActiveSheet() -> setCellValue($cell_char++ . $row, $value['total_no_of_members']);
            $objPHPExcel -> getActiveSheet() -> setCellValue($cell_char++ . $row, (empty($value['total_no_of_members_joined'])) ? 0 : $value['total_no_of_members_joined']);
            $objPHPExcel -> getActiveSheet() -> setCellValue($cell_char++ . $row, (empty($value['accumulate_price'])) ? 0 :$value['accumulate_price']);
            $row++;  
        }
        
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        header('Content-Type: application/vnd.ms-excel');
        $filename = "CommisionExcelReport".date("d-m-Y-H:i:s").".xls";
        header('Content-Disposition: attachment;filename='.$filename .' ');
        header('Cache-Control: max-age=0');   
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        $objWriter->save('php://output');
        die;
    }

    public function TransactionExcel($from,$to)
    {
        $from = date('Y-m-d',strtotime($from));
        $to = date('Y-m-d',strtotime($to));   
        
        $objPHPExcel = new \PHPExcel();
        $sheet = 0;
        
        $objPHPExcel -> setActiveSheetIndex($sheet);
        $objPHPExcel -> getActiveSheet() -> setTitle("Transaction Report") 
        -> setCellValue('A1', 'Subscription ID & Name')
        -> setCellValue('B1', 'Email ID')
        -> setCellValue('C1', 'Mobile Number')
        -> setCellValue('D1', 'Order ID')
        -> setCellValue('E1', 'Transaction ID')
        -> setCellValue('F1', 'Amount')
        -> setCellValue('G1', 'Status');

        $level_completion_status = PaymentLog::find()->where(['between','date(created_at)',$from,$to])->asArray()->all();
        $get_subscription_id_map = ArrayHelper::map($level_completion_status,'subscripe_id','subscripe_id');
        $subscription_table = SubscriptionTable::find()->select('subscripe_id,random_subscribe_no,user_id')->where(['IN','subscripe_id',$get_subscription_id_map])->asArray()->indexBy('subscripe_id')->all();
        $get_user_id_map = ArrayHelper::map($subscription_table,'user_id','user_id');
        $user_table = User::find()->select('id,first_name,email_id,mobile_number')->where(['IN','id',$get_user_id_map])->asArray()->indexBy('id')->all(); 
        $r_a=65;$r_a1=64;
        $row = 2;
        foreach ($level_completion_status as $key => $value) 
        {
            $cell_char=chr($r_a);
            //Subscription
            $name = '';
            $random_id = '';
            $email_id = '';
            $mobile_number = '';
            if(isset($subscription_table[$value['subscripe_id']]))
            {
                $random_id = $subscription_table[$value['subscripe_id']]['random_subscribe_no'];
                if(isset($subscription_table[$value['subscripe_id']]['user_id']))
                {
                    $name = $user_table[$subscription_table[$value['subscripe_id']]['user_id']]['first_name'];
                    $email_id = $user_table[$subscription_table[$value['subscripe_id']]['user_id']]['email_id'];
                    $mobile_number = $user_table[$subscription_table[$value['subscripe_id']]['user_id']]['mobile_number'];
                }
            }

            $objPHPExcel -> getActiveSheet() -> setCellValue($cell_char++ . $row, $name.'-'.$random_id);
            $objPHPExcel -> getActiveSheet() -> setCellValue($cell_char++ . $row, $email_id);
            $objPHPExcel -> getActiveSheet() -> setCellValue($cell_char++ . $row, $mobile_number);
            $objPHPExcel -> getActiveSheet() -> setCellValue($cell_char++ . $row, $value['order_id']);
            $objPHPExcel -> getActiveSheet() -> setCellValue($cell_char++ . $row, $value['transaction_id']);
            $objPHPExcel -> getActiveSheet() -> setCellValue($cell_char++ . $row, $value['paid_amount']);
            $objPHPExcel -> getActiveSheet() -> setCellValue($cell_char++ . $row, $value['payment_status']);
            $row++;  
        }
        
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        header('Content-Type: application/vnd.ms-excel');
        $filename = "TransactionExacelReport".date("d-m-Y-H:i:s").".xls";
        header('Content-Disposition: attachment;filename='.$filename .' ');
        header('Cache-Control: max-age=0');   
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        $objWriter->save('php://output');
        die;
    }

    public function CommisionPDF($subscription_id)
    {

        require ('../../vendor/tcpdf/tcpdf.php');
        $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Nicola Asuni');
        $pdf->SetTitle('Invoice');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);

        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $pdf->setFontSubsetting(true);
        $pdf->SetFont('helvetica', '', 8, '', true);
        $pdf->AddPage();


      if($subscription_id)
      {
          $level_completion_status = LevelCompletionStatus::find()->where(['IN','subscription_id',$subscription_id])->asArray()->all();

        $get_subscription_id_map = ArrayHelper::map($level_completion_status,'subscription_id','subscription_id');
        $subscription_table = SubscriptionTable::find()->select('subscripe_id,random_subscribe_no,user_id')->where(['IN','subscripe_id',$get_subscription_id_map])->asArray()->indexBy('subscripe_id')->all();
        $get_user_id_map = ArrayHelper::map($subscription_table,'user_id','user_id');
        $user_table = User::find()->select('id,first_name')->where(['IN','id',$get_user_id_map])->asArray()->indexBy('id')->all(); 

        $table = "";
        $table.= "<!DOCTYPE html>
<html>
<head>
<style>
table, th, td {
  border: 1px solid black;
  padding: 5px;
}
table {
  border-spacing: 15px;
}
</style>
</head>
<body>

<center><h2>Commision Report</h2></center>

<table style='width:100%'>
  <tr>
    <th>Subscription ID & Name</th>
    <th>No Of Levels</th> 
    <th>Total No Of Pairs Each Level</th>
    <th>No Of Pairs Completed</th>
    <th>Total No Of Members</th>
    <th>Total No Of Members Joined</th>
    <th>Accumulate Price</th>
  </tr>";

  foreach ($level_completion_status as $key => $value) 
  {
      $name = '';
      $random_id = '';
      if(isset($subscription_table[$value['subscription_id']]))
      {
          $random_id = $subscription_table[$value['subscription_id']]['random_subscribe_no'];
          if(isset($subscription_table[$value['subscription_id']]['user_id']))
          {
              $name = $user_table[$subscription_table[$value['subscription_id']]['user_id']]['first_name'];
          }
      }

      $total_no_of_members_joined=empty($value['total_no_of_members_joined']) ? 0 : $value['total_no_of_members_joined'];
      $accumulate_price = empty($value['accumulate_price']) ? 0 :$value['accumulate_price'];

      $table.="<tr>
          <td>".$name.'-'.$random_id."</td>
          <td>".$value['no_of_levels']."</td>
          <td>".$value['total_no_of_pairs_each_level']."</td>
          <td>".$value['no_of_pairs_completed']."</td>
          <td>".$value['total_no_of_members']."</td>
          <td>".$total_no_of_members_joined."</td>
          <td>".$accumulate_price."</td>
        </tr>"; 
  }
$table.="</table>
</body>
</html>
";
      $pdf->writeHTML($table, true, false, false, false, '');
      }
      else
      {
                $pdf->writeHTML("<p>No Data Found</p>", true, false, false, false, '');
      }
        $pdf->Output('CommisionPDF.pdf');
    }


    public function TransactionPDF($from,$to)
    {

        require ('../../vendor/tcpdf/tcpdf.php');
        $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Nicola Asuni');
        $pdf->SetTitle('Invoice');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);

        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $pdf->setFontSubsetting(true);
        $pdf->SetFont('helvetica', '', 8, '', true);
        $pdf->AddPage();


      if(!empty($from) && !empty($to))
      {
        $from = date('Y-m-d',strtotime($from));
        $to = date('Y-m-d',strtotime($to));   
        
        $level_completion_status = PaymentLog::find()->where(['between','date(created_at)',$from,$to])->asArray()->all();
        $get_subscription_id_map = ArrayHelper::map($level_completion_status,'subscripe_id','subscripe_id');
        $subscription_table = SubscriptionTable::find()->select('subscripe_id,random_subscribe_no,user_id')->where(['IN','subscripe_id',$get_subscription_id_map])->asArray()->indexBy('subscripe_id')->all();
        $get_user_id_map = ArrayHelper::map($subscription_table,'user_id','user_id');
        $user_table = User::find()->select('id,first_name,email_id,mobile_number')->where(['IN','id',$get_user_id_map])->asArray()->indexBy('id')->all();

        $table = "";
        $table.= "<!DOCTYPE html>
<html>
<head>
<style>
table, th, td {
  border: 1px solid black;
  padding: 5px;
}
table {
  border-spacing: 15px;
}
</style>
</head>
<body>

<center><h2>Transaction Report</h2></center>

<table style='width:100%'>
  <tr>
    <th>Subscription ID & Name</th>
    <th>Email ID</th> 
    <th>Mobile Number</th>
    <th>Order ID</th>
    <th>Transaction ID</th>
    <th>Amount</th>
    <th>Status</th>
  </tr>";
  foreach ($level_completion_status as $key => $value) 
  {
      $name = '';
      $random_id = '';
      $email_id = '';
      $mobile_number = '';
      if(isset($subscription_table[$value['subscripe_id']]))
      {
          $random_id = $subscription_table[$value['subscripe_id']]['random_subscribe_no'];
          if(isset($subscription_table[$value['subscripe_id']]['user_id']))
          {
              $name = $user_table[$subscription_table[$value['subscripe_id']]['user_id']]['first_name'];
              $email_id = $user_table[$subscription_table[$value['subscripe_id']]['user_id']]['email_id'];
              $mobile_number = $user_table[$subscription_table[$value['subscripe_id']]['user_id']]['mobile_number'];
          }
      }
      $table.="<tr>
          <td>".$name.'-'.$random_id."</td>
          <td>".$email_id."</td>
          <td>".$mobile_number."</td>
          <td>".$value['order_id']."</td>
          <td>".$value['transaction_id']."</td>
          <td>".$value['paid_amount']."</td>
          <td>".$value['payment_status']."</td>
        </tr>"; 
  }
$table.="</table>
</body>
</html>
";
      $pdf->writeHTML($table, true, false, false, false, '');
      }
      else
      {
                $pdf->writeHTML("<p>No Data Found</p>", true, false, false, false, '');
      }
        $pdf->Output('CommisionPDF.pdf');
    }


    public function EmailTemplate($data)
    {
       $status = 'F';
       //$to="",$subject="",$multiattach="",$customer_name=""
       $to=trim($data['to']);
       //$bcc='vivekv2v@gmail.com'; 
       $messagesend = Yii::$app->mailer->compose('layouts/html');
       //$messagesend =  Yii::$app->mailer->compose('layouts/html', ['contactForm' => $form])
       $messagesend->setFrom([Yii::$app->params['adminEmail'] => 'Abdhi Enterprises']);
       $messagesend->setTo("$to");
        /*if($bcc!=""){
               $messagesend->setBcc($bcc); 
        }*/
       $messagesend->setHtmlBody($data['message']);
       $messagesend->setSubject($data['subject']);
       //$messagesend->attach($data['multiattach']);
       if($messagesend->send()){
          
       }
       else
       {
          $status = 'S';
       } 

       $trigger_log = new TriggerLog();
       $trigger_log->trigger_type = 'Email';
       $trigger_log->user_id = $data['user_id'];
       //$trigger_log->mobile_no = $phone;
       $trigger_log->content = $data['message'];
       $trigger_log->status = $status;
       $trigger_log->created_at = date('Y-m-d H:i:s');
       $trigger_log->updated_at = date('Y-m-d H:i:s');
       $trigger_log->save();

    }


    public function MailTemplate($mail_data)
    {
          $message='<html>

<head>
    
    
    <style type="text/css">
       

        /* CLIENT-SPECIFIC STYLES */
        body,
        table,
        td{
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        table,
        td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }

        /* RESET STYLES */
        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
        }

        table {
            border-collapse: collapse !important;
        }

        body {
            height: 100% !important;
            margin: 0 !important;
            padding: 0 !important;
            width: 100% !important;
        }

        /* iOS BLUE LINKS */
    

        /* MOBILE STYLES */
        @media screen and (max-width:600px) {
            h1 {
                font-size: 32px !important;
                line-height: 32px !important;
            }
        }

        /* ANDROID CENTER FIX */
        div[style*="margin: 16px 0;"] {
            margin: 0 !important;
        }
    </style>
</head>

<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">
    <!-- HIDDEN PREHEADER TEXT -->
    <div style=display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family:Lato, Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;></div>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <!-- LOGO -->
        <tr>
            <td bgcolor="#2b71bc" align="center">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                    <tr>
                        <td align="center" valign="top" style="padding: 40px 10px 40px 10px;"> </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#2b71bc" align="center" style="padding: 0px 10px 0px 10px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                    <tr>
                        <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 4px; line-height: 48px;">
                             <img src="https://admin.abdhienterprises.com/backend/web/images/abdhi-logo.png" width="125" height="120" style="display: block; border: 0px;" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                            <p style="margin: 0;">Dear '.$mail_data['customer_name'].'</p>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                            <p style="margin: 0;">Thanks you for choosing us for Your Subscription Package Name is '.$mail_data['package_name'].' and Package Amount is '.$mail_data['package_amount'].'</p>
                        </td>
                    </tr>
                   
                    <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 0px 30px 0px 30px; color: #666666; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                            <p style="margin: 0;">Your Subscription Package has been approved and process is initiated.</p>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 0px 30px 0px 30px; color: #666666; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                            <p style="margin: 0;">And Your Referal ID '.$mail_data['referalid'].'.</p>
                        </td>
                    </tr> <!-- COPY -->          
                    <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 0px 30px 40px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                            <p style="margin: 0;">Cheers,<br>Abdhi Team<br><a href="https://abdhienterprises.com/"</a></p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>';

return $message;
    }


    public function MailClosedRegister($mail_data)
    {
          $message='<html>

<head>
    
    
    <style type="text/css">
       

        /* CLIENT-SPECIFIC STYLES */
        body,
        table,
        td{
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        table,
        td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }

        /* RESET STYLES */
        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
        }

        table {
            border-collapse: collapse !important;
        }

        body {
            height: 100% !important;
            margin: 0 !important;
            padding: 0 !important;
            width: 100% !important;
        }

        /* iOS BLUE LINKS */
    

        /* MOBILE STYLES */
        @media screen and (max-width:600px) {
            h1 {
                font-size: 32px !important;
                line-height: 32px !important;
            }
        }

        /* ANDROID CENTER FIX */
        div[style*="margin: 16px 0;"] {
            margin: 0 !important;
        }
    </style>
</head>

<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">
    <!-- HIDDEN PREHEADER TEXT -->
    <div style=display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family:Lato, Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;></div>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <!-- LOGO -->
        <tr>
            <td bgcolor="#2b71bc" align="center">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                    <tr>
                        <td align="center" valign="top" style="padding: 40px 10px 40px 10px;"> </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#2b71bc" align="center" style="padding: 0px 10px 0px 10px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                    <tr>
                        <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 4px; line-height: 48px;">
                             <img src="https://admin.abdhienterprises.com/backend/web/images/abdhi-logo.png" width="125" height="120" style="display: block; border: 0px;" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                            <p style="margin: 0;">Dear '.$mail_data['customer_name'].'</p>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                            <p style="margin: 0;">Congratulations Your Subscription Package Has Successfully Closed</p>
                        </td>
                    </tr>
                   
                    <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 0px 30px 0px 30px; color: #666666; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                            <p style="margin: 0;"></p>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 0px 30px 0px 30px; color: #666666; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                            <p style="margin: 0;"></p>
                        </td>
                    </tr> <!-- COPY -->          
                    <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 0px 30px 40px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                            <p style="margin: 0;">Cheers,<br>Abdhi Team<br><a href="https://abdhienterprises.com/"</a></p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>';

return $message;
    }


    public function RegisterationSMS($data)
    {
        $message = "Dear ".$data['customer_name'].", Your Referal ID ".$data['referalid']." and Package Name is ".$data['package_name']."";
        return $message;
    }

    public function ClosedRegisterationSMS($data)
    {
        $message = "Dear ".$data['customer_name'].", Congratulations Your Subscription Package Has Successfully Closed";
        return $message;
    }


    public function MessageIntegration($phone, $sms_message="",$data) {
            
        if($phone!="" && $sms_message!=""){
            $sms_message = str_replace("&", " and " , $sms_message);
            $sms_message = str_replace("'", " " , $sms_message);
            $sms_message = str_replace("`", " " , $sms_message);
            //$sms_message = str_replace("?", " " , $sms_message);
            $sms_message = urlencode($sms_message);
            $phone  = "91".substr($phone,(strlen($phone)-10),strlen($phone));
            $url="http://bulksms.mysmsmantra.com:8080/WebSMS/SMSAPI.jsp?username=MRNAGENDRA&password=563802743&sendername=ABDHIE&mobileno=".$phone."&message=".$sms_message;
            $curl = curl_init();
            curl_setopt_array($curl, array(
              CURLOPT_URL => $url,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
              CURLOPT_SSL_VERIFYHOST => 0,
              CURLOPT_SSL_VERIFYPEER => 0,
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            $status = 'F';
            if ($err) {
                $response=false;
            } else { 
              $status = 'S';
              $response=true;
            }

            $trigger_log = new TriggerLog();
            $trigger_log->trigger_type = 'SMS';
            $trigger_log->user_id = $data['user_id'];
            $trigger_log->mobile_no = $phone;
            $trigger_log->content = $sms_message;
            $trigger_log->status = $status;
            $trigger_log->created_at = date('Y-m-d H:i:s');
            $trigger_log->updated_at = date('Y-m-d H:i:s');
            $trigger_log->save();

          }
          return  $response;
    }
}
?>
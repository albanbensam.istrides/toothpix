<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\User;

/**
 * UserSearch represents the model behind the search form of `backend\models\User`.
 */
class UserSearch extends User
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at', 'subscripe_plan'], 'integer'],
            [['username', 'first_name', 'last_name', 'dob', 'user_type', 'city', 'auth_key', 'password_hash', 'password_reset_token', 'rights', 'status_flag', 'user_level', 'mobile_number', 'designation', 'user_role', 'address', 'aadhar_number', 'refferal_id', 'refferal_name', 'bank_account_no', 'ifsc', 'branch', 'email_id', 'create_at', 'update_at','bank_name','random_subscribe_no_data','gender','age','clients_name','gender_age'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find()->where(['user_role'=>NULL])->orderBy(['id'=>SORT_DESC]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'dob' => $this->dob,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'subscripe_plan' => $this->subscripe_plan,
            'create_at' => $this->create_at,
            'update_at' => $this->update_at,
        ]);

        $query
            
            ->andFilterWhere(['like', 'bank_name', $this->bank_name])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'user_type', $this->user_type])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'rights', $this->rights])
            ->andFilterWhere(['like', 'status_flag', $this->status_flag])
            ->andFilterWhere(['like', 'user_level', $this->user_level])
            ->andFilterWhere(['like', 'mobile_number', $this->mobile_number])
            ->andFilterWhere(['like', 'designation', $this->designation])
            ->andFilterWhere(['like', 'user_role', $this->user_role])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'aadhar_number', $this->aadhar_number])
            ->andFilterWhere(['like', 'refferal_id', $this->refferal_id])
            ->andFilterWhere(['like', 'refferal_name', $this->refferal_name])
            ->andFilterWhere(['like', 'bank_account_no', $this->bank_account_no])
            ->andFilterWhere(['like', 'ifsc', $this->ifsc])
            ->andFilterWhere(['like', 'branch', $this->branch])
            ->andFilterWhere(['like', 'email_id', $this->email_id]);

        return $dataProvider;
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchtoothpix($params)
    {
        $query = User::find()->where(['user_role'=>NULL])->orderBy(['id'=>SORT_DESC]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'dob' => $this->dob,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'subscripe_plan' => $this->subscripe_plan,
            'create_at' => $this->create_at,
            'update_at' => $this->update_at,
        ]);

        $query
                   
            //->andFilterWhere(['OR',['like','concat(first_name," ",last_name) as clients_name', $this->clients_name]])
            //->andFilterWhere(['or',['like', 'first_name', $this->clients_name],['like', 'last_name', $this->clients_name]])
            ->andFilterWhere(['like', 'bank_name', $this->bank_name])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'user_type', $this->user_type])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'rights', $this->rights])
            ->andFilterWhere(['like', 'status_flag', $this->status_flag])
            ->andFilterWhere(['like', 'user_level', $this->user_level])
            ->andFilterWhere(['like', 'mobile_number', $this->mobile_number])
            ->andFilterWhere(['like', 'designation', $this->designation])
            ->andFilterWhere(['like', 'user_role', $this->user_role])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'aadhar_number', $this->aadhar_number])
            ->andFilterWhere(['like', 'refferal_id', $this->refferal_id])
            ->andFilterWhere(['like', 'refferal_name', $this->refferal_name])
            ->andFilterWhere(['like', 'bank_account_no', $this->bank_account_no])
            ->andFilterWhere(['like', 'ifsc', $this->ifsc])
            ->andFilterWhere(['like', 'branch', $this->branch])
            ->andFilterWhere(['like', 'email_id', $this->email_id]);

        return $dataProvider;
    }
     
}

<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Enquiry;

/**
 * EnquirySearch represents the model behind the search form about `backend\models\Enquiry`.
 */
class EnquirySearch extends Enquiry
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['enq_id', 'user_id'], 'integer'],
            [['image_date', 'enq_date', 'is_recommentation', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Enquiry::find()->orderBy(['enq_id'=>SORT_DESC]);
        $query->joinWith('user');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'enq_id' => $this->enq_id,
            'image_date' => $this->image_date,
            'enq_date' => $this->enq_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'user_id' => $this->user_id,
        ]);

        $query
        ->andFilterWhere(['like', 'user.email_id', $this->email_id])
            ->andFilterWhere(['like', 'user.first_name', $this->first_name])
            ->andFilterWhere(['like', 'user.last_name', $this->last_name])
            ->andFilterWhere(['like', 'user.gender', $this->gender])
            ->andFilterWhere(['like', 'user.age', $this->age])
            ->andFilterWhere(['like', 'user.mobile_number', $this->mobile_number])
        ->andFilterWhere(['like', 'is_recommentation', $this->is_recommentation]);

        return $dataProvider;
    }
}

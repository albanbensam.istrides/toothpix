<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "enquiry".
 *
 * @property integer $enq_id
 * @property string $image_date
 * @property string $enq_date
 * @property string $is_recommentation
 * @property string $created_at
 * @property string $updated_at
 * @property integer $user_id
 */
class Enquiry extends \yii\db\ActiveRecord
{
    public $email_id,$first_name,$last_name,$gender,$age,$mobile_number;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'enquiry';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image_date', 'enq_date', 'created_at', 'updated_at'], 'safe'],
            [['user_id'], 'integer'],
            [['is_recommentation'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'enq_id' => 'Enq ID',
            'image_date' => 'Uploaded Date',
            'enq_date' => 'Recommendation Date',
            'is_recommentation' => 'Recommendation Given?',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'user_id' => 'User ID',
            'gender_age' => 'Gender & Age',
            'client_name' => 'Client Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}

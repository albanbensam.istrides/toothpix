<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $first_name
 * @property string $last_name
 * @property string $dob
 * @property string $user_type
 * @property string $city
 * @property string $auth_key
 * @property string $profile_image
 * @property string $profile_pdf
 * @property string $password_hash
 * @property string $password_reset_token
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $rights
 * @property string $status_flag
 * @property string $user_level
 * @property string $mobile_number
 * @property string $designation
 * @property string $user_role
 * @property string $address
 * @property string $aadhar_number
 * @property integer $subscripe_plan
 * @property string $refferal_id
 * @property string $refferal_name
 * @property string $bank_name
 * @property string $bank_account_no
 * @property string $ifsc
 * @property string $branch
 * @property string $email_id
 * @property string $user_type_data
 * @property string $create_at
 * @property string $update_at
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';
    public $clients_name,$gender_age;
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dob', 'create_at', 'update_at'], 'safe'],
            [['user_type', 'rights', 'status_flag', 'address', 'user_type_data'], 'string'],
            [['status', 'created_at', 'updated_at', 'subscripe_plan'], 'integer'],
            [['username', 'profile_image', 'profile_pdf', 'password_hash', 'password_reset_token', 'user_role', 'refferal_name', 'bank_name', 'bank_account_no', 'branch', 'email_id'], 'string', 'max' => 255],
            [['first_name', 'last_name', 'city'], 'string', 'max' => 70],
            [['auth_key'], 'string', 'max' => 32],
            [['user_level', 'mobile_number', 'refferal_id'], 'string', 'max' => 20],
            [['designation', 'ifsc'], 'string', 'max' => 100],
            [['aadhar_number'], 'string', 'max' => 30],
            [['email_id'], 'unique'],
            [['email_id','first_name','last_name','gender','age','mobile_number','password_hash'], 'required','on' => self::SCENARIO_CREATE],
            [['email_id','first_name','last_name','gender','age','mobile_number'], 'required','on' => self::SCENARIO_UPDATE],
            [['email_id'], 'unique'],
            [['email_id'], 'email'],
            [['mobile_number'], 'isNumeric'],
            [['age'], 'isAge'],
            
            
        ];
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => ['email_id','first_name','last_name','gender','age','mobile_number','password_hash'],
            self::SCENARIO_UPDATE => ['email_id','first_name','last_name','gender','age','mobile_number'],
                   
        ];
    }

    public function isNumeric($attribute, $params)
    {
        if (!is_numeric($this->mobile_number))
            $this->addError($attribute, Yii::t('app', 'Mobile Number Should Be in Number Format'));
    }

    public function isAge($attribute, $params)
    {
        if (!is_numeric($this->age))
            $this->addError($attribute, Yii::t('app', 'Age Should Be in Number Format'));

        if ($this->age > 120)
            $this->addError($attribute, Yii::t('app', 'Age Should Be in 120 Less'));
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'dob' => 'Dob',
            'user_type' => 'User Type',
            'city' => 'City',
            'auth_key' => 'Auth Key',
            'profile_image' => 'Profile Image',
            'profile_pdf' => 'Profile Pdf',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'rights' => 'Rights',
            'status_flag' => 'Status Flag',
            'user_level' => 'User Level',
            'mobile_number' => 'Mobile Number',
            'designation' => 'Designation',
            'user_role' => 'User Role',
            'address' => 'Address',
            'aadhar_number' => 'Aadhar Number',
            'subscripe_plan' => 'Subscripe Plan',
            'refferal_id' => 'Refferal ID',
            'refferal_name' => 'Refferal Name',
            'bank_name' => 'Bank Name',
            'bank_account_no' => 'Bank Account No',
            'ifsc' => 'Ifsc',
            'branch' => 'Branch',
            'email_id' => 'Email ID',
            'user_type_data' => 'User Type Data',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
            'gender' => 'Gender',
            'age' => 'Age',
            'gender_age' => 'Gender & Age',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToothpix()
    {
        return $this->hasMany(ToothPicture::className(), ['user_id' => 'id']);
    }
}

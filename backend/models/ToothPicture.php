<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tooth_picture".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $image_url
 * @property string $label
 * @property string $comments
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 */
class ToothPicture extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $email_id,$first_name,$last_name,$gender,$age,$mobile_number;
    public static function tableName()
    {
        return 'tooth_picture';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['image_url', 'comments'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['label', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'image_url' => 'Image Url',
            'label' => 'Label',
            'comments' => 'Comments',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}

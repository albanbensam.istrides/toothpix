<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\View;
/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DashboardAsset extends AssetBundle
{
	   public function init() {
        $this->jsOptions['position'] = View::POS_BEGIN;
        parent::init();
    }
	
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        
        "datetimepicker/build/css/bootstrap-datetimepicker.css",
        'theme-plugin/bootstrap/css/font-awesome.min.css',
        'theme-plugin/bootstrap/css/ionicons.min.css',
        'theme-plugin/assets/css/core.min.css',
        'theme-plugin/assets/css/app.min.css',

        'theme-plugin/assets/css/style.min.css',
		    'css/customized-style1.css',
        'css/site.css',
        'confirm/dist/jquery-confirm.min.css',
        'select2/css/select2.css',
        'validator/bootstrapValidator.min.css',

        //'datetimepicker/build/css/bootstrap-datetimepicker.css',
        //'datetimepicker/build/css/bootstrap-datetimepicker.min.css',
        'daterangepicker-master/daterangepicker.css',

        
        'alertify/css/alertify.min.css',
        'alertify/css/themes/semantic.min.css',
    ];
    public $js = [   		
      'theme-plugin/assets/js/core.min.js', 
      'theme-plugin/assets/js/app.min.js',
      'theme-plugin/assets/js/script.min.js',
	    'maxlengthvalidation/src/bootstrap-maxlength.js',
      'confirm/dist/jquery-confirm.min.js',	
      'sortable/sortable.js', 
      'select2/js/select2.js',
     // 'assets/e501e84a/yii.gridView.js',
      //'assets/28e1c8bf/jquery.pjax.js',

      'validator/bootstrapValidator.min.js',

      'daterangepicker-master/moment.min.js',
      'daterangepicker-master/daterangepicker.js',
      'twitter-bootstrap-wizard-master/validation.js',
      //'datetimepicker/build/js/moment.js',
      //'datetimepicker/build/js/bootstrap-datetimepicker.min.js',
      
      'alertify/alertify.js',
      'alertify/alertify.min.js',
      'google_chart/loader.js',
      "datetimepicker/build/js/moment.js",
      
       "datetimepicker/build/js/bootstrap-datetimepicker.min.js",
    ];
	
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}

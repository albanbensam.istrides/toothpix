<?php
date_default_timezone_set('asia/kolkata');

//$left_menu[0]['identify'] = array('name'=> 'DashBoard');
$left_menu[1]['one'] = array('name'=> 'DashBoard','url'=>'','action'=>'site/index');
$left_menu[2]['identify'] = array('name'=> 'Client Management');
$left_menu[3]['one'] = array('name'=> 'Manage Clients','url'=>'/user-index.html','action'=>'user/index');
$left_menu[4]['one'] = array('name'=> 'Toothpix Uploads','url'=>'/enquiry-index.html','action'=>'enquiry/index');
/*$left_menu[4]['identify'] = array('name'=> 'Master Data');
$left_menu[5]['one'] = array('name'=> 'Subscription Management','url'=>'/subscription-index.html','action'=>'subscription-master/index');
$left_menu[6]['one'] = array('name'=> 'Commission Structure','url'=>'/commission-index.html','action'=>'commission-structure/index');
$left_menu[7]['identify'] = array('name'=> 'User Management');
$left_menu[8]['more'] = array('menu-name'=>'User Management','sub-menu'=>array(array('name'=> 'Manage Users','url'=>'/user-index.html','action'=>'user/index'),array('name'=> 'Subscribed Users','url'=>'/subscription-table-active.html','action'=>'subscription-table/active-user')));
$left_menu[9]['identify'] = array('name'=> 'Reports');
$left_menu[10]['more'] = array('menu-name'=>'Report','sub-menu'=>array(array('name'=> 'Commission Report','url'=>'/level-completion-report.html','action'=>'level-completion-status/index'),array('name'=> 'Transaction Report','url'=>'/payment-log.html','action'=>'payment-log/index'),array('name'=> 'User Tree','url'=>'/binary-tree.html','action'=>'binary-user/indextree'),array('name'=> 'Excel & PDF Report','url'=>'/level-excel-report.html','action'=>'level-completion-status/commisionexcelreport')));
*/

$left_menu_data = array();
$left_menu_data['site/index'] = array('site/index');
$left_menu_data['user/index'] = array('user/create','user/update','user/index');
$left_menu_data['user/toothpix'] = array('user/toothpix','user/toothpixdata','user/toothpixpictureshow');
$left_menu_data['enquiry/index'] = array('enquiry/index','user/toothpixdata');



    
/*$left_menu_data = array();
$left_menu_data['site/index'] = array('site/index');
$left_menu_data['subscription-master/index'] = array('subscription-master/index');
$left_menu_data['commission-structure/index'] = array('commission-structure/index');
$left_menu_data['user/adminindex'] = array('user/admincreate','user/adminupdate','user/adminindex');
$left_menu_data['user/index'] = array('user/create','user/update');
$left_menu_data['subscription-table/active-user'] = array('subscription-table/closed-user','subscription-table/eliminated-user','subscription-table/rejoin-user','subscription-table/rejected-user');
$left_menu_data['level-completion-status/index'] = array();
$left_menu_data['payment-log/index'] = array();
$left_menu_data['binary-user/indextree'] = array();
$left_menu_data['level-completion-status/commisionexcelreport'] = array();*/


return [

    'admin_restriction' => array('name'=>array('Admin Management','Manage Admin Users','Master Data','Subscription Management','Commission Structure'),'menu-name'=>array('')),

    'left-menu-data' => $left_menu_data, 

    'adminEmail' => 'admin@abdhienterprises.com',
    'pagination' => array('5' => '5', '10' => '10', '20' => '20', '50' => '50', '100' => '100'),
    'left_menu' => $left_menu,

    'scenario' => 'SCENARIO_API_CREATE',

    'menu-key' => array('identify','one','more'),
    'date' => date('Y-m-d H:i:s'),
    'inputname' => 'hidden_token',
    'method_name' => array('active-user'=>array("Active","subscription-table-active"),'closed-user'=>array("Closed","subscription-table-closed"),'eliminated-user'=>array("Eliminated","subscription-table-eliminated"),'rejoin-user'=>array("Rejoin Request","subscription-table-rejoin"),'rejected-user'=>array("Rejoin Rejected","subscription-table-rejected")),
    //Labels Hardcode
    'labels'=> array('Eliminate Subscription','Elimination Remarks','Referred By','Close Subscription','Closing Date','Refferal Code & Name','Subscription Plan','No.of.Levels','No.of.Branches Per Level','User Referral Key'),
    //Title Hardcode
    'title' => array('user-index'=>'Manage Clients','tooth-index'=>'Toothpix Uploads'),

    //Success Message
    'success_message' => "<script type='text/javascript'> $('#w1-success-0').addClass('hide');  alertify.set('notifier','position', 'top-right');alertify.success('Record Saved Successfully');</script>",

    //Error Message
    'error_message' => "<script type='text/javascript'> $('#w1-success-0').addClass('hide');  alertify.set('notifier','position', 'top-right');alertify.error('Data Not Saved. Please Contact Admin');</script>",

    //Side
    'side' => array('R'=>'Right','L'=>'Left'),

    'image-type' => array('Top_Left','Top_Right','Bottom_Left','Bottom_Right'),
];

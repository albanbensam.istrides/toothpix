<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php'),
    require(__DIR__ . '/../../api/config/params.php'),
    require(__DIR__ . '/../../api/config/params-local.php')
    
);
use \yii\web\Request;

$baseUrl = str_replace('/backend/web', '', (new Request)->getBaseUrl());

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',

    'bootstrap' => ['log','MyGlobalClass'],
    'modules' => [],
    'timeZone' => 'Asia/Kolkata',
    'components' => [

    'formatter' => [
       'class' => 'yii\i18n\Formatter',
       'nullDisplay' => '',
    ],


    'session' => [
            'name' => 'MLMBACKEND' ,
            'timeout' => 1440,
           /* 'class' => 'yii\web\DbSession',
            'sessionTable' => 'yiisession',  */       
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => false,
            'authTimeout' => 3600, // auth expire 
        ],
        'MyGlobalClass'=>[        
            'class'=>'backend\components\MyGlobalClass'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'flushInterval' => 100,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'categories' => ['my_category'],
                    'exportInterval' => 100,
                    'logFile' => '@app/runtime/logs/trace.log',
                ],
            ],
        ],
        'errorHandler' => [
        'errorAction' => 'site/error',
        ],
		'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest'],
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
                'enableStrictParsing' => false,
                'suffix' => '.html',
                'rules' => [
                    // ...
                'home' => 'site/login',
                'logout' => 'site/logout',
                'index' => 'site/index',
                'user-index' => 'user/index',
                'user-create' => 'user/create',
                'user-update/<id:\d+>' => 'user/update',
                'user-delete/<id:\d+>' => 'user/delete',
                'profile/<id:\d+>' => 'user/profile',
                'user-toothpix' => 'user/toothpix',
                'tooth-index' => 'tooth-picture/index',
                'toothpixdata/<id:\d+>' => 'user/toothpixdata',
                'toothpixpictureshow/<id:\d+>' => 'user/toothpixpictureshow',
                'enquiry-index' => 'enquiry/index',
                'enquiry-view/<id:\d+>' => 'enquiry/view',
                    
                
            ],
        ],
        
    ],
    'params' => $params,
    /*'on beforeAction' => function ($event) {
        if(Yii::$app->request->pathInfo === 'search') {
            $url = 'site/index?' . Yii::$app->request->queryString;
            Yii::$app->response->redirect($url)->send();
            $event->handled = true;
        }
    }*/
];

<?php
namespace backend\controllers;

use Yii;
use backend\models\User;
use backend\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\components\QueryBehaviour;
use common\models\CommonSave;
use yii\helpers\Url;
use backend\models\ToothPicture;
use backend\models\Enquiry;
/**
 * UserSearchController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'user-delete' => ['POST'],
                ],
            ],

            'QueryBehaviour' => QueryBehaviour::className(),
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionAdminindex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->searchadmin(Yii::$app->request->queryParams);

        return $this->render('adminindex', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()    
    {
        $model = new User(['scenario' => User::SCENARIO_CREATE]);
        $session = Yii::$app->session;
        if ($model->load(Yii::$app->request->post())) 
        {

            if(Yii::$app->request->isAjax)
            {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            $dataProvider = Yii::$app->request->post('User');
            unset($dataProvider['hidden_token']);
            foreach ($dataProvider as $key => $value) {
                if($key == 'password_hash')
                {
                    $model->password_hash = Yii::$app->security->generatePasswordHash($value);
                    continue;
                }
                $model->$key = $value;  
            }
            $model->auth_key= Yii::$app->security->generateRandomString();
            if($model->save())
            {
                Yii::$app->session->setFlash('success', Yii::$app->params['success_message']);
            }
            else
            {
                Yii::$app->session->setFlash('success', Yii::$app->params['error_message']); 
            }
            return $this->redirect(['index']);
        }
        else
        {
            $formTokenName = uniqid();
            $session['hidden_token']=$formTokenName;
            return Yii::$app->controller->render('_form', [
                'model' => $model,
                'formTokenName' => $formTokenName,
            ]);    
        }
    }

    
    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = User::SCENARIO_UPDATE;
        $session = Yii::$app->session;
        if ($model->load(Yii::$app->request->post())) 
        {
            if(Yii::$app->request->isAjax)
            {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            $dataProvider = Yii::$app->request->post('User');
            unset($dataProvider['hidden_token']);
            foreach ($dataProvider as $key => $value) {
                if($key == 'password_hash')
                {
                    if($value == "")
                    {
                        $model->password_hash =$model->password_hash;
                    }
                    else
                    {
                        $model->password_hash = Yii::$app->security->generatePasswordHash($value);
                    }
                    continue;
                }
                $model->$key = $value;  
            }
            if($model->save())
            {
                Yii::$app->session->setFlash('success', Yii::$app->params['success_message']);
            }
            else
            {
                Yii::$app->session->setFlash('success', Yii::$app->params['error_message']); 
            }
            return $this->redirect(['index']);
        }
        else
        {
            $formTokenName = uniqid();
            $session['hidden_token']=$formTokenName;
            return Yii::$app->controller->render('_form', [
                'model' => $model,
                'formTokenName' => $formTokenName,
            ]);    
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionProfile($id)
    {
        $model = $this->findModel($id);
        $picture = ToothPicture::findAll(['user_id'=>1]); 
        return Yii::$app->controller->render('_picture', [
                'model' => $model,
                'picture' => $picture,
            ]);    
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionToothpix()
    { 
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->searchtoothpix(Yii::$app->request->queryParams);
        return $this->render('_toothpixindex', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);    
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionToothpixdata($id)
    {
        $enquiry = Enquiry::findOne(['enq_id'=>$id]);
            
        $picture = ToothPicture::findAll(['enq_id'=>$enquiry->enq_id]); 
        if(Yii::$app->request->post())
        {
            $enquiry->is_recommentation = 'Yes';
            $enquiry->enq_date = Yii::$app->params['date'];
            if($enquiry->save())
            {
                foreach (Yii::$app->request->post('label') as $key => $value) 
                {
                      $_pictureUpdate =  ToothPicture::findOne(['id'=>$key]);
                      $_pictureUpdate->label = $value;
                      $_pictureUpdate->comments = Yii::$app->request->post('Recommentation')[$key];
                      $_pictureUpdate->cavity = Yii::$app->request->post('cavity')[$key];
                      $_pictureUpdate->broken_filing = Yii::$app->request->post('brokenfilling')[$key];
                      $_pictureUpdate->save();
                }
            }
             return $this->redirect(['enquiry/index']);
        }
        else
        {
                return $this->render('_picture', [
                    'picture' => $picture,
                    'enquiry' => $enquiry,
                ]);
        }    
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionToothpixpictureshow($id,$data="")
    {
        $picture = ToothPicture::findOne(['id'=>$id]); 
          
          return Yii::$app->controller->renderPartial('_pictureone', [  'picture' => $picture, 'title'=>$data], true, false);

            /*return $this->render('_pictureone', [
                'picture' => $picture,
            ]); */
    }

    


    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    

}

<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;
use common\models\User;
use yii\db\Query;
use backend\components\QueryBehaviour;

//use backend\models\UserLogin;
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','create','userindex'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
                ],
            ],

            'QueryBehaviour' => QueryBehaviour::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {   
        $session = Yii::$app->session;

        $data_array = $this->dashboard_report();
        //$data_array = array();
        return $this->render('index',[
            'data_array' => $data_array,
        ]);
    }

   public function actionLogin()
    {         
        $this->layout='loginLayout';
        $model = new LoginForm();
        //die;
        if ($model->load(Yii::$app->request->post()) && $model->login()) 
        {
            $email_id = $_REQUEST['LoginForm']['email_id'];
            $user_data = User::find()->where(['email_id' => $email_id])->one();
            $session = Yii::$app->session;
            return $this->goHome(); //goBack changed as goHome
        } 
        else 
        { 
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        $session = Yii::$app->session;
        Yii::$app->user->logout();
        $session->destroy();
        return $this->goHome();
    }
}

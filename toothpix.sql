-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3307
-- Generation Time: Feb 13, 2021 at 12:50 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toothpix`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `increment_product_no` ()  NO SQL
BEGIN
DECLARE increment INT;	
SET increment = (SELECT random_id from random_generation where id=1)+1;
UPDATE random_generation SET random_id=increment WHERE id=1;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tooth_picture`
--

CREATE TABLE `tooth_picture` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `image_url` text,
  `label` varchar(255) DEFAULT NULL,
  `comments` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tooth_picture`
--

INSERT INTO `tooth_picture` (`id`, `user_id`, `image_url`, `label`, `comments`, `created_at`, `updated_at`, `status`) VALUES
(16, 1, 'backend/web/uploads/FrOfdQgEMP20161101002.jpg', NULL, NULL, '2021-02-13 17:00:42', '2021-02-13 17:00:42', 'A'),
(17, 1, 'backend/web/uploads/mjJACdSdriverKavin.jpg', NULL, NULL, '2021-02-13 17:00:42', '2021-02-13 17:00:42', 'A'),
(18, 1, 'backend/web/uploads/1nn1vcYdriverArun_Madhan.jpg', NULL, NULL, '2021-02-13 17:00:42', '2021-02-13 17:00:42', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `user_type` enum('A','U','P') COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'A=Admin, U=User, P=Player',
  `city` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile_pdf` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `rights` text COLLATE utf8_unicode_ci,
  `status_flag` enum('A','I') COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'A=Active, I=Inactive',
  `user_level` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `designation` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `aadhar_number` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subscripe_plan` int(11) DEFAULT NULL,
  `refferal_id` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `refferal_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_account_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ifsc` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `branch` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type_data` enum('P','C','O') COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'P=Parent,C=Child,O=Closed',
  `create_at` timestamp NULL DEFAULT NULL,
  `update_at` timestamp NULL DEFAULT NULL,
  `gender` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `age` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `first_name`, `last_name`, `dob`, `user_type`, `city`, `auth_key`, `profile_image`, `profile_pdf`, `password_hash`, `password_reset_token`, `status`, `created_at`, `updated_at`, `rights`, `status_flag`, `user_level`, `mobile_number`, `designation`, `user_role`, `address`, `aadhar_number`, `subscripe_plan`, `refferal_id`, `refferal_name`, `bank_name`, `bank_account_no`, `ifsc`, `branch`, `email_id`, `user_type_data`, `create_at`, `update_at`, `gender`, `age`) VALUES
(1, 'mlm', 'admin', 'a', '2019-11-01', 'A', 'Chennai', 'VVXWUK7plyXtnf47nHElnAQxEUoEvO4f', NULL, NULL, '$2y$13$ZLZaGB4c5p3dxFgDyG.Qi.4Fc7wxYw1kUVP/CHsVZC.PvZnHTm6r.', NULL, 10, 1567240418, 1567240418, NULL, 'A', '2', '24578887841', 'Developer', 'S', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'mlm@gmail.com', NULL, NULL, NULL, NULL, NULL),
(522, NULL, 'aaaa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$13$ZLZaGB4c5p3dxFgDyG.Qi.4Fc7wxYw1kUVP/CHsVZC.PvZnHTm6r.', NULL, 10, NULL, NULL, NULL, NULL, NULL, '9788440809', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'alben@gmail.com', NULL, NULL, NULL, 'Male', 23),
(523, NULL, 'alban', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$13$cL2xEyghO7RTp0cke7GQc.p9V5I4utxMzF65.5a27B.jAf0wWYdFO', NULL, 10, NULL, NULL, NULL, NULL, NULL, '9788440809', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'alban1@gmail.com', NULL, NULL, NULL, 'Male', 12);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tooth_picture`
--
ALTER TABLE `tooth_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_id` (`email_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tooth_picture`
--
ALTER TABLE `tooth_picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=524;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
